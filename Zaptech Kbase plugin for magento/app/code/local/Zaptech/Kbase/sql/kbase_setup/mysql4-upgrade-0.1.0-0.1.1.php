<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;

$installer->startSetup();
$installer->run("
        ALTER TABLE {$this->getTable('kbase')} ADD COLUMN `meta_title` varchar(255) DEFAULT '';
        ALTER TABLE {$this->getTable('kbase')} ADD COLUMN `meta_description` varchar(255) DEFAULT '';
        ALTER TABLE {$this->getTable('kbase')} ADD COLUMN `meta_keywords` varchar(255) DEFAULT '';
            
	ALTER TABLE {$this->getTable('kbase_cat')} ADD COLUMN `meta_title` varchar(255) DEFAULT '';
        ALTER TABLE {$this->getTable('kbase_cat')} ADD COLUMN `meta_description` varchar(255) DEFAULT '';
        ALTER TABLE {$this->getTable('kbase_cat')} ADD COLUMN `meta_keywords` varchar(255) DEFAULT '';
	");
$installer->endSetup();
