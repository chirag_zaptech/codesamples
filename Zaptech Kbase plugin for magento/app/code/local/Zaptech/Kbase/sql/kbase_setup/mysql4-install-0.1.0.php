<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('kbase')};
CREATE TABLE {$this->getTable('kbase')} (
  `kbase_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `identifier` varchar(255) NOT NULL default '',
  `contents` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `categories` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`kbase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO {$this->getTable('kbase')} (`kbase_id` ,`title`, `identifier` ,`contents` ,`status` ,`categories` ,`created_time` ,`update_time`)
VALUES (NULL ,'Test Article', 'test-article', 'This is first test article by Zaptech. This is your first article!', '1', '1' , NOW( ) , NOW( ));



-- DROP TABLE IF EXISTS {$this->getTable('kbase_cat')};
CREATE TABLE {$this->getTable('kbase_cat')} (
  `cat_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `identifier` varchar(255) NOT NULL default '',
  `contents` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO {$this->getTable('kbase_cat')} (`cat_id` ,`name`, `identifier` ,`contents` ,`status` ,`created_time` ,`update_time`)
VALUES (NULL ,'Test Category', 'test-category', 'This is first test category by Zaptech. This is your first Category!', '1', NOW( ) , NOW( ));



-- DROP TABLE IF EXISTS {$this->getTable('kbase_rating')};
CREATE TABLE {$this->getTable('kbase_rating')} (
  `rating_id` int(11) unsigned NOT NULL auto_increment,
  `articleid` int(11), 
  `articletitle` varchar(255) NOT NULL default '', 
  `customerid` int(11),
  `customername` varchar(255) NOT NULL default '',
  `totalrate` int(11),
  `status` smallint(6) NOT NULL default '0',  
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO {$this->getTable('kbase_rating')} (`rating_id` ,`articleid`, `articletitle` ,`customerid` ,`customername`, `totalrate`, `status`, `created_time`, `update_time`)
VALUES (NULL ,'1', 'Test Article', '1', 'Zaptech User', '5', '1' , NOW( ), NOW( ));



-- DROP TABLE IF EXISTS {$this->getTable('kbase_totalrating')};
CREATE TABLE {$this->getTable('kbase_totalrating')} (
  `totalrating_id` int(11) unsigned NOT NULL auto_increment,
  `articleid` int(11),
  `totalrate` int(11),
  `totalrating` int(11),
  `ratepercentage` int(11),
  PRIMARY KEY (`totalrating_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO {$this->getTable('kbase_totalrating')} (`totalrating_id` ,`articleid`, `totalrate` ,`totalrating` ,`ratepercentage`)
VALUES (NULL ,'1', '5', '1', '100');

    ");

$installer->endSetup();
