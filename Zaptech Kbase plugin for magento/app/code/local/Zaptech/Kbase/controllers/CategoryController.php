<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_CategoryController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        if (Mage::getStoreConfig('kbaseoptions/general/enabled')) {
            $this->loadLayout();
            $identifier = $this->getRequest()->getParam('identifier');
            $categoryCol = Mage::getModel('kbase/cat')->getCollection();
            $categoryCol->addFieldToFilter('identifier', $identifier);
            $categoryCol = $categoryCol->getData();
            $title = $categoryCol[0]['meta_title'];
            $description = $categoryCol[0]['meta_description'];
            $keywords = $categoryCol[0]['meta_keywords'];

            if ($title != "") {
                $this->getLayout()->getBlock('head')->setTitle($this->__($title));
            }
            if ($description != "") {
                $this->getLayout()->getBlock('head')->setDescription($this->__($description));
            }
            if ($keywords != "") {
                $this->getLayout()->getBlock('head')->setKeywords($this->__($keywords));
            }


            $this->renderLayout();
        } else {
            $this->_redirectUrl(Mage::helper('core/url')->getHomeUrl());
        }
    }

    public function articleAction() {
        if (Mage::getStoreConfig('kbaseoptions/general/enabled')) {
            $this->loadLayout();
            $identifier = $this->getRequest()->getParam('identifier');
            $articleCol = Mage::getModel('kbase/kbase')->getCollection();
            $articleCol->addFieldToFilter('identifier', $identifier);
            $articleCol = $articleCol->getData();
            $title = $articleCol[0]['meta_title'];
            $description = $articleCol[0]['meta_description'];
            $keywords = $articleCol[0]['meta_keywords'];
            if ($title != "") {
                $this->getLayout()->getBlock('head')->setTitle($this->__($title));
            }
            if ($description != "") {
                $this->getLayout()->getBlock('head')->setDescription($this->__($description));
            }
            if ($keywords != "") {
                $this->getLayout()->getBlock('head')->setKeywords($this->__($keywords));
            }
            $this->renderLayout();
        } else {
            $this->_redirectUrl(Mage::helper('core/url')->getHomeUrl());
        }
    }

    public function voteAction() {
        $identifier = $this->getRequest()->getParam('identifier');
        $articleid = $this->getRequest()->getParam('articleid');
        $articletitle = $this->getRequest()->getParam('articletitle');
        $custid = $this->getRequest()->getParam('custid');
        $custname = $this->getRequest()->getParam('custname');
        $rating = $this->getRequest()->getParam('rating');

        $model = Mage::getModel('kbase/rating');
        $model->setArticleid($articleid);
        $model->setArticletitle($articletitle);
        $model->setCustomerid($custid);
        $model->setCustomername($custname);
        $model->setStatus(2);
        $model->setTotalrate($rating);
        try {
            if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                $model->setCreatedTime(now())
                        ->setUpdateTime(now());
            } else {
                $model->setUpdateTime(now());
            }
            $model->save();
            Mage::getSingleton('core/session')->addSuccess(Mage::helper('kbase')->__('Your vote has been accepted. Thank you!'));
            Mage::getSingleton('core/session')->setFormData(false);
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError(Mage::helper('kbase')->__('Your vote not saved!'));
            Mage::getSingleton('core/session')->setFormData($data);
        }
        $this->_redirect("*/article/$identifier/");
    }

}
