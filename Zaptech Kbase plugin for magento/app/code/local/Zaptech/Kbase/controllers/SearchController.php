<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_SearchController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        if (Mage::getStoreConfig('kbaseoptions/search/enable')) {
            $this->loadLayout();
            $this->renderLayout();
        } else {
            $this->_redirectUrl(Mage::helper('core/url')->getHomeUrl());
        }
    }

}
