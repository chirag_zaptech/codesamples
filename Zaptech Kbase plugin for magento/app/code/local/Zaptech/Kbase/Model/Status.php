<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Model_Status extends Varien_Object {

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    static public function getOptionArray() {
        return array(
            self::STATUS_ENABLED => Mage::helper('kbase')->__('Approved'),
            self::STATUS_DISABLED => Mage::helper('kbase')->__('Disapproved')
        );
    }

}
