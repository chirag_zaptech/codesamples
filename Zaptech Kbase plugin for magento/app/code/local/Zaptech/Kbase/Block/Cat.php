<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Cat extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getCat() {
        if (!$this->hasData('cat')) {
            $this->setData('cat', Mage::registry('cat'));
        }
        return $this->getData('cat');
    }

    public function getCategoryName() {
        $id = $this->getRequest()->getParam('id');
        $category = Mage::getModel('kbase/cat')->load($id);
        return $category->getName();
    }

}
