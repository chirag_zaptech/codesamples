<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Kbase extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }
    
    public function getKbase() {
        if (!$this->hasData('kbase')) {
            $this->setData('kbase', Mage::registry('kbase'));
        }
        return $this->getData('kbase');
    }

}
