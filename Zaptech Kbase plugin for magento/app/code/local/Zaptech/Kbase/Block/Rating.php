<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Rating extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }
    
    /*
     * Get Rating Stored in Registry
     * @input void
     * @return string
     */
    public function getRating() {
        if (!$this->hasData('rating')) {
            $this->setData('rating', Mage::registry('rating'));
        }
        return $this->getData('rating');
    }

}
