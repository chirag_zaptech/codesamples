<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Cat_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'kbase';
        $this->_controller = 'adminhtml_cat';

        $this->_updateButton('save', 'label', Mage::helper('kbase')->__('Save Category'));
        $this->_updateButton('delete', 'label', Mage::helper('kbase')->__('Delete Category'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('kbase_contents') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'kbase_contents');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'kbase_contents');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /*
     * Get Header Text Of Edit/Add Category Form
     * @input void
     * @return string
     */

    public function getHeaderText() {
        if (Mage::registry('kbase_data') && Mage::registry('kbase_data')->getId()) {
            return Mage::helper('kbase')->__("Edit Category '%s'", $this->htmlEscape(Mage::registry('kbase_data')->getName()));
        } else {
            return Mage::helper('kbase')->__('Add Category');
        }
    }

}
