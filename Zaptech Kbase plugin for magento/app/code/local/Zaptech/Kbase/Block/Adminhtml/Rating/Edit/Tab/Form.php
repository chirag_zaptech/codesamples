<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Rating_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('rating_form', array('legend' => Mage::helper('kbase')->__('General information')));

        $fieldset->addField('articleid', 'hidden', array(
            'label' => Mage::helper('kbase')->__('Article Id'),
            'name' => 'articleid',
        ));

        $fieldset->addField('articletitle', 'text', array(
            'label' => Mage::helper('kbase')->__('Article Title'),
            'name' => 'articletitle',
            'readonly' => 'readonly',
        ));

        $fieldset->addField('customername', 'text', array(
            'label' => Mage::helper('kbase')->__('Customer Name'),
            'name' => 'customername',
            'readonly' => 'readonly',
        ));

        $fieldset->addField('totalrate', 'text', array(
            'label' => Mage::helper('kbase')->__('Total Rate'),
            'name' => 'totalrate',
            'readonly' => 'readonly',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('kbase')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('kbase')->__('Approved'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('kbase')->__('Disapproved'),
                ),
            ),
        ));

        if (Mage::getSingleton('adminhtml/session')->getCatData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getCatData());
            Mage::getSingleton('adminhtml/session')->setCatData(null);
        } elseif (Mage::registry('kbase_data')) {
            $form->setValues(Mage::registry('kbase_data')->getData());
        }
        return parent::_prepareForm();
    }

}
