<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Kbase_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('kbaseGrid');
        $this->setDefaultSort('kbase_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('kbase/kbase')->getCollection();
        $categoryTable = Mage::getSingleton('core/resource')->getTableName('kbase/cat');

        $collection->getSelect()
                ->joinLeft(array("cat" => $categoryTable), "main_table.categories = cat.cat_id", array("cat_name" => "cat.name"));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('kbase_id', array(
            'header' => Mage::helper('kbase')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'kbase_id',
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('kbase')->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));

        $this->addColumn('identifier', array(
            'header' => Mage::helper('kbase')->__('Identifier'),
            'align' => 'left',
            'index' => 'identifier',
            'filter_index' => 'main_table.identifier'
        ));


        $this->addColumn('category', array(
            'header' => Mage::helper('kbase')->__('Category'),
            'width' => '150px',
            'index' => 'cat_name',
            'filter_index' => 'cat.name',
        ));


        $this->addColumn('status', array(
            'header' => Mage::helper('kbase')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('kbase')->__('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('kbase')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('kbase')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('kbase')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('kbase_id');
        $this->getMassactionBlock()->setFormFieldName('kbase');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('kbase')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('kbase')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('kbase/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('kbase')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('kbase')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
