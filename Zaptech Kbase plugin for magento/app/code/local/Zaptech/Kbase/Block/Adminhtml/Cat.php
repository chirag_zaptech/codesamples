<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Cat extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_cat';
        $this->_blockGroup = 'kbase';
        $this->_headerText = Mage::helper('kbase')->__('Categories Manager');
        $this->_addButtonLabel = Mage::helper('kbase')->__('Add Category');
        parent::__construct();
    }

}
