<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Rating_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('ratingGrid');
        $this->setDefaultSort('rating_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('kbase/rating')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('rating_id', array(
            'header' => Mage::helper('kbase')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'rating_id',
        ));

        $this->addColumn('articletitle', array(
            'header' => Mage::helper('kbase')->__('Article Title'),
            'align' => 'left',
            'index' => 'articletitle',
        ));

        $this->addColumn('customername', array(
            'header' => Mage::helper('kbase')->__('Customer Name'),
            'align' => 'left',
            'index' => 'customername',
        ));
        
        $this->addColumn('totalrate', array(
            'header' => Mage::helper('kbase')->__('Rate'),
            'align' => 'left',
            'index' => 'totalrate',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('kbase')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Approved',
                2 => 'Disapproved',
            ),
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('kbase')->__('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('kbase')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('kbase')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('kbase')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('rating_id');
        $this->getMassactionBlock()->setFormFieldName('rating');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('kbase')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('kbase')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('kbase/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('kbase')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('kbase')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
