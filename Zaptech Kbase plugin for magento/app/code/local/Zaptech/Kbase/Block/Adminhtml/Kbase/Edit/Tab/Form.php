<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Kbase_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareLayout() {
        $return = parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        return $return;
    }

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('kbase_form', array('legend' => Mage::helper('kbase')->__('General information')));

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('kbase')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('identifier', 'text', array(
            'label' => Mage::helper('kbase')->__('Url Key'),
            'class' => 'required-entry',
            'name' => 'identifier',
            'disabled' => ($this->getRequest()->getParam('id') > 0) ? 'disabled' : '',
            'class' => 'validation-identifier',
            'after_element_html' => "
                <p class='note'>This should be unique url key. It will be autogenerated if it's left blank.</p><script>
                    Validation.add(
                    'validation-identifier', 
                    '" . addslashes(Mage::helper('kbase')->__("Please use only letters (a-z or A-Z), numbers (0-9) or symbols '-' and '_' in this field")) . "',
                    function(v, elm) {
                                    return Validation.get('IsEmpty').test(v) || /^[a-z0-9][a-z0-9_\/-]+(\.[a-z0-9_-]+)?$/.test(v)                           
                         }
                    );
                    </script>",
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('kbase')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('kbase')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('kbase')->__('Disabled'),
                ),
            ),
        ));

        $fieldset->addField('contents', 'editor', array(
            "label" => Mage::helper("kbase")->__("Content"),
            'required' => true,
            "value" => $this->getRequest()->getParam('id') ? Mage::getModel('kbase/kbase')->load($this->getRequest()->getParam('id'))->getContent() : "",
            'style' => 'width:700px; height:500px;',
            'wysiwyg' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            "name" => "contents",
        ));


        if (Mage::getSingleton('adminhtml/session')->getKbaseData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getKbaseData());
            Mage::getSingleton('adminhtml/session')->setKbaseData(null);
        } elseif (Mage::registry('kbase_data')) {
            $form->setValues(Mage::registry('kbase_data'));
        }
        return parent::_prepareForm();
    }

}
