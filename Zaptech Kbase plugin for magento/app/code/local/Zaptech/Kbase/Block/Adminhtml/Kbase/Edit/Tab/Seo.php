<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Kbase_Edit_Tab_Seo extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('kbase_form', array('legend' => Mage::helper('kbase')->__('Meta information')));

        $fieldset->addField('meta_title', 'text', array(
            'label' => Mage::helper('kbase')->__('Meta Title'),
            'required' => false,
            'name' => 'meta_title',
        ));

        $fieldset->addField('meta_keywords', 'textarea', array(
            'label' => Mage::helper('kbase')->__('Meta Keywords'),
            'required' => false,
            'name' => 'meta_keywords',
        ));

        $fieldset->addField('meta_description', 'textarea', array(
            'label' => Mage::helper('kbase')->__('Meta Description'),
            'required' => false,
            'name' => 'meta_description',
        ));

        if (Mage::getSingleton('adminhtml/session')->getKbaseData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getKbaseData());
            Mage::getSingleton('adminhtml/session')->setKbaseData(null);
        } elseif (Mage::registry('kbase_data')) {
            $form->setValues(Mage::registry('kbase_data'));
        }

        return parent::_prepareForm();
    }

}
