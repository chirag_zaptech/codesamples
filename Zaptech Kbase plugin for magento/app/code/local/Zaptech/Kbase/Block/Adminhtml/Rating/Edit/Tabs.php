<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Rating_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('kbase_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('kbase')->__('Rating Information'));
    }
    
    /*
     * Create Tab For Edit Rating
     */

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => Mage::helper('kbase')->__('General'),
            'title' => Mage::helper('kbase')->__('General'),
            'content' => $this->getLayout()->createBlock('kbase/adminhtml_rating_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
