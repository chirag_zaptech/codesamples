<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Kbase_Edit_Tab_Categories extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('kbase_form', array('legend' => Mage::helper('kbase')->__('Categorie information')));

        $option = array("" => "Select Category");
        $catcoll = Mage::getModel("kbase/cat")->getCollection();
        foreach ($catcoll as $cat):
            $option[$cat->getId()] = $cat->getName();
        endforeach;
        $fieldset->addField('categories', 'select', array(
            'label' => Mage::helper('kbase')->__('Categories'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'categories',
            'values' => $option,
        ));

        if (Mage::getSingleton('adminhtml/session')->getKbaseData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getKbaseData());
            Mage::getSingleton('adminhtml/session')->setKbaseData(null);
        } elseif (Mage::registry('kbase_data')) {
            $form->setValues(Mage::registry('kbase_data')->getData());
        }
        return parent::_prepareForm();
    }

}
