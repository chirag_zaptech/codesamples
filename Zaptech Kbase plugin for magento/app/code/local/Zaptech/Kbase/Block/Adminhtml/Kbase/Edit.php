<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Block_Adminhtml_Kbase_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'kbase';
        $this->_controller = 'adminhtml_kbase';

        $this->_updateButton('save', 'label', Mage::helper('kbase')->__('Save Article'));
        $this->_updateButton('delete', 'label', Mage::helper('kbase')->__('Delete Article'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('kbase_contents') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'kbase_contents');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'kbase_contents');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /*
     * Get Header Text Of Edit Article/Add Article From
     * @input void
     * @return string
     */
    public function getHeaderText() {
        if (Mage::registry('kbase_data') && Mage::registry('kbase_data')->getId()) {
            return Mage::helper('kbase')->__("Edit Article '%s'", $this->htmlEscape(Mage::registry('kbase_data')->getTitle()));
        } else {
            return Mage::helper('kbase')->__('Add Article');
        }
    }

}
