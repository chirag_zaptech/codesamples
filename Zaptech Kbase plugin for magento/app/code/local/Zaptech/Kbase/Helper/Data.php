<?php
/* 
 * Zaptech Knowledge Base - Version 0.1.1 
 * Websites: http://www.zaptechsolutions.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zaptech_Kbase_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getKbaseUrl() {
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . 'kbase/';
        return $url;
    }

    /*
     * Get to know whether kbase is enabled or not
     * @input void
     * @return boolean
     */
    public function getMode() {
        $Mode = Mage::getStoreConfig('kbaseoptions/general/enable');
        return $Mode;
    }

    /*
     * If a category is having enabled articles then it's available to be shown on frontend
     * @input Zaptech_Kbase_Model_Cat
     * @return boolean
     */

    public function isAvailable(Zaptech_Kbase_Model_Cat $cat) {
        $articleCollection = Mage::getModel('kbase/kbase')->getCollection();
        $articleCollection->addFieldToFilter('status', 1);
        $articleCollection->addFieldToFilter('categories', $cat->getCatId());
        if ($articleCollection->count()):
            return true;
        endif;
        return false;
    }

}
