﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webPrecision.BusinessAccess;
using System.Data;
using webPrecision.BusinessAccess.Bean;
using System.Text;
using System.IO;

namespace webPrecision
{
    public partial class index : System.Web.UI.Page
    {
        #region "Variables"
        static int shortway = 0;
        #endregion

        #region "Page Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            GlobalFunctions.FrontLoginCheck(Convert.ToString(Request.Url.LocalPath.Substring(1)));
            if (!IsPostBack)
            {
                bindgrid();
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grdView1.AllowPaging = false;
                grdView1.GridLines = GridLines.Both;

                bindgrid();
                System.IO.StringWriter stringwriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlwriter = new System.Web.UI.HtmlTextWriter(stringwriter);
                grdView1.RenderControl(htmlwriter);
                hndGridViewPrintContent.Value = stringwriter.ToString();

                grdView1.AllowPaging = true;
                grdView1.GridLines = GridLines.None;
                bindgrid();
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        { }
        #endregion

        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
        //       server control at run time. */
        //}
        
        #region "Click Events"
        protected void btn_SignUp_Click(object sender, EventArgs e)
        {
            Response.Redirect("Signup.aspx");
        }
        protected void btn_Logoff_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            GlobalFunctions.ToSession("Loggedin_front", "false");
            Response.Redirect("index.aspx");
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int iError = 0;
            string sError = string.Empty;

            trError.Visible = false;
            try
            {
                ClearControls();

                string[] sData = txtData.Text.Replace("\r", string.Empty).Replace("\n\n \n\n", "\n").Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                string sTitle = string.Empty;
                string sCategory = string.Empty;
                string sType = string.Empty;
                string sDate = string.Empty;
                string sNAICS = string.Empty;
                string sFEDBIZOPPS = string.Empty;
                string sDepartment = string.Empty;
                string sAddress = string.Empty;
                string sDescription = string.Empty;
                string sLink = string.Empty;
                int iPermalink = 0;
                int iEMAIL = 0;
                int iNAICS = 0;
                string sPermalink = string.Empty;
                string sEMAIL = string.Empty;

                try { sTitle = sData[0].Replace("Title:", string.Empty).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Title not found.\r\n";
                }

                try { sCategory = sData[1].Replace("Category:", string.Empty).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Category not found.\r\n";
                }

                try { sType = sData[2].Replace("Type:", string.Empty).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Type not found.\r\n";
                }

                try { sDate = sData[3].Replace("Date:", string.Empty).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Date not found.\r\n";
                }

                try { sNAICS = sData[4].Replace("Keyword:", string.Empty).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Keyword not found.\r\n";
                }

                try { sFEDBIZOPPS = sData[5].Replace("FEDBIZOPPS:", string.Empty).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") FEDBIZOPPS not found.\r\n";
                }

                try { sDepartment = sData[6].Substring(0, sData[6].IndexOf(",")).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Department not found.\r\n";
                }

                try { sAddress = sData[6].Substring(sData[6].IndexOf(",") + 1).Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Address not found.\r\n";
                }

                try { sDescription = sData[7].Trim(); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Description not found.\r\n";
                }

                //try { sLink = sData[9].Trim(); }
                //catch
                //{
                //    iError++;
                //    sError += iError.ToString() + ") Link not found.\r\n";
                //}

                txtNAICS.Text = sNAICS;

                try
                {
                    string[] strArr = sDescription.Split(' ');
                    int SOLIndex = 0;
                    for (int i = 0; i < strArr.Length; i++)
                    {
                        if (strArr[i].Equals("SOL"))
                        {
                            SOLIndex = i;
                            txtSOL.Text = strArr[SOLIndex + 1];
                            break;
                        }
                    }

                    if (SOLIndex.Equals(0))
                    {
                        iError++;
                        sError += iError.ToString() + ") SOL not found.\r\n";
                    }
                    //txtSOL.Text = sTitle.Substring(0, sTitle.IndexOf(" - ")).Trim();
                    //txtSOL.Text = sDescription.Substring(sDescription.IndexOf("SOL"), sDescription.IndexOf("POC") - sDescription.IndexOf("SOL")).Replace("SOL", string.Empty).Trim();
                }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") SOL not found.\r\n";
                }

                txtDate.Text = sDate;
                txtType.Text = sType;

                txtTitle.Text = sTitle.Replace("_", " ");
                txtCategory.Text = sCategory;

                txtDepartment.Text = sDepartment;
                txtAddress.Text = sAddress;
                txtDescription.Text = sDescription;
                txtDueDate.Text = DateTime.Now.ToString("MM/dd/yyyy").Replace("-", "/");

                try { iPermalink = txtData.Text.IndexOf("Permalink"); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") Permalink not found.\r\n";
                }

                try { iEMAIL = txtData.Text.IndexOf("E-MAIL:"); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") EMAIL not found.\r\n";
                }

                try { iNAICS = txtData.Text.IndexOf("NAICS:"); }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") NAICS not found.\r\n";
                }

                try { sPermalink = txtData.Text.Substring(iPermalink, iEMAIL - iPermalink).Replace("Permalink", string.Empty).Trim(); }
                catch
                {
                    try { sPermalink = txtData.Text.Substring(iPermalink, iNAICS - iPermalink).Replace("Permalink", string.Empty).Trim(); }
                    catch
                    {
                        iError++;
                        sError += iError.ToString() + ") Permalink not found.\r\n";
                    }
                }


                try
                {
                    sEMAIL = string.Empty;
                    string[] strArr = txtData.Text.Substring(iEMAIL, iNAICS - iEMAIL).Replace("E-MAIL:", string.Empty).Trim().Split(' ');
                    int EmailIndex = 0;

                    for (int i = 0; i < strArr.Length; i++)
                    {
                        if (EmailIndex > 0)
                            break;
                        else
                        {
                            if (string.IsNullOrEmpty(sEMAIL))
                                sEMAIL = strArr[i];
                            else
                                sEMAIL = sEMAIL + ' ' + strArr[i];
                        }

                        if (strArr[i].IndexOf("@") >= 0)
                            EmailIndex = i;
                    }

                    if (EmailIndex.Equals(0))
                    {
                        iError++;
                        sError += iError.ToString() + ") EMAIL not found.\r\n";
                    }
                }
                catch
                {
                    iError++;
                    sError += iError.ToString() + ") EMAIL not found.\r\n";
                }

                //try { sEMAIL = txtData.Text.Substring(iEMAIL, iNAICS - iEMAIL).Replace("E-MAIL:", string.Empty).Trim(); }
                //catch
                //{
                //    iError++;
                //    sError += iError.ToString() + ") EMAIL not found.\r\n";
                //}

                txtLink.Text = sPermalink;
                txtContact.Text = sEMAIL;

                if (txtData.Text.IndexOf("RFI") > 0 && txtData.Text.IndexOf("RFP") > 0)
                    ddlRType.SelectedIndex = 3;
                else
                {
                    if (txtData.Text.IndexOf("RFI") > 0)
                        ddlRType.SelectedIndex = 1;
                    else if (txtData.Text.IndexOf("RFP") > 0)
                        ddlRType.SelectedIndex = 2;
                    else
                        ddlRType.SelectedIndex = 0;
                }

                tabLoad.ActiveTabIndex = 2;
                if (!string.IsNullOrEmpty(sError))
                {
                    trError.Visible = true;
                    lblError.Text = sError;
                }
            }
            catch
            {
                ClearControls();
                trError.Visible = true;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                webPrecision.Entities.Opportunity objOpportunity = new webPrecision.Entities.Opportunity();

                objOpportunity.OpportunityId = Convert.ToInt32(hf_OpportunityId.Value);
                objOpportunity.ONAICS = txtNAICS.Text.Trim();
                objOpportunity.Priority = txtPriority.Text.Trim();
                objOpportunity.OSOL = txtSOL.Text.Trim();
                objOpportunity.ORType = ddlRType.SelectedItem.Text.Trim();
                objOpportunity.ODate = txtDate.Text.Trim();
                objOpportunity.OSB = ddlSB.SelectedItem.Text.Trim();
                objOpportunity.OType = txtType.Text.Trim();
                objOpportunity.OTitle = txtTitle.Text.Trim();
                objOpportunity.OIsHide = chkHide.Checked == true ? 1 : 0;
                objOpportunity.OCategory = txtCategory.Text.Trim();
                objOpportunity.ODepartment = txtDepartment.Text.Trim();
                objOpportunity.OAddress = txtAddress.Text.Trim();
                objOpportunity.OContact = txtContact.Text.Trim();
                objOpportunity.OLink = txtLink.Text.Trim();
                objOpportunity.OPrime = txtPrime.Text.Trim();
                objOpportunity.ODescription = txtDescription.Text.Trim();
                objOpportunity.ODueDate = txtDueDate.Text.Trim();
                objOpportunity.OAssignedto = txtAssignedTo.Text.Trim();
                objOpportunity.ONotes = txtNotes.Text.Trim();

                BeanHelper.OpportunityBean.OpportunityObject = objOpportunity;
                int insertid = 0;

                if (Convert.ToInt32(hf_OpportunityId.Value) == 0)
                    insertid = BeanHelper.OpportunityBean.InsertRecord();
                else
                    insertid = BeanHelper.OpportunityBean.UpdateRecord();

                if (insertid > 0)
                {
                    txtData.Text = string.Empty;
                    ClearControls();

                    bindgrid();
                    tabLoad.TabIndex = 0;
                    txtData.Focus();
                }
            }
            catch (Exception Ex)
            {
                txtNotes.Text = Ex.Source.ToString() + "\r\n" + Ex.StackTrace.ToString();
            }
        }
        protected void btn_click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            if (lnk.ID == "lnk_update")
            {
                string id = Convert.ToString(lnk.CommandArgument);
                if (!string.IsNullOrEmpty(id))
                {
                    DataRow dr = BeanHelper.OpportunityBean.GetRecord(Convert.ToInt64(id));

                    hf_OpportunityId.Value = Convert.ToString(dr["OpportunityId"]);
                    txtNAICS.Text = Convert.ToString(dr["ONAICS"]);
                    txtPriority.Text = Convert.ToString(dr["Priority"]);
                    txtSOL.Text = Convert.ToString(dr["OSOL"]);

                    #region-->Changes done by Yashashri on 11-12-2013
                    string strRType = Convert.ToString(dr["ORType"]);
                    if (strRType == "RFI")
                    {
                        ddlRType.SelectedIndex = 1;
                    }
                    else if (strRType == "RFQ")
                    {
                        ddlRType.SelectedIndex = 2;
                    }
                    else
                    {
                        ddlRType.SelectedIndex = 0;
                    }
                    //ddlRType.SelectedItem.Text = Convert.ToString(dr["ORType"]);
                    #endregion
                    txtDate.Text = Convert.ToString(dr["ODate"]);

                    #region-->changes done by Yashashri on 11-12-2013
                    string strSB = Convert.ToString(dr["OSB"]);
                    if (strSB == "SB")
                    {
                        ddlSB.SelectedIndex = 1;
                    }
                    else if (strSB == "VOSB")
                    {
                        ddlSB.SelectedIndex = 2;
                    }
                    else if (strSB == "SDVOSB")
                    {
                        ddlSB.SelectedIndex = 3;
                    }
                    else if (strSB == "8A")
                    {
                        ddlSB.SelectedIndex = 4;
                    }
                    else
                    {
                        ddlSB.SelectedIndex = 0;
                    }
                    #endregion
                    //ddlSB.SelectedItem.Text = Convert.ToString(dr["OSB"]);
                    txtType.Text = Convert.ToString(dr["OType"]);
                    txtTitle.Text = Convert.ToString(dr["OTitle"]);
                    chkHide.Checked = Convert.ToInt32(dr["OIsHide"]) == 1 ? true : false;
                    txtCategory.Text = Convert.ToString(dr["OCategory"]);

                    txtDepartment.Text = Convert.ToString(dr["ODepartment"]);
                    txtAddress.Text = Convert.ToString(dr["OAddress"]);
                    txtContact.Text = Convert.ToString(dr["OContact"]);
                    txtLink.Text = Convert.ToString(dr["OLink"]);
                    txtPrime.Text = Convert.ToString(dr["OPrime"]);
                    txtDescription.Text = Convert.ToString(dr["ODescription"]);

                    txtDueDate.Text = Convert.ToString(dr["ODueDate"]);
                    txtAssignedTo.Text = Convert.ToString(dr["OAssignedto"]);
                    txtNotes.Text = Convert.ToString(dr["ONotes"]);
                    tabLoad.ActiveTabIndex = 2;
                }
            }
            else if (lnk.ID == "lnk_delete")
            {
                string id = Convert.ToString(lnk.CommandArgument);
                if (!string.IsNullOrEmpty(id))
                {
                    int returnid = BeanHelper.OpportunityBean.DeleteRecord(Convert.ToInt64(id));
                    bindgrid();
                }
            }
        }
        protected void chkShowHidden_CheckedChanged(object sender, EventArgs e)
        {
            bindgrid();
        }

        //#region--> Click Event added for printing the Opportunities Grid View. Yashashri on 12/12/2013
        //protected void Print_Click(object sender, EventArgs e)
        //{
        //    grd_main.AllowPaging = false;
        //    //bindgrid();
        //    DataTable Grid_tbl = new DataTable();
        //    Grid_tbl = BeanHelper.OpportunityBean.SearchRecord(chkShowHidden.Checked == false ? "OIsHide = 0" : string.Empty);
        //    grd_main.DataSource = Grid_tbl;
        //    grd_main.DataBind();
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter hw = new HtmlTextWriter(sw);
        //    grd_main.RenderControl(hw);
        //    string gridHTML = sw.ToString().Replace("\"", "'")
        //        .Replace(System.Environment.NewLine, "");
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<script type = 'text/javascript'>");
        //    sb.Append("window.onload = new function(){");
        //    sb.Append("var printWin = window.open('', '', 'left=0");
        //    sb.Append(",top=0,width=1000,height=600,status=0');");
        //    sb.Append("printWin.document.write(\"");
        //    sb.Append(gridHTML);
        //    sb.Append("\");");
        //    sb.Append("printWin.document.close();");
        //    sb.Append("printWin.focus();");
        //    sb.Append("printWin.print();");
        //    sb.Append("printWin.close();};");
        //    sb.Append("</script>");
        //    ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
        //    grd_main.AllowPaging = true;
        //    grd_main.DataSource = Grid_tbl;
        //    grd_main.DataBind();
        //    Grid_tbl = null;
        //}
        //#endregion
        #endregion

        #region "Functions"
        void ClearControls()
        {
            hf_OpportunityId.Value = "0";
            txtNAICS.Text = string.Empty;
            txtPriority.Text = string.Empty;
            txtSOL.Text = string.Empty;
            ddlRType.SelectedIndex = 0;
            txtDate.Text = string.Empty;
            ddlSB.SelectedIndex = 0;
            txtType.Text = string.Empty;

            txtTitle.Text = string.Empty;
            chkHide.Checked = false;
            txtCategory.Text = string.Empty;
            txtDepartment.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtContact.Text = string.Empty;
            txtLink.Text = string.Empty;
            txtPrime.Text = string.Empty;

            txtDescription.Text = string.Empty;
            txtDueDate.Text = string.Empty;
            txtAssignedTo.Text = string.Empty;
            txtNotes.Text = string.Empty;
        }
        protected string logout()
        {
            //try
            //{
            //    if (Session["ClearSession"].Equals(1))
            //    {
            Session.Abandon();
            GlobalFunctions.ToSession("Loggedin_front", "false");
            Session["ClearSession"] = 1;
            //    }
            //}
            //catch { }
            return "Done";
        }
        #endregion

        #region Grid function
        public void bindgrid()
        {
            DataTable Grid_tbl = new DataTable();
            Grid_tbl = BeanHelper.OpportunityBean.SearchRecord(chkShowHidden.Checked == false ? "OIsHide = 0" : string.Empty);

            GlobalFunctions.SetPaging(ref grd_main);
            if (Grid_tbl.Rows.Count > 0)
            {
                if (ViewState["lastsorton"] != null)
                {
                    DataView dv = new DataView(Grid_tbl);
                    dv.Sort = Convert.ToString(ViewState["lastsorton"]);

                    grd_main.DataSource = dv;
                    grd_main.DataBind();

                    grdView1.DataSource = dv;
                    grdView1.DataBind();
                }
                else
                {
                    grd_main.DataSource = Grid_tbl;
                    grd_main.DataBind();

                    grdView1.DataSource = Grid_tbl;
                    grdView1.DataBind();
                }
            }
            else
            {
                grd_main.DataSource = null;
                grd_main.DataBind();

                grdView1.DataSource = null;
                grdView1.DataBind();
            }
            Grid_tbl = null;
        }
        protected void lnksort_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                if (lnk != null)
                {
                    if (shortway % 2 == 0)
                        ViewState["lastsorton"] = Convert.ToString(lnk.ID) + " ASC";
                    else
                        ViewState["lastsorton"] = Convert.ToString(lnk.ID) + " DESC";

                    bindgrid();
                    shortway++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Convert.ToString(ex));
            }
        }
        protected void grd_main_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grd_main.PageIndex = e.NewPageIndex;
            bindgrid();
        }
        protected void grd_main_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (Convert.ToString(((System.Data.DataRowView)(e.Row.DataItem)).Row["OIsHide"]) == "0")
                {
                    if ((System.Web.UI.HtmlControls.HtmlImage)e.Row.FindControl("imgIsHidden") != null)
                    {
                        ((System.Web.UI.HtmlControls.HtmlImage)e.Row.FindControl("imgIsHidden")).Visible = false;
                    }
                }
            }
        }
        #endregion
    }
}