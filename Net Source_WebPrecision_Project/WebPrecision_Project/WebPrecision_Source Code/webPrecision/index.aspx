﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="webPrecision.index"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Precision Turbines, INC.</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="themes/style.css" />
    <meta http-equiv="X-XSS-Protection" content="0" />
    <meta http-equiv="Content-Security-Policy" content="0" />
</head>
<body>    
    <script type="text/javascript" language="javascript">
        function PrintGridView() {
            var printWindow = window.open("", "mywindow", "location=0,status=0,scrollbars=1,resizable=1");
            var strContent = "<html><body>";
            strContent = strContent + "<title>Print Preview</title>";
            strContent = strContent + "<link href=\"App_Themes/Default/Default.css\" type=\"text/css\" rel=\"stylesheet\" />";
            strContent = strContent + "</head><body>";
            strContent = strContent + "<div id='buttons' style='width:100%;text-align:right;'>";
            strContent = strContent + "<input type='button' id='btnPrint' value='Print' style='width:100px' onclick='window.print()' />";
            strContent = strContent + "<input type='button' id='btnCancel' value='Cancel' style='width:100px' onclick='window.close()' />";
            strContent = strContent + "</div>";
            strContent = strContent + "<div style='width:100%;'>";
            strContent = strContent + document.getElementById('<%= hndGridViewPrintContent.ClientID %>').value;
            strContent = strContent + "</div>";
            strContent = strContent + "</body>";
            printWindow.document.write(strContent);
            printWindow.document.close();
            printWindow.focus();
        }
    </script>
    <script language="javascript" type="text/javascript">
        var sessionTimeoutWarning =
"<%= System.Configuration.ConfigurationManager.AppSettings
	["SessionWarning"].ToString()%>";
        var sessionTimeout = "<%= Session.Timeout %>";
        var timeOnPageLoad = new Date();
        var sessionWarningTimer = null;
        var redirectToWelcomePageTimer = null;
        var logoutSessionClear = 0;
        //For warning
        var sessionWarningTimer = setTimeout('SessionWarning()',
				parseInt(sessionTimeoutWarning) * 15 * 60 * 1000);
        //To redirect to the welcome page
        var redirectToWelcomePageTimer = setTimeout('RedirectToWelcomePage()',
					parseInt(sessionTimeout) * 15 * 60 * 1000);

        //Session Warning
        function SessionWarning() {
            //minutes left for expiry
            var minutesForExpiry = (parseInt(sessionTimeout) -
					parseInt(sessionTimeoutWarning));
            var message = "Your session will expire in another " +
		minutesForExpiry + " mins. Do you want to extend the session?";

            //Confirm the user if he wants to extend the session
            answer = confirm(message);

            //if yes, extend the session.
            if (answer) {
                var img = new Image(1, 1);
                img.src = 'KeepAlive.aspx?date=' + escape(new Date());

                //Clear the RedirectToWelcomePage method
                if (redirectToWelcomePageTimer != null) {
                    clearTimeout(redirectToWelcomePageTimer);
                }
                //reset the time on page load
                timeOnPageLoad = new Date();
                sessionWarningTimer = setTimeout('SessionWarning()',
				parseInt(sessionTimeoutWarning) * 15 * 60 * 1000);
                //To redirect to the welcome page
                redirectToWelcomePageTimer = setTimeout
		('RedirectToWelcomePage()', parseInt(sessionTimeout) * 15 * 60 * 1000);
            }
            else {
                logoutSessionClear = 1;
            }

            //*************************
            //Even after clicking ok(extending session) or cancel button, 
            //if the session time is over. Then exit the session.
            var currentTime = new Date();
            //time for expiry
            var timeForExpiry = timeOnPageLoad.setMinutes(timeOnPageLoad.getMinutes() +
				parseInt(sessionTimeout));

            //Current time is greater than the expiry time
            if (Date.parse(currentTime) > timeForExpiry) {
                alert("Session expired. You will be redirected to welcome page");

                window.location = "index.aspx";
            }
            //**************************
        }

        //Session timeout
        function RedirectToWelcomePage() {
            alert("Session expired. You will be redirected to Login page");
            if (logoutSessionClear == 1) {

            }
            window.location = "index.aspx";
        }

        function OpenLink() {
            var link = document.getElementById('<%=txtLink.ClientID%>').value;
            window.open(link);
        }

    </script>
    <form id="form1" runat="server">
    <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajax:ToolkitScriptManager>
    <div>
        <div class="maindiv">
            <div class="header">
            </div>
            <div class="middlediv">
                <div style="text-align: right">
                    <asp:Button ID="btn_SignUp" runat="server" Text="Sign Up" CssClass="btnsmall" OnClick="btn_SignUp_Click" />
                    <asp:Button ID="btn_Logoff" runat="server" Text="Log Off" CssClass="btnsmall" OnClick="btn_Logoff_Click" />
                </div>
                <div id="trError" runat="server" visible="false" class="centerdiv">
                    <asp:Label ID="lblError" runat="server" Text="Invalid input data found." CssClass="error-message"></asp:Label>
                </div>
                <ajax:TabContainer ID="tabLoad" runat="server" TabStripPlacement="Top" ActiveTabIndex="-1">
                    <ajax:TabPanel ID="tbpnluser" runat="server">
                        <HeaderTemplate>
                            Load
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="pnlLoad" runat="server">
                                <table align="left" width="100%">
                                    <tr>
                                        <td width="100%" align="left">
                                            <b>Drop and opportunity from the Email here.</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="left">
                                            <asp:TextBox ID="txtData" runat="server" TextMode="MultiLine" CssClass="txtMultiline"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="right">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel ID="tbpnlusrdetails" runat="server">
                        <HeaderTemplate>
                            Opportunities
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="pnlOpportunities" runat="server">
                                <table align="left" width="100%">
                                    <tr>
                                        <td>
                                            <div class="content">
                                                <asp:CheckBox ID="chkShowHidden" runat="server" Text="Show Hidden" OnCheckedChanged="chkShowHidden_CheckedChanged"
                                                    AutoPostBack="true" />
                                                <asp:HiddenField ID="hndGridViewPrintContent" runat="server" />
                                                <%--<asp:Button runat="server" ID="Print" OnClick="Print_Click" Text="Print" />--%>
                                                <input type="button" value="Print" onclick="PrintGridView()" class="black" />
                                                <asp:GridView ID="grd_main" runat="server" AllowPaging="True" AllowSorting="True"
                                                    AutoGenerateColumns="false" GridLines="Horizontal" OnPageIndexChanging="grd_main_PageIndexChanging"
                                                    OnRowDataBound="grd_main_RowDataBound" EmptyDataText="No Opportunity Found."
                                                    Width="100%" BackColor="White" CellPadding="4">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="OIsHidden" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                    Text="Hidden"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <img id="imgIsHidden" runat="server" src="~/images/Tick.png" alt="" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="OTitle" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                    Text="Title"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%#Eval("OTitle")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="OPrime" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                    Text="Prime"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%#Eval("OPrime")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="Priority" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                    Text="Priority"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%#Eval("Priority")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="OSOL" runat="server" ToolTip="Sort" OnClick="lnksort_Click" Text="SOL"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%#Eval("OSOL")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="ODueDate" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                    Text="Due Date"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%#Eval("ODueDate")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="OSB" runat="server" ToolTip="Sort" OnClick="lnksort_Click" Text="S/B"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%#Eval("OSB")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="OAssignedto" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                    Text="Assigned"></asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%#Eval("OAssignedto")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <span style="color: #898989;">Actions</span>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnk_update" runat="server" CommandArgument='<%#Eval("OpportunityId")%>'
                                                                    OnClick="btn_click"><img src="img/edit.png" alt="" /></asp:LinkButton>
                                                                <asp:LinkButton ID="lnk_delete" runat="server" CommandArgument='<%#Eval("OpportunityId")%>'
                                                                    OnClick="btn_click" OnClientClick="javascript:return confirm('Are you sure want to delete this record?');"><img src="img/delete.png" alt="" /></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="rowcss" BackColor="White" ForeColor="#333333" />
                                                    <AlternatingRowStyle CssClass="altrowcss" />
                                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                                    <HeaderStyle CssClass="headercss" />
                                                    <RowStyle CssClass="rowcss" />
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <PagerStyle CssClass="pagination" HorizontalAlign="Right" />
                                                </asp:GridView>
                                                <div id="div1" runat="server" visible="false">
                                                    <asp:GridView ID="grdView1" runat="server" AllowPaging="false" AllowSorting="True"
                                                        AutoGenerateColumns="false" GridLines="Horizontal" EmptyDataText="No Opportunity Found."
                                                        Width="100%" BackColor="White" CellPadding="4">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="OTitle" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                        Text="Title"></asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%#Eval("OTitle")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="OPrime" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                        Text="Prime"></asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%#Eval("OPrime")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="Priority" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                        Text="Priority"></asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%#Eval("Priority")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="OSOL" runat="server" ToolTip="Sort" OnClick="lnksort_Click" Text="SOL"></asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%#Eval("OSOL")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="ODueDate" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                        Text="Due Date"></asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%#Eval("ODueDate")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="OSB" runat="server" ToolTip="Sort" OnClick="lnksort_Click" Text="S/B"></asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%#Eval("OSB")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="OAssignedto" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                                                        Text="Assigned"></asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%#Eval("OAssignedto")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="rowcss" BackColor="White" ForeColor="#333333" />
                                                        <AlternatingRowStyle CssClass="altrowcss" />
                                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                        <SortedDescendingHeaderStyle BackColor="#275353" />
                                                        <HeaderStyle CssClass="headercss" />
                                                        <RowStyle CssClass="rowcss" />
                                                        <PagerSettings Mode="NumericFirstLast" />
                                                        <PagerStyle CssClass="pagination" HorizontalAlign="Right" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajax:TabPanel>
                    <ajax:TabPanel ID="tbpnljobdetails" runat="server">
                        <HeaderTemplate>
                            Opportunity
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="pnlOpportunity" runat="server">
                                <table align="left" width="100%">
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>NAICS :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td align="left" width="8%">
                                                        <asp:TextBox ID="txtNAICS" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                    <td width="8%" align="right" style="padding-right: 3px">
                                                        <span>Priority :</span>
                                                    </td>
                                                    <td width="5%" align="right" style="padding-right: 3px">
                                                        <asp:TextBox ID="txtPriority" runat="server" CssClass="txtSingleGreen" MaxLength="2"></asp:TextBox>
                                                    </td>
                                                    <td width="5%" align="right" style="padding-right: 3px">
                                                        <span>SOL :</span>
                                                    </td>
                                                    <td width="16%" align="right" style="padding-right: 3px">
                                                        <asp:TextBox ID="txtSOL" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                    <td width="8%" align="right" style="padding-right: 3px">
                                                        <span>R-Type :</span>
                                                    </td>
                                                    <td width="8%">
                                                        <asp:DropDownList ID="ddlRType" runat="server">
                                                            <asp:ListItem Text="-None-" Value="0" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="RFI" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="RFQ" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td width="6%" align="right" style="padding-right: 3px">
                                                        <span>Date :</span>
                                                    </td>
                                                    <td width="9%">
                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                    <td width="6%" align="right" style="padding-right: 3px">
                                                        <span>S/B :</span>
                                                    </td>
                                                    <td width="9%">
                                                        <asp:DropDownList ID="ddlSB" runat="server">
                                                            <asp:ListItem Text="-None-" Value="0" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="SB" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="VOSB" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="SDVOSB" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="8A" Value="4"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td width="6%" align="right" style="padding-right: 3px">
                                                        <span>Type :</span>
                                                    </td>
                                                    <td width="7%">
                                                        <asp:TextBox ID="txtType" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Title :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td width="85%">
                                                        <asp:TextBox ID="txtTitle" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                    <td width="6%" align="right" style="padding-right: 3px">
                                                        <span>Hide :</span>
                                                    </td>
                                                    <td width="9%">
                                                        <asp:CheckBox ID="chkHide" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Category :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtCategory" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Department :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Address :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtAddress" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Contact :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtContact" runat="server" CssClass="txtSingleGreen"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Link :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtLink" runat="server" CssClass="txtSingleGreen" ForeColor="Blue"
                                                            Font-Underline="True" onclick="OpenLink()" onmouseover="this.style.cursor='hand'"></asp:TextBox>
                                                        <%--<asp:TextBox ID="txtLink" runat="server" CssClass="txtSingleGreen"></asp:TextBox>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Prime :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtPrime" runat="server" CssClass="txtSingle"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Description :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="txtMultilineGreen"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Due Date :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td align="left" width="25%">
                                                        <asp:UpdatePanel ID="UpdatePnlDueDate" runat="server">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtDueDate" runat="server" CssClass="txtSingle" Width="50%"></asp:TextBox>
                                                                <asp:ImageButton ID="imgDueDate" runat="server" ImageUrl="~/images/Calendar.png" />
                                                                <ajax:CalendarExtender runat="server" TargetControlID="txtDueDate" Format="dd/MM/yyyy"
                                                                    PopupButtonID="imgDueDate">
                                                                </ajax:CalendarExtender>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td width="10%" align="right" style="padding-right: 3px">
                                                        <span>Assigned to :</span>
                                                    </td>
                                                    <td width="15%">
                                                        <asp:TextBox ID="txtAssignedTo" runat="server" CssClass="txtSingle"></asp:TextBox>
                                                    </td>
                                                    <td width="50%">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="10%">
                                            <span>Notes :</span>
                                        </td>
                                        <td align="left" width="90%">
                                            <table align="left" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="txtMultiline"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <asp:HiddenField ID="hf_OpportunityId" runat="server" Value="0" />
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajax:TabPanel>
                </ajax:TabContainer>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
