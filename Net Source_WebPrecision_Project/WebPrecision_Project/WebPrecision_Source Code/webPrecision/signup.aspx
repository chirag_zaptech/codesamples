<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="signup.aspx.cs" Inherits="webPrecision.signup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Log in Screen</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="themes/style.css" />
</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div>
        <div class="maindiv">
            <div class="header">
            </div>
            <div class="middlediv">
                <table width="100%">
                    <tr>
                        <td width="25%">
                            &nbsp;
                        </td>
                        <td width="50%">
                            <div class="information">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="validate_group"
                                    ShowMessageBox="false" ShowSummary="false" />
                                <table width="100%" style="text-align: left;">
                                    <tr>
                                        <td width="50%" style="text-align: right;">
                                            <span class="span">* User Name</span>
                                        </td>
                                        <td width="50%">
                                            <asp:TextBox ID="txt_UserName" runat="server" MaxLength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Req_txt_UserName" runat="server" ControlToValidate="txt_UserName"
                                                CssClass="error-message" Display="Dynamic" ErrorMessage="UserName is required."
                                                ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" style="text-align: right;">
                                            <span class="span">* Email Id</span>
                                        </td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtEmailId" runat="server" MaxLength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqEmailId" runat="server" ControlToValidate="txtEmailId"
                                                CssClass="error-message" Display="Dynamic" ErrorMessage="Email Id is required."
                                                ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regEmailId" runat="server" ControlToValidate="txtEmailId"
                                                CssClass="error-message" Display="Dynamic" ErrorMessage="Invalid Email" SetFocusOnError="True"
                                                ValidationGroup="validate_group" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" style="text-align: right;">
                                            <span class="span">* User Password</span>
                                        </td>
                                        <td width="50%">
                                            <asp:TextBox ID="txt_UserPassword" runat="server" MaxLength="50" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Req_txt_UserPassword" runat="server" ControlToValidate="txt_UserPassword"
                                                CssClass="error-message" Display="Dynamic" ErrorMessage="Password required."
                                                ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" style="text-align: right;">
                                            <span class="span">* Retype Password</span>
                                        </td>
                                        <td width="50%">
                                            <asp:TextBox ID="txt_RetypePassword" runat="server" MaxLength="50" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Req_txt_RetypePassword" runat="server" ControlToValidate="txt_RetypePassword"
                                                CssClass="error-message" Display="Dynamic" ErrorMessage="Password required."
                                                ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cmv_txt_RetypePassword" runat="server" ControlToCompare="txt_UserPassword"
                                                CssClass="error-message" ControlToValidate="txt_RetypePassword" Display="Dynamic"
                                                ErrorMessage="Password doesn't match" ValidationGroup="validate_group"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" style="text-align: right;">
                                            <asp:HiddenField ID="hd_primaryid" runat="server" />
                                            <asp:Button ID="btn_submit" runat="server" Text="Save" CssClass="btn" OnClick="btn_submit_Click"
                                                ValidationGroup="validate_group" CausesValidation="True" />
                                        </td>
                                        <td width="50%">
                                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" OnClick="btnBack_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="25%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
