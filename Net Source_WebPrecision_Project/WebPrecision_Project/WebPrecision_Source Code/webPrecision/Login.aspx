﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="webPrecision.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Log in Screen</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="themes/style.css" />
</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div>
        <div class="maindiv">
            <div class="header">
            </div>
            <div class="middlediv">
                <table width="100%">
                    <tr>
                        <td width="25%">
                            &nbsp;
                        </td>
                        <td width="50%">
                            <div class="information">
                                <table width="100%" style="text-align: left;">
                                    <tr>
                                        <td width="100%" colspan="2" style="text-align: center;">
                                            <span style="font-size: 16px; font-weight: bold; color: #455a21; font-family: Arial, Helvetica, sans-serif">
                                                Please identify yourself</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" style="text-align: right;">
                                            <span class="span">Username :</span>
                                        </td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtlogin" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" style="text-align: right;">
                                            <span class="span">Password :</span>
                                        </td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtpass" runat="server" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" colspan="2" style="text-align: center;">
                                            <asp:Button ID="btn_submit" runat="server" Text="Sign In" CssClass="btn" OnClick="btn_submit_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" colspan="2" style="text-align: center;">
                                            <asp:LinkButton ID="lb_ForgotPassword" runat="server" Text="Forgot Password ?" OnClick="lb_ForgotPassword_Click"
                                                Style="font-size: 14px; font-weight: bold; color: #455a21; font-family: Arial, Helvetica, sans-serif"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                                <div id="divErrorMsg" runat="server" visible="false" style="font-size: 14px; font-weight: bold;
                                    color: Red; font-family: Arial, Helvetica, sans-serif; text-align: center;">
                                    <ul>
                                        <li id="liError" runat="server" style="list-style: none; text-align: center;"></li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                        <td width="25%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
