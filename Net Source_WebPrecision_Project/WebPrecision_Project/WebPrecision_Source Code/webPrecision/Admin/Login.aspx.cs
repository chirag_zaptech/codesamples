﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webPrecision.BusinessAccess;

namespace webPrecision.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        #region "Page Events"
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region "Click Events"
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            if (txtlogin.Text.Trim() != "" && txtpass.Text.Trim() != "")
            {
                if (CheckValidUser())
                {
                    Response.Redirect("Home.aspx");
                }
            }
            else
            {
                Response.Write("<script>alert('Login Name & Password can not be blank.')</script>");
            }
        }
        #endregion

        #region Functions
        bool CheckValidUser()
        {
            string username = txtlogin.Text.Trim();
            string Password = GlobalFunctions.Encrypt(txtpass.Text.Trim());

            int AdminID = BeanHelper.UserMasterBean.IsAdminUserExist(username, Password);
            if (AdminID > 0)
            {
                Session["AdminID"] = AdminID;
                Session["AdminUserName"] = txtlogin.Text.Trim();
                GlobalFunctions.ToSession("Loggedin", "true");
                return true;
            }
            else
            {
                Response.Write("<script>alert('Invalid User.')</script>");
                return false;
            }
        }
        #endregion
    }
}