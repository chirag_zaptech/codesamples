﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="Home.aspx.cs" Inherits="webPrecision.Admin.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="white">
        <h1>
            <img src="img/icons/dashboard.png" alt="" />
            Dashboard
        </h1>
        <div class="bloc left h1">
            <div>
                <div class="title">
                    Quick Link(s)
                </div>
                <div class="content dashboard">
                    <div class="center">
                        <a id="hp_Users" class="shortcut" href="UserMaster.aspx">
                            <img src="img/user.png" alt="" width="48" height="48" />
                            User(s)</a><a id="hp_EmailSettings" class="shortcut" href="emailsettings.aspx">
                                <img src="img/EmailSettings.png" alt="" width="48" height="48" />
                                Email Setting </a>
                        <div class="cb">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cb">
        </div>
    </div>
</asp:Content>
