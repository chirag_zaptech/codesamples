﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webPrecision.BusinessAccess;
using System.Data;

namespace webPrecision.Admin
{
    public partial class UserMaster : System.Web.UI.Page
    {
        #region Variables
        static int shortway = 0;
        #endregion

        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            GlobalFunctions.LoginCheck(Convert.ToString(Request.Url.LocalPath.Replace("/Admin/", string.Empty)));
            if (!IsPostBack)
            {
                bindgrid();
            }
        }
        #endregion

        #region Grid function
        public void bindgrid()
        {
            pnl_add.Visible = false;
            pnl_list.Visible = true;

            DataTable Grid_tbl = new DataTable();
            if (string.IsNullOrEmpty(Convert.ToString(ViewState["search"])))
                Grid_tbl = BeanHelper.UserMasterBean.SearchRecord(string.Empty);
            else
                Grid_tbl = BeanHelper.UserMasterBean.SearchRecord(Convert.ToString(ViewState["search"]));

            GlobalFunctions.SetPaging(ref grd_main);
            if (Grid_tbl.Rows.Count > 0)
            {
                lbl_total.Text = Convert.ToString(Grid_tbl.Rows.Count);
                if (ViewState["lastsorton"] != null)
                {
                    DataView dv = new DataView(Grid_tbl);
                    dv.Sort = Convert.ToString(ViewState["lastsorton"]);

                    grd_main.DataSource = dv;
                    grd_main.DataBind();

                    GridView1.DataSource = dv;
                    GridView1.DataBind();
                }
                else
                {
                    grd_main.DataSource = Grid_tbl;
                    grd_main.DataBind();

                    GridView1.DataSource = Grid_tbl;
                    GridView1.DataBind();
                }
            }
            else
            {
                grd_main.DataSource = null;
                grd_main.DataBind();

                GridView1.DataSource = null;
                GridView1.DataBind();

                lbl_total.Text = "0";
            }

            Grid_tbl = null;
        }
        protected void lnksort_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                if (lnk != null)
                {
                    if (shortway % 2 == 0)
                        ViewState["lastsorton"] = Convert.ToString(lnk.ID) + " ASC";
                    else
                        ViewState["lastsorton"] = Convert.ToString(lnk.ID) + " DESC";

                    bindgrid();
                    shortway++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Convert.ToString(ex));
            }
        }
        protected void grd_main_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grd_main.PageIndex = e.NewPageIndex;
            bindgrid();
        }
        #endregion

        #region Click Events
        protected void btn_export_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GlobalFunctions.Export("UserMaster.xls", GridView1);
            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            ViewState["search"] = string.Empty;
            string strSearchStr = string.Empty;

            if (!string.IsNullOrEmpty(txt_UserSearch.Text.Trim()))
                strSearchStr = " UserName Like ''%" + txt_UserSearch.Text.Trim().Replace("'", "") + "%''";

            ViewState["search"] = strSearchStr;
            bindgrid();
        }
        protected void btn_Cancelsearch_Click(object sender, EventArgs e)
        {
            txt_UserSearch.Text = string.Empty;

            ViewState["search"] = string.Empty;
            bindgrid();
        }

        protected void btn_add_Click(object sender, EventArgs e)
        {
            lt_caption.Text = "Add User";

            pnl_add.Visible = true;
            pnl_list.Visible = false;
            clearcontrol();
        }
        protected void btn_click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            if (lnk.ID == "lnk_update")
            {
                lt_caption.Text = "Edit User";
                pnl_add.Visible = true;
                pnl_list.Visible = false;

                string id = Convert.ToString(lnk.CommandArgument);
                if (!string.IsNullOrEmpty(id))
                {
                    DataRow dr = BeanHelper.UserMasterBean.GetRecord(Convert.ToInt64(id));
                    hd_primaryid.Value = id;
                    txt_UserName.Text = Convert.ToString(dr["UserName"]);
                    txt_EmailId.Text = Convert.ToString(dr["EmailId"]);
                    txt_UserPassword.Text = GlobalFunctions.Decrypt(Convert.ToString(dr["UserPassword"]));
                    rblUserType.SelectedValue = Convert.ToString(dr["UserType"]);
                    cbIsActive.Checked = Convert.ToString(dr["IsActive"]) == "1" ? true : false;
                }
            }
            else if (lnk.ID == "lnk_delete")
            {
                pnl_add.Visible = false;
                pnl_list.Visible = true;

                string id = Convert.ToString(lnk.CommandArgument);
                if (!string.IsNullOrEmpty(id))
                {
                    int returnid = BeanHelper.UserMasterBean.DeleteRecord(Convert.ToInt64(id));
                    bindgrid();
                }
            }
        }
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            webPrecision.Entities.UserMaster objUserMaster = new webPrecision.Entities.UserMaster();
            if (!string.IsNullOrEmpty(txt_UserName.Text.Trim()) && !string.IsNullOrEmpty(txt_UserPassword.Text.Trim()))
            {
                if (string.IsNullOrEmpty(hd_primaryid.Value))
                {
                    string UserName_Id = "0";
                    DataTable objnamecheck = BeanHelper.UserMasterBean.CheckNameForInsert(txt_UserName.Text.Trim());
                    if (objnamecheck.Rows.Count > 0)
                    {
                        if (Convert.ToString(objnamecheck.Rows[0][0]) != "0")
                        {
                            UserName_Id = Convert.ToString(objnamecheck.Rows[0][0]);
                        }
                    }
                    objnamecheck = null;
                    if (UserName_Id == "0")
                    {
                        objUserMaster.UserName = txt_UserName.Text.Trim();
                        objUserMaster.EmailId = txt_EmailId.Text.Trim();
                        objUserMaster.UserPassword = GlobalFunctions.Encrypt(txt_UserPassword.Text.Trim());
                        objUserMaster.UserType = Convert.ToInt32(rblUserType.SelectedValue);
                        objUserMaster.IsActive = cbIsActive.Checked == true ? 1 : 0;

                        BeanHelper.UserMasterBean.UserMasterObject = objUserMaster;
                        int insertid = BeanHelper.UserMasterBean.InsertRecord();
                        bindgrid();
                    }
                    else if (UserName_Id != "0")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "InsertName", "alert('User Name already exist.');", true);
                    }
                }
                else if (!string.IsNullOrEmpty(hd_primaryid.Value))
                {
                    string UserName_Id = "0";
                    DataTable objnamecheck = BeanHelper.UserMasterBean.CheckNameForUpdate(txt_UserName.Text.Trim(), hd_primaryid.Value);
                    if (objnamecheck.Rows.Count > 0)
                    {
                        if (Convert.ToString(objnamecheck.Rows[0][0]) != "0")
                        {
                            UserName_Id = Convert.ToString(objnamecheck.Rows[0][0]);
                        }
                    }
                    objnamecheck = null;
                    if (UserName_Id == "0")
                    {
                        objUserMaster.UserId = Int64.Parse(hd_primaryid.Value);
                        objUserMaster.UserName = txt_UserName.Text.Trim();
                        objUserMaster.EmailId = txt_EmailId.Text.Trim();
                        objUserMaster.UserPassword = GlobalFunctions.Encrypt(txt_UserPassword.Text.Trim());
                        objUserMaster.UserType = Convert.ToInt32(rblUserType.SelectedValue);
                        objUserMaster.IsActive = cbIsActive.Checked == true ? 1 : 0;

                        BeanHelper.UserMasterBean.UserMasterObject = objUserMaster;
                        int updateid = BeanHelper.UserMasterBean.UpdateRecord();
                        bindgrid();
                    }
                    else if (UserName_Id != "0")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateName", "alert('Name already assign to other User.');", true);
                    }
                }
            }

        }
        #endregion

        #region clear control and cancel event
        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            clearcontrol();
            bindgrid();
        }
        public void clearcontrol()
        {
            hd_primaryid.Value = string.Empty;

            txt_UserName.Text = string.Empty;
            txt_EmailId.Text = string.Empty;
            txt_UserPassword.Text = string.Empty;
            txt_RetypePassword.Text = string.Empty;
            rblUserType.SelectedValue = "2";
            cbIsActive.Checked = true;
        }
        #endregion
    }
}
