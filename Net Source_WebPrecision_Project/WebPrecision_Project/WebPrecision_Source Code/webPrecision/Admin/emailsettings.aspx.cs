﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using webPrecision.BusinessAccess;
using webPrecision.Entities;
using System.Data;

namespace webPrecision.Admin
{
    public partial class emailsettings : System.Web.UI.Page
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            GlobalFunctions.LoginCheck(Convert.ToString(Request.Url.LocalPath.Replace("/Admin/", string.Empty)));
            if (!IsPostBack)
            {
                DataTable EmailSettings = BeanHelper.EmailSettingsBean.GetRecord();
                if (EmailSettings.Rows.Count > 0)
                {
                    txtSMTPUserName.Text = Convert.ToString(EmailSettings.Rows[0]["SMTPUserName"]);
                    txtSMTPPassword.Text = GlobalFunctions.Decrypt(Convert.ToString(EmailSettings.Rows[0]["SMTPPassword"]));
                    txtSMTPPassword.Attributes.Add("Password", GlobalFunctions.Decrypt(Convert.ToString(EmailSettings.Rows[0]["SMTPPassword"])));
                    txtSMTPServer.Text = Convert.ToString(EmailSettings.Rows[0]["SMTPServer"]);
                    txtSMTPPort.Text = Convert.ToString(EmailSettings.Rows[0]["SMTPPort"]);
                    cbEnableSSL.Checked = Convert.ToBoolean(Convert.ToString(EmailSettings.Rows[0]["EnableSSL"]));
                }
            }
        }
        #endregion

        #region Click Events
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtSMTPPort.Text.Trim() == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('SMTP Port No. cannot be Zero.');", true);
                return;
            }

            EmailSettings objEmailSettings = new EmailSettings();
            objEmailSettings.SMTPUserName = txtSMTPUserName.Text.Trim();
            objEmailSettings.SMTPPassword = GlobalFunctions.Encrypt(txtSMTPPassword.Text.Trim());
            objEmailSettings.SMTPServer = txtSMTPServer.Text.Trim();
            objEmailSettings.SMTPPort = Convert.ToInt32(txtSMTPPort.Text.Trim());
            objEmailSettings.EnableSSL = cbEnableSSL.Checked;

            BeanHelper.EmailSettingsBean.EmailSettingsObject = objEmailSettings;
            int insertid = BeanHelper.EmailSettingsBean.InsertRecord();

            if (insertid > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('Email Settings updated Successfully.');", true);
            }
        }
        protected void btnTestEmail_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage message = new System.Net.Mail.MailMessage();
                MailAddress mailfrom = new MailAddress(txtSMTPUserName.Text.Trim());
                message.From = mailfrom;
                message.To.Add(txtSMTPUserName.Text.Trim());
                message.Body = "Test Email.";
                message.IsBodyHtml = true;
                message.Subject = "Test Email";

                string sError = string.Empty;
                if (GlobalFunctions.SendMail(message, this.txtSMTPUserName.Text.Trim().Replace("'", string.Empty), this.txtSMTPPassword.Text.Trim().Replace("'", string.Empty), this.txtSMTPServer.Text.Trim().Replace("'", string.Empty), Convert.ToInt32(this.txtSMTPPort.Text.Trim()), cbEnableSSL.Checked, ref sError))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('Email sent Successfully.');", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('Email failed : '" + sError + " );", true);
            }
            catch (Exception Ex)
            { ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('Email failed : '" + Ex.Message + ");", true); }
        }
        #endregion
    }
}
