﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webPrecision.BusinessAccess;
using System.Data;
using webPrecision.BusinessAccess.Bean;

namespace webPrecision.Admin
{
    public partial class Home : System.Web.UI.Page
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            GlobalFunctions.LoginCheck(Convert.ToString(Request.Url.LocalPath.Replace("/Admin/", string.Empty)));

            if (!IsPostBack)
            {
                
            }
        }
        #endregion
    }
}