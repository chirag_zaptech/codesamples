﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webPrecision.BusinessAccess;
using System.Web.UI.HtmlControls;

namespace webPrecision.Admin
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string nvurl = "", urlname = string.Empty;
            urlname = webPrecision.BusinessAccess.GlobalFunctions.GetFileNameFromURL(Request.ServerVariables["URL"].ToString());
            if (urlname != "")
            {
                foreach (Control ctrl in form1.Controls)
                {
                    if (ctrl.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlGenericControl")
                    {
                        HtmlGenericControl obj = (HtmlGenericControl)ctrl;
                        string parid = obj.ID.ToString();
                        foreach (Control ctrl1 in obj.Controls)
                        {
                            if (ctrl1.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlGenericControl")
                            {
                                string id = ctrl1.ID.ToString();
                                foreach (Control innerctrl in ctrl1.Controls)
                                {
                                    if (innerctrl.GetType().ToString() == "System.Web.UI.WebControls.HyperLink")
                                    {
                                        HyperLink objhyper = (HyperLink)innerctrl;
                                        if (objhyper.NavigateUrl.ToString() != "")
                                        {
                                            string navurl = objhyper.NavigateUrl.ToString();

                                            string[] myarr = navurl.Split(("/").ToCharArray());
                                            if (myarr.Length > 0)
                                            {
                                                if (myarr[myarr.Length - 1].ToString() != "")
                                                {
                                                    nvurl = myarr[myarr.Length - 1].ToString();
                                                }
                                            }

                                            if (nvurl.Equals((urlname.ToLower())))
                                            {
                                                HtmlGenericControl ht = (HtmlGenericControl)ctrl1.FindControl(id);
                                                if (ht != null)
                                                {
                                                    string myid = ht.ID.ToString();
                                                    ht.Attributes.Add("class", "current");
                                                }
                                                HtmlGenericControl parentli = (HtmlGenericControl)ctrl.FindControl(parid);
                                                if (parentli != null)
                                                {
                                                    string myid = parentli.ID.ToString();
                                                    parentli.Attributes.Add("class", "current hover");
                                                }
                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }
                }// end of for each loop
            }// end of if urlname blank check
        }
        protected void lnk_logout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            GlobalFunctions.RemoveSession("Loggedin");
            Response.Redirect("Login.aspx");            
        }
    }
}
