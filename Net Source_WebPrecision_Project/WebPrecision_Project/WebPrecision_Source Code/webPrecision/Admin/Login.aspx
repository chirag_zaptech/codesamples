﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="webPrecision.Admin.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Login</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="js/jwysiwyg/jquery.wysiwyg.old-school.css" />

    <script type="text/javascript" src="ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>

    <script type="text/javascript" src="ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="content" class="login">
        <h1>
            <img src="img/icons/lock-closed.png" alt="" />Admin Panel</h1>
        <div class="input">
            <asp:TextBox ID="txtlogin" runat="server"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="twe_txtlogin" runat="server" WatermarkText="Login"
                TargetControlID="txtlogin">
            </cc1:TextBoxWatermarkExtender>
        </div>
        <div class="input">
            <asp:TextBox ID="txtpass" runat="server" TextMode="Password"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="twe_txtpass" runat="server" WatermarkText="Password"
                TargetControlID="txtpass">
            </cc1:TextBoxWatermarkExtender>
        </div>
        <div class="submit">
            <asp:Button ID="btn_submit" runat="server" Text="Login" OnClick="btn_submit_Click" />
        </div>
    </div>
    </form>
</body>
</html>
