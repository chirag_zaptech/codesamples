﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="emailsettings.aspx.cs" Inherits="webPrecision.Admin.emailsettings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="white">
        <h1>
            <img src="img/EmailSettings.png" width="32px" height="32px" alt="" />
            E-Mail Setting</h1>
        <br />
        <div class="bloc" style="margin-top: -2px;">
            <div class="title">
                <asp:Literal ID="lt_caption" runat="server" Text="Add/Edit Email Settings"></asp:Literal></div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="validate_group"
                ShowMessageBox="false" ShowSummary="false" />
            <div class="content">
                <div class="input medium">
                    <label for="input1">
                        * SMTP User Name</label>
                    <asp:TextBox ID="txtSMTPUserName" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Req_txtSMTPUserName" runat="server" ControlToValidate="txtSMTPUserName"
                        CssClass="error-message" Display="Dynamic" ErrorMessage="User Name is required."
                        ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                </div>
                <div class="input">
                    <label for="input2">
                        * SMTP Password</label>
                    <asp:TextBox ID="txtSMTPPassword" runat="server" TextMode="Password" MaxLength="20"
                        Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="req_txtSMTPPassword" runat="server" ControlToValidate="txtSMTPPassword"
                        CssClass="error-message" Display="Dynamic" ErrorMessage="Password is required."
                        ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                </div>
                <div class="input medium">
                    <label for="input3">
                        * SMTP Server Name</label>
                    <asp:TextBox ID="txtSMTPServer" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSMTPServer"
                        CssClass="error-message" Display="Dynamic" ErrorMessage="SMTP Server Name is required."
                        ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                </div>
                <div class="input">
                    <label for="input4">
                        * SMTP Port No.</label>
                    <asp:TextBox ID="txtSMTPPort" runat="server" MaxLength="3" Width="100px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Req_txtSMTPPort" runat="server" ControlToValidate="txtSMTPPort"
                        CssClass="error-message" Display="Dynamic" ErrorMessage="SMTP Port No is required."
                        ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender3" TargetControlID="txtSMTPPort"
                        ValidChars="0123456789." FilterMode="ValidChars">
                    </cc1:FilteredTextBoxExtender>
                </div>
                <div class="input">
                    <label for="input5">
                        * Enable SSL ?</label>
                    <asp:CheckBox ID="cbEnableSSL" runat="server" Text="" />
                </div>
                <div class="submit">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="black" OnClick="btnSubmit_Click"
                        ValidationGroup="validate_group" CausesValidation="True" />
                    <asp:Button ID="btnTestEmail" runat="server" Text="Test Mail" CssClass="black" OnClick="btnTestEmail_Click"
                        ValidationGroup="validate_group" CausesValidation="True" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
