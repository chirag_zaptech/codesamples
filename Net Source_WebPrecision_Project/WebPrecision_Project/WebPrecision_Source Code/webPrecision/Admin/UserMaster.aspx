﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="UserMaster.aspx.cs" Inherits="webPrecision.Admin.UserMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="white">
        <h1>
            <img src="img/user.png" width="32px" height="32px" alt="" />
            User Master</h1>
        <asp:Panel ID="pnl_add" runat="server">
            <br />
            <div class="bloc" style="margin-top: -2px;">
                <div class="title">
                    <asp:Literal ID="lt_caption" runat="server"></asp:Literal></div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="validate_group"
                    ShowMessageBox="false" ShowSummary="false" />
                <div class="content">
                    <div class="input">
                        <label for="input1">
                            * User Name</label>
                        <asp:TextBox ID="txt_UserName" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Req_txt_UserName" runat="server" ControlToValidate="txt_UserName"
                            CssClass="error-message" Display="Dynamic" ErrorMessage="UserName is required."
                            ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="input">
                        <label for="input1">
                            * Email Id</label>
                        <asp:TextBox ID="txt_EmailId" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Req_txt_EmailId" runat="server" ControlToValidate="txt_EmailId"
                            CssClass="error-message" Display="Dynamic" ErrorMessage="Email Id is required."
                            ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="input">
                        <label for="input1">
                            * User Password</label>
                        <asp:TextBox ID="txt_UserPassword" runat="server" MaxLength="50" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Req_txt_UserPassword" runat="server" ControlToValidate="txt_UserPassword"
                            CssClass="error-message" Display="Dynamic" ErrorMessage="Password is required."
                            ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="input">
                        <label for="input1">
                            * Retype Password</label>
                        <asp:TextBox ID="txt_RetypePassword" runat="server" MaxLength="50" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Req_txt_RetypePassword" runat="server" ControlToValidate="txt_RetypePassword"
                            CssClass="error-message" Display="Dynamic" ErrorMessage="Password is required."
                            ValidationGroup="validate_group"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CV_txt_RetypePassword" runat="server" ControlToCompare="txt_UserPassword"
                            ControlToValidate="txt_RetypePassword" CssClass="error-message" Display="Dynamic"
                            ErrorMessage="Password doesn't match." ValidationGroup="validate_group"></asp:CompareValidator>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="input">
                        <label for="input1">
                            * User Type</label>
                        <asp:RadioButtonList ID="rblUserType" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Normal" Value="2" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Admin" Value="1"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="input medium">
                        <label for="input1">
                            Is Active ?</label>
                        <asp:CheckBox ID="cbIsActive" runat="server" Checked="true" />
                    </div>
                    <div class="clear">
                    </div>
                    <div class="submit">
                        <asp:HiddenField ID="hd_primaryid" runat="server" />
                        <asp:Button ID="btn_submit" runat="server" Text="Save" CssClass="black" OnClick="btn_submit_Click"
                            ValidationGroup="validate_group" CausesValidation="True" />
                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="white" OnClick="btn_cancel_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnl_list" runat="server">
            <br />
            <div class="bloc" style="margin-top: -2px;">
                <div class="title">
                    Search</div>
                <div class="content">
                    <div class="input" style="float: left">
                        <label for="input3">
                            UserName</label>
                        <asp:TextBox ID="txt_UserSearch" runat="server" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="submit">
                        <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="black" OnClick="btn_search_Click" />
                        <asp:Button ID="btn_Cancelsearch" runat="server" Text="Clear" CssClass="white" OnClick="btn_Cancelsearch_Click" />
                    </div>
                </div>
            </div>
            <br />
            <div class="bloc" style="margin-top: -2px;">
                <div class="title">
                    Total User(s) :
                    <asp:Label ID="lbl_total" runat="server" ForeColor="#CE4B23" Font-Bold="true" Font-Size="12px"></asp:Label>
                    <div class="submit" style="float: right; padding-right: 25px; padding-top: 1px;">
                        <asp:Button ID="btn_export" runat="server" Text="Export to Excel" CssClass="black"
                            OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="content">
                    <div class="submit">
                        <asp:Button ID="btn_add" runat="server" Text="Add User" OnClick="btn_add_Click" /></div>
                    <hr />
                    <asp:GridView ID="grd_main" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="false" GridLines="None" OnPageIndexChanging="grd_main_PageIndexChanging"
                        EmptyDataText="No User(s) Found." Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="UserName" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                        Text="User Name"></asp:LinkButton>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("UserName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="EmailId" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                        Text="Email Id"></asp:LinkButton>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("EmailId")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="UserTypeName" runat="server" ToolTip="Sort" OnClick="lnksort_Click"
                                        Text="User Type"></asp:LinkButton>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("UserTypeName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <span style="font-weight: bold">Actions</span>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk_update" runat="server" CommandArgument='<%#Eval("UserId")%>'
                                        OnClick="btn_click"><img src="img/icons/actions/edit.png" alt="" /></asp:LinkButton>
                                    <asp:LinkButton ID="lnk_delete" runat="server" CommandArgument='<%#Eval("UserId")%>'
                                        OnClick="btn_click" OnClientClick="javascript:return confirm('Are you sure want to delete this record?');"><img
                                            src="img/icons/actions/delete.png" alt="" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="headercss" />
                        <PagerSettings Mode="NumericFirstLast" />
                        <PagerStyle CssClass="pagination" HorizontalAlign="Right" />
                    </asp:GridView>
                    <div style="display: none;">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" GridLines="None"
                            Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        User Name
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("UserName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Email Id
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("EmailId")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        User Type
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("UserTypeName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>