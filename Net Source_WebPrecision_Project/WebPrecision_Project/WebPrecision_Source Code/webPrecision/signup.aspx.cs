﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webPrecision.BusinessAccess;
using System.Data;

namespace webPrecision
{
    public partial class signup : System.Web.UI.Page
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region Click Events
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            webPrecision.Entities.UserMaster objUserMaster = new webPrecision.Entities.UserMaster();
            if (!string.IsNullOrEmpty(txt_UserName.Text.Trim()) && !string.IsNullOrEmpty(txt_UserPassword.Text.Trim()))
            {
                if (string.IsNullOrEmpty(hd_primaryid.Value))
                {
                    string UserName_Id = "0";
                    DataTable objnamecheck = BeanHelper.UserMasterBean.CheckNameForInsert(txt_UserName.Text.Trim());
                    if (objnamecheck.Rows.Count > 0)
                    {
                        if (Convert.ToString(objnamecheck.Rows[0][0]) != "0")
                        {
                            UserName_Id = Convert.ToString(objnamecheck.Rows[0][0]);
                        }
                    }
                    objnamecheck = null;
                    if (UserName_Id == "0")
                    {
                        objUserMaster.UserName = txt_UserName.Text.Trim();
                        objUserMaster.UserPassword = GlobalFunctions.Encrypt(txt_UserPassword.Text.Trim());
                        objUserMaster.EmailId = txtEmailId.Text.Trim();
                        objUserMaster.UserType = 0;
                        objUserMaster.IsActive = 1;

                        BeanHelper.UserMasterBean.UserMasterObject = objUserMaster;
                        int insertid = BeanHelper.UserMasterBean.InsertRecord();
                    }
                    else if (UserName_Id != "0")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "InsertName", "alert('User Name already exist.');", true);
                    }
                }
                else if (!string.IsNullOrEmpty(hd_primaryid.Value))
                {
                    string UserName_Id = "0";
                    DataTable objnamecheck = BeanHelper.UserMasterBean.CheckNameForUpdate(txt_UserName.Text.Trim(), hd_primaryid.Value);
                    if (objnamecheck.Rows.Count > 0)
                    {
                        if (Convert.ToString(objnamecheck.Rows[0][0]) != "0")
                        {
                            UserName_Id = Convert.ToString(objnamecheck.Rows[0][0]);
                        }
                    }
                    objnamecheck = null;
                    if (UserName_Id == "0")
                    {
                        objUserMaster.UserId = Int64.Parse(hd_primaryid.Value);
                        objUserMaster.UserName = txt_UserName.Text.Trim();
                        objUserMaster.UserPassword = GlobalFunctions.Encrypt(txt_UserPassword.Text.Trim());
                        objUserMaster.EmailId = txtEmailId.Text.Trim();
                        objUserMaster.UserType = 0;
                        objUserMaster.IsActive = 1;

                        BeanHelper.UserMasterBean.UserMasterObject = objUserMaster;
                        int updateid = BeanHelper.UserMasterBean.UpdateRecord();
                    }
                    else if (UserName_Id != "0")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateName", "alert('Name already assign to other User.');", true);
                    }
                }
            }

            Response.Redirect("index.aspx");
        }
        #endregion

        #region clear control and cancel event
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }
        public void clearcontrol()
        {
            hd_primaryid.Value = string.Empty;

            txt_UserName.Text = string.Empty;
            txt_UserPassword.Text = string.Empty;
            txtEmailId.Text = string.Empty;
        }
        #endregion
    }
}
