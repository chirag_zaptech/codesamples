﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webPrecision.BusinessAccess;
using System.Data;
using System.Net.Mail;

namespace webPrecision
{
    public partial class Login : System.Web.UI.Page
    {
        #region "Page Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (Request.Cookies["UName"] != null)
                //{
                //    txtlogin.Text = Request.Cookies["UName"].Value;
                //    if (Request.Cookies["PWD"] != null)
                //        txtpass.Attributes.Add("value", Request.Cookies["PWD"].Value);
                //    if (Request.Cookies["UName"] != null && Request.Cookies["PWD"] != null)
                //        cbRemember.Checked = true;
                //}
            }
        }
        #endregion

        #region "Click Events"
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            divErrorMsg.Visible = false;
            if (txtlogin.Text.Trim() != "" && txtpass.Text.Trim() != "")
            {
                if (CheckValidUser())
                {
                    //Setcookies();
                    Response.Redirect("index.aspx");
                }
            }
            else
            {
                divErrorMsg.Visible = true;
                liError.InnerHtml = "Login Name & Password can not be blank.";
                //Response.Write("<script>alert('Login Name & Password can not be blank.')</script>");
            }
        }
        protected void lb_ForgotPassword_Click(object sender, EventArgs e)
        {
            divErrorMsg.Visible = false;
            if (string.IsNullOrEmpty(txtlogin.Text.Trim()))
            {
                divErrorMsg.Visible = true;
                liError.InnerHtml = "Login Name can not be blank.";
            }
            else
            {
                DataTable dtUserMaster = BeanHelper.UserMasterBean.SearchRecord("UserName = ''" + txtlogin.Text.Trim() + "''");
                if (dtUserMaster == null || dtUserMaster.Rows.Count == 0)
                {
                    divErrorMsg.Visible = true;
                    liError.InnerHtml = "Login Name not found.";
                }
                else
                {
                    DataTable EmailSettings = BeanHelper.EmailSettingsBean.GetRecord();
                    if (EmailSettings.Rows.Count > 0)
                    {
                        MailMessage message = new System.Net.Mail.MailMessage();
                        MailAddress mailfrom = new MailAddress(Convert.ToString(EmailSettings.Rows[0]["SMTPUserName"]));
                        message.From = mailfrom;
                        message.To.Add(Convert.ToString(dtUserMaster.Rows[0]["EmailId"]));
                        message.Body = "User details for " + Convert.ToString(dtUserMaster.Rows[0]["UserName"]) + " is given below.";
                        message.Body += "\r\n\r\nUser Name : " + Convert.ToString(dtUserMaster.Rows[0]["UserName"]);
                        message.Body += "\r\n\r\nPassword : " + GlobalFunctions.Decrypt(Convert.ToString(dtUserMaster.Rows[0]["UserPassword"]));

                        message.IsBodyHtml = false;
                        message.Subject = "Forgot password : " + Convert.ToString(dtUserMaster.Rows[0]["UserName"]);

                        string sError = string.Empty;
                        if (GlobalFunctions.SendMail(message, Convert.ToString(EmailSettings.Rows[0]["SMTPUserName"]),
                                                            GlobalFunctions.Decrypt(Convert.ToString(EmailSettings.Rows[0]["SMTPPassword"])),
                                                            Convert.ToString(EmailSettings.Rows[0]["SMTPServer"]),
                                                            Convert.ToInt32(EmailSettings.Rows[0]["SMTPPort"]),
                                                            Convert.ToBoolean(Convert.ToString(EmailSettings.Rows[0]["EnableSSL"])), ref sError))
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('Email sent Successfully.');", true);
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('Email failed : '" + sError + " );", true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "edit", "alert('Email settings not found.');", true);
                }
            }
        }
        #endregion

        #region "Functions"
        bool CheckValidUser()
        {
            divErrorMsg.Visible = false;
            string username = txtlogin.Text.Trim();
            string Password = GlobalFunctions.Encrypt(txtpass.Text.Trim());

            int AdminID = BeanHelper.UserMasterBean.IsUserExist(username, Password);
            if (AdminID > 0)
            {
                Session["UserID"] = AdminID;
                Session["UserName"] = txtlogin.Text.Trim();
                GlobalFunctions.ToSession("Loggedin_front", "true");
                return true;
            }
            else
            {
                divErrorMsg.Visible = true;
                liError.InnerHtml = "Invalid User.";
                //Response.Write("<script>alert('Invalid User.')</script>");
                return false;
            }
        }
        #endregion

        //#region Remember password
        //public void Setcookies()
        //{
        //    if (this.cbRemember.Checked == true)
        //    {
        //        Response.Cookies["UName"].Value = txtlogin.Text;
        //        Response.Cookies["PWD"].Value = txtpass.Text;
        //        Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(2);
        //        Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(2);
        //    }
        //    else
        //    {
        //        Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(-1);
        //        Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(-1);
        //    }
        //}
        //#endregion
    }
}