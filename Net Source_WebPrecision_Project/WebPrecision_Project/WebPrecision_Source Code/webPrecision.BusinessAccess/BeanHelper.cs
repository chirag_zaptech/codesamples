﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webPrecision.DBAccess;
using webPrecision.BusinessAccess.Bean;

namespace webPrecision.BusinessAccess
{
    public static class BeanHelper
    {
        static DBHelper _DBHelper;
        public static DBHelper DBHelper
        {
            get
            {
                if (_DBHelper == null)
                    return _DBHelper = new DBHelper();

                return _DBHelper;
            }
            set { _DBHelper = value; }
        }

        //static AdminSettingsBean _AdminSettingsBean;
        //public static AdminSettingsBean AdminSettingsBean
        //{
        //    get
        //    {
        //        if (_AdminSettingsBean == null)
        //            return _AdminSettingsBean = new AdminSettingsBean();

        //        return _AdminSettingsBean;
        //    }
        //    set { _AdminSettingsBean = value; }
        //}

        static EmailSettingsBean _EmailSettingsBean;
        public static EmailSettingsBean EmailSettingsBean
        {
            get
            {
                if (_EmailSettingsBean == null)
                    return _EmailSettingsBean = new EmailSettingsBean();

                return _EmailSettingsBean;
            }
            set { _EmailSettingsBean = value; }
        }        

        //static EmailTemplateBean _EmailTemplateBean;
        //public static EmailTemplateBean EmailTemplateBean
        //{
        //    get
        //    {
        //        if (_EmailTemplateBean == null)
        //            return _EmailTemplateBean = new EmailTemplateBean();

        //        return _EmailTemplateBean;
        //    }
        //    set { _EmailTemplateBean = value; }
        //}        

        //static GeneralSettingsBean _GeneralSettingsBean;
        //public static GeneralSettingsBean GeneralSettingsBean
        //{
        //    get
        //    {
        //        if (_GeneralSettingsBean == null)
        //            return _GeneralSettingsBean = new GeneralSettingsBean();

        //        return _GeneralSettingsBean;
        //    }
        //    set { _GeneralSettingsBean = value; }
        //}

        static UserMasterBean _UserMasterBean;
        public static UserMasterBean UserMasterBean
        {
            get
            {
                if (_UserMasterBean == null)
                    return _UserMasterBean = new UserMasterBean();

                return _UserMasterBean;
            }
            set { _UserMasterBean = value; }
        }

        static OpportunityBean _OpportunityBean;
        public static OpportunityBean OpportunityBean
        {
            get
            {
                if (_OpportunityBean == null)
                    return _OpportunityBean = new OpportunityBean();

                return _OpportunityBean;
            }
            set { _OpportunityBean = value; }
        }
    }
}