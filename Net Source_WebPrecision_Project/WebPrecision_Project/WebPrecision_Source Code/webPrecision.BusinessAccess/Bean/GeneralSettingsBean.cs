﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webPrecision.Interfaces;
using System.Data;
using System.Collections;
using webPrecision.Entities;

namespace webPrecision.BusinessAccess.Bean
{
    public class GeneralSettingsBean : ICommonOperations, IGeneralSettings
    {
        #region Constructor
        public GeneralSettingsBean() { }
        #endregion

        #region Properties
        public GeneralSettings GeneralSettingsObject { get; set; }
        #endregion

        #region ICommonOperations Members
        public int InsertRecord()
        {
            Hashtable Paravalues = new Hashtable();
            Paravalues.Add("@Paging", GeneralSettingsObject.Paging);
            Paravalues.Add("@PageSize", GeneralSettingsObject.PageSize);

            int ID = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_GeneralSettings_Insert", Paravalues, true);
            return ID;
        }
        public int UpdateRecord()
        {
            throw new NotImplementedException();
        }
        public int DeleteRecord(Int64 ID)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IGeneralSettings Members
        public DataTable GetRecord()
        {
            return BeanHelper.DBHelper.FillTable("Exec usp_GeneralSettings_Select");
        }
        public void PageSettings(ref bool Paging, ref int PageSize)
        {
            DataTable dtPageSettings = BeanHelper.DBHelper.FillTable("Exec usp_GeneralSettings_Select");
            if (dtPageSettings != null && dtPageSettings.Rows.Count > 0)
            {
                Paging = Convert.ToBoolean(dtPageSettings.Rows[0]["Paging"]);
                PageSize = Convert.ToInt32(dtPageSettings.Rows[0]["PageSize"]);
            }
            else
            {
                Paging = true;
                PageSize = 10;
            }
        }
        #endregion
    }
}