﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using webPrecision.Entities;
using System.Data;
using webPrecision.Interfaces;

namespace webPrecision.BusinessAccess.Bean
{
    public class EmailSettingsBean
    {
        #region Constructor
        public EmailSettingsBean() { }
        #endregion

        #region Properties
        public EmailSettings EmailSettingsObject { get; set; }
        #endregion

        #region ICommonOperations Members
        public int InsertRecord()
        {
            Hashtable Paravalues = new Hashtable();
            Paravalues.Add("@SMTPUserName", EmailSettingsObject.SMTPUserName);
            Paravalues.Add("@SMTPPassword", EmailSettingsObject.SMTPPassword);
            Paravalues.Add("@SMTPServer", EmailSettingsObject.SMTPServer);
            Paravalues.Add("@SMTPPort", EmailSettingsObject.SMTPPort);
            Paravalues.Add("@EnableSSL", EmailSettingsObject.EnableSSL);

            int ID = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_EmailSettings", Paravalues, true);
            return ID;
        }
        public DataTable GetRecord()
        {
            return BeanHelper.DBHelper.FillTable("Select ID, SMTPUserName, SMTPPassword, SMTPServer, SMTPPort, EnableSSL From EmailSettings");
        }
        #endregion
    }
}
