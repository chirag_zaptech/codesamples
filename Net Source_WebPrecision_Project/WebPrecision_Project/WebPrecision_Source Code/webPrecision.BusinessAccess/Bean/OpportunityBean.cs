﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webPrecision.Interfaces;
using System.Data;
using webPrecision.Entities;
using System.Collections;

namespace webPrecision.BusinessAccess.Bean
{
    public class OpportunityBean: ICommonOperations, IOpportunity
    {
        #region Constructor
        public OpportunityBean() { }
        #endregion

        #region Properties
        public Opportunity OpportunityObject { get; set; }
        #endregion

        #region ICommonOperations Members
        public int InsertRecord()
        {
            Hashtable Paravalues = new Hashtable();

            Paravalues.Add("@OpportunityId", "0");
            Paravalues.Add("@ONAICS", OpportunityObject.ONAICS);
            Paravalues.Add("@OSOL", OpportunityObject.OSOL);
            Paravalues.Add("@ORType", OpportunityObject.ORType);
            Paravalues.Add("@ODate", OpportunityObject.ODate);
            Paravalues.Add("@OSB", OpportunityObject.OSB);
            Paravalues.Add("@OType", OpportunityObject.OType);
            Paravalues.Add("@OTitle", OpportunityObject.OTitle);
            Paravalues.Add("@OIsHide", OpportunityObject.OIsHide);
            Paravalues.Add("@OCategory", OpportunityObject.OCategory);
            Paravalues.Add("@ODepartment", OpportunityObject.ODepartment);
            Paravalues.Add("@OAddress", OpportunityObject.OAddress);
            Paravalues.Add("@OContact", OpportunityObject.OContact);
            Paravalues.Add("@OLink", OpportunityObject.OLink);
            Paravalues.Add("@OPrime", OpportunityObject.OPrime);
            Paravalues.Add("@ODescription", OpportunityObject.ODescription);
            Paravalues.Add("@ODueDate", OpportunityObject.ODueDate);
            Paravalues.Add("@OAssignedto", OpportunityObject.OAssignedto);
            Paravalues.Add("@ONotes", OpportunityObject.ONotes);
            Paravalues.Add("@Priority", OpportunityObject.Priority);
            Paravalues.Add("@Opr", "I");

            int ID = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_Opportunity", Paravalues, true);
            return ID;
        }
        public int UpdateRecord()
        {
            Hashtable Paravalues = new Hashtable();

            Paravalues.Add("@OpportunityId", OpportunityObject.OpportunityId);
            Paravalues.Add("@ONAICS", OpportunityObject.ONAICS);
            Paravalues.Add("@OSOL", OpportunityObject.OSOL);
            Paravalues.Add("@ORType", OpportunityObject.ORType);
            Paravalues.Add("@ODate", OpportunityObject.ODate);
            Paravalues.Add("@OSB", OpportunityObject.OSB);
            Paravalues.Add("@OType", OpportunityObject.OType);
            Paravalues.Add("@OTitle", OpportunityObject.OTitle);
            Paravalues.Add("@OIsHide", OpportunityObject.OIsHide);
            Paravalues.Add("@OCategory", OpportunityObject.OCategory);
            Paravalues.Add("@ODepartment", OpportunityObject.ODepartment);
            Paravalues.Add("@OAddress", OpportunityObject.OAddress);
            Paravalues.Add("@OContact", OpportunityObject.OContact);
            Paravalues.Add("@OLink", OpportunityObject.OLink);
            Paravalues.Add("@OPrime", OpportunityObject.OPrime);
            Paravalues.Add("@ODescription", OpportunityObject.ODescription);
            Paravalues.Add("@ODueDate", OpportunityObject.ODueDate);
            Paravalues.Add("@OAssignedto", OpportunityObject.OAssignedto);
            Paravalues.Add("@ONotes", OpportunityObject.ONotes);
            Paravalues.Add("@Priority", OpportunityObject.Priority);
            Paravalues.Add("@Opr", "U");

            int ID = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_Opportunity", Paravalues, true);
            return ID;
        }
        public int DeleteRecord(Int64 ID)
        {
            Hashtable Paravalues = new Hashtable();

            Paravalues.Add("@OpportunityId", ID);
            Paravalues.Add("@Opr", "D");

            int OpportunityId = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_Opportunity", Paravalues, true);
            return OpportunityId;
        }
        #endregion

        #region IOpportunity Members
        public DataTable SearchRecord(string searchstr)
        {
            return BeanHelper.DBHelper.FillTable("Exec usp_Opportunity_Search @Search = '" + searchstr + "'");
        }
        public DataRow GetRecord(Int64 OpportunityId)
        {
            return BeanHelper.DBHelper.GetDataRow("Exec usp_Opportunity @Opr = 'G', @OpportunityId = " + Convert.ToString(OpportunityId));
        }
        #endregion
    }
}