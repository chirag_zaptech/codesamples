﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using webPrecision.Entities;
using webPrecision.Interfaces;

namespace webPrecision.BusinessAccess.Bean
{
    public class UserMasterBean : ICommonOperations, IUserMaster
    {
        #region Constructor
        public UserMasterBean() { }
        #endregion

        #region Properties
        public UserMaster UserMasterObject { get; set; }
        #endregion

        #region ICommonOperations Members
        public int InsertRecord()
        {
            Hashtable Paravalues = new Hashtable();

            Paravalues.Add("@UserId", "0");
            Paravalues.Add("@UserName", UserMasterObject.UserName);
            Paravalues.Add("@UserPassword", UserMasterObject.UserPassword);
            Paravalues.Add("@UserType", UserMasterObject.UserType);
            Paravalues.Add("@EmailId", UserMasterObject.EmailId);
            Paravalues.Add("@IsActive", UserMasterObject.IsActive);
            Paravalues.Add("@Opr", "I");

            int ID = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_UserMaster", Paravalues, true);
            return ID;
        }
        public int UpdateRecord()
        {
            Hashtable Paravalues = new Hashtable();

            Paravalues.Add("@UserId", UserMasterObject.UserId);
            Paravalues.Add("@UserName", UserMasterObject.UserName);
            Paravalues.Add("@UserPassword", UserMasterObject.UserPassword);
            Paravalues.Add("@UserType", UserMasterObject.UserType);
            Paravalues.Add("@EmailId", UserMasterObject.EmailId);
            Paravalues.Add("@IsActive", UserMasterObject.IsActive);
            Paravalues.Add("@Opr", "U");

            int ID = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_UserMaster", Paravalues, true);
            return ID;
        }
        public int DeleteRecord(Int64 ID)
        {
            Hashtable Paravalues = new Hashtable();

            Paravalues.Add("@UserId", ID);
            Paravalues.Add("@Opr", "D");

            int UserID = BeanHelper.DBHelper.ExecuteStoredProcedure("usp_UserMaster", Paravalues, true);
            return UserID;
        }
        #endregion

        #region IUserMaster Members
        public DataTable SearchRecord(string searchstr)
        {
            return BeanHelper.DBHelper.FillTable("Exec usp_UserMaster_Search @Search = '" + searchstr + "'");
        }
        public DataRow GetRecord(Int64 UserId)
        {
            return BeanHelper.DBHelper.GetDataRow("Exec usp_UserMaster @Opr = 'G', @UserId = " + Convert.ToString(UserId));
        }

        public DataTable CheckNameForInsert(string Name)
        {
            return BeanHelper.DBHelper.FillTable("Exec usp_UserMaster_Select @Opr = 'CheckNameInsert', @UserName = '" + Name + "'");
        }
        public DataTable CheckNameForUpdate(string Name, string ID)
        {
            return BeanHelper.DBHelper.FillTable("Exec usp_UserMaster_Select @Opr = 'CheckNameUpdate', @UserName = '" + Name + "', @UserId = " + ID);
        }
        public int IsAdminUserExist(string UserName, string UserPassword)
        {
            return Convert.ToInt32(BeanHelper.DBHelper.ExecuteScalar("Exec usp_UserMaster_Select @Opr = 'CheckAdmin', @UserName = '" + UserName + "', @UserPassword = '" + UserPassword + "'"));
        }
        public int IsUserExist(string UserName, string UserPassword)
        {
            return Convert.ToInt32(BeanHelper.DBHelper.ExecuteScalar("Exec usp_UserMaster_Select @Opr = 'CheckUser', @UserName = '" + UserName + "', @UserPassword = '" + UserPassword + "'"));
        }
        #endregion
    }
}