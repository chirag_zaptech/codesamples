﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data;
using System.Net.Mail;
using System.Net;

namespace webPrecision.BusinessAccess
{
    public class GlobalFunctions
    {
        public static string GetFileNameFromURL(string name)
        {
            return System.IO.Path.GetFileName(name);
        }
        public static string Encrypt(string strText)
        {
            try
            {
                byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(strText);
                string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
                return returnValue;
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }
        public static string Decrypt(string strText)
        {
            try
            {
                byte[] encodedDataAsBytes = System.Convert.FromBase64String(strText);
                string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
                return returnValue;
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }
        public static void LoginCheck(string returnURL)
        {
            if (string.IsNullOrEmpty(Convert.ToString(FromSession("Loggedin"))))
            {
                HttpContext.Current.Response.Redirect("login.aspx?returnURL=" + returnURL);
            }
        }
        public static bool LoginCheckReturn()
        {
            bool blnReturn = true;
            if (string.IsNullOrEmpty(Convert.ToString(FromSession("Loggedin"))))
            {
                blnReturn = false;
            }
            return blnReturn;
        }
        public static void FrontLoginCheck(string returnURL)
        {
            if (string.IsNullOrEmpty(Convert.ToString(FromSession("Loggedin_front"))))
            {
                HttpContext.Current.Response.Redirect("login.aspx?returnURL=" + returnURL);
            }
        }
        public static bool FrontLoginCheckReturn()
        {
            bool blnReturn = true;
            if (string.IsNullOrEmpty(Convert.ToString(FromSession("Loggedin_front"))))
            {
                blnReturn = false;
            }
            return blnReturn;
        }
        public static string FromSession(string Name)
        {
            return Convert.ToString(HttpContext.Current.Session[Name]);
        }
        public static void ToSession(string Name, string Value)
        {
            HttpContext.Current.Session[Name] = Value;
        }
        public static string AddQuota(string Name)
        {
            return Name.Replace("'", "''").Trim();
        }
        public static string RemoveQuota(string Name)
        {
            return Name.Replace("''", "'").Trim();
        }

        public static void RemoveSession(string Name)
        {
            HttpContext.Current.Session.Remove(Name);
        }
        public static void RemoveAllSession()
        {
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Session.Abandon();
        }

        public static bool SendMail(MailMessage Message, ref string sError)
        {
            try
            {
                //string smtpUserName = string.Empty;
                //string smtpPassword = string.Empty;
                //string smtpServer = string.Empty;
                //int smtpPort = 0;
                //bool blnEnableSSL = false;

                //DataTable EmailSettings = BeanHelper.EmailSettingsBean.GetRecord();
                //if (EmailSettings.Rows.Count > 0)
                //{
                //    smtpUserName = Convert.ToString(EmailSettings.Rows[0]["SMTPUserName"]);
                //    smtpPassword = GlobalFunctions.Decrypt(Convert.ToString(EmailSettings.Rows[0]["SMTPPassword"]));
                //    smtpServer = Convert.ToString(EmailSettings.Rows[0]["SMTPServer"]);
                //    smtpPort = Convert.ToInt32(Convert.ToString(EmailSettings.Rows[0]["SMTPPort"]));
                //    blnEnableSSL = Convert.ToBoolean(Convert.ToString(EmailSettings.Rows[0]["EnableSSL"]));
                //}

                //NetworkCredential network_cdr = new NetworkCredential();
                //network_cdr.UserName = smtpUserName;
                //network_cdr.Password = smtpPassword;

                //SmtpClient mail_client = new SmtpClient();
                //mail_client.UseDefaultCredentials = true;
                //mail_client.Credentials = network_cdr;                  //Network Credential
                //mail_client.Port = smtpPort;                            //SMTP Port
                //mail_client.Host = smtpServer;                          //SMTP Server
                //mail_client.EnableSsl = blnEnableSSL;                   //Secure Sockets Layer(SSL)                
                ////mail_client.Timeout = 0;
                //mail_client.Send(Message);                              //Send the message

                //sError = string.Empty;
                return true;
            }
            catch (Exception Ex)
            {
                sError = Ex.Message;
                return false;
            }
        }
        public static bool SendMail(MailMessage Message, string smtpUserName, string smtpPassword, string smtpserver, int smtpPort, bool blnEnableSSL, ref string sError)
        {
            try
            {
                NetworkCredential network_cdr = new NetworkCredential();
                network_cdr.UserName = smtpUserName;
                network_cdr.Password = smtpPassword;

                SmtpClient mail_client = new SmtpClient();
                mail_client.UseDefaultCredentials = true;
                mail_client.Credentials = network_cdr;                  //Network Credential
                mail_client.Port = smtpPort;                            //SMTP Port
                mail_client.Host = smtpserver;                          //SMTP Server
                mail_client.EnableSsl = blnEnableSSL;                   //Secure Sockets Layer(SSL)                
                //mail_client.Timeout = 0;
                mail_client.Send(Message);                              //Send the message

                sError = string.Empty;
                return true;
            }
            catch (Exception Ex)
            {
                sError = Ex.Message;
                return false;
            }
        }

        public static void Export(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a form to contain the grid
                    Table table = new Table();
                    table.GridLines = GridLines.Both;
                    //  add the header row to the table
                    if (gv.HeaderRow != null)
                    {
                        GlobalFunctions.PrepareControlForExport(gv.HeaderRow);
                        table.Rows.Add(gv.HeaderRow);
                    }

                    //  add each of the data rows to the table
                    foreach (GridViewRow row in gv.Rows)
                    {
                        GlobalFunctions.PrepareControlForExport(row);
                        table.Rows.Add(row);
                    }

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);
                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }
        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                else if (current is TextBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as TextBox).Text));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HiddenField).Value));
                }

                if (current.HasControls())
                {
                    GlobalFunctions.PrepareControlForExport(current);
                }
            }
        }
        public static void SetPaging(ref GridView grd)
        {
            bool blnPaging = true;
            int iPageSize = 10;

            //BeanHelper.GeneralSettingsBean.PageSettings(ref blnPaging, ref iPageSize);

            grd.AllowPaging = blnPaging;
            if (blnPaging)
                grd.PageSize = iPageSize;
        }
    }
}