﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace webPrecision.Interfaces
{
    public interface IUserMaster
    {
        DataTable SearchRecord(string searchstr);
        DataRow GetRecord(Int64 UserId);

        DataTable CheckNameForInsert(string Name);
        DataTable CheckNameForUpdate(string Name, string ID);
        int IsAdminUserExist(string UserName, string UserPassword);
        int IsUserExist(string UserName, string UserPassword);
    }
}