﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace webPrecision.Interfaces
{
    public interface IGeneralSettings
    {
        DataTable GetRecord();
        void PageSettings(ref bool Paging, ref int PageSize);
    }
}
