﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace webPrecision.Interfaces
{
    public interface ICommonOperations
    {
        int InsertRecord();
        int UpdateRecord();
        int DeleteRecord(Int64 ID);
    }
}
