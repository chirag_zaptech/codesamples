﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace webPrecision.Interfaces
{
    public interface IOpportunity
    {
        DataTable SearchRecord(string searchstr);
        DataRow GetRecord(Int64 QuestionBankId);
    }
}