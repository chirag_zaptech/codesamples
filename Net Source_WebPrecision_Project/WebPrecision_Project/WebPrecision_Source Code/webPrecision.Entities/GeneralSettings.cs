﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webPrecision.Entities
{
    public class GeneralSettings
    {
        public GeneralSettings() { }
        public Int64 ID { get; set; }
        public bool Paging { get; set; }
        public int PageSize { get; set; }
    }
}
