﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webPrecision.Entities
{
    public class Opportunity
    {
        public Opportunity() { }

        public Int64 OpportunityId { get; set; }
        public string ONAICS { get; set; }
        public string Priority { get; set; }
        public string OSOL { get; set; }
        public string ORType { get; set; }
        public string ODate { get; set; }
        public string OSB { get; set; }
        public string OType { get; set; }                
        public string OTitle { get; set; }
        public int OIsHide { get; set; }
        public string OCategory { get; set; }
        public string ODepartment { get; set; }
        public string OAddress { get; set; }
        public string OContact { get; set; }
        public string OLink { get; set; }
        public string OPrime { get; set; }
        public string ODescription { get; set; }
        public string ODueDate { get; set; }
        public string OAssignedto { get; set; }
        public string ONotes { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
    }
}