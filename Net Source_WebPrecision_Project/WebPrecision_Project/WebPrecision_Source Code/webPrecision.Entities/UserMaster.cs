﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webPrecision.Entities
{
    public class UserMaster
    {
        public UserMaster() { }

        public Int64 UserId { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public string EmailId { get; set; }
        public int UserType { get; set; }
        public int IsActive { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
    }
}