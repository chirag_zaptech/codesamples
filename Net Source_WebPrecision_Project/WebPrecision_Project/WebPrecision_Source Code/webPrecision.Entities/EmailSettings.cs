﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webPrecision.Entities
{
    public class EmailSettings
    {
        public EmailSettings() { }
        public Int64 ID { get; set; }
        public string SMTPUserName { get; set; }
        public string SMTPPassword { get; set; }
        public string SMTPServer { get; set; }
        public int SMTPPort { get; set; }
        public bool EnableSSL { get; set; }
    }
}
