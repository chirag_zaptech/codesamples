package com.pegalinas.pdf;

import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.pegalinas.pdf.multi.templetes.TempletePegalinasMulti;
import com.pegalinas.pdf.templetes.TempletePegalinas;

/*
 * It is special class for multiple template genetation
 */
public class TemplateGeneratorMulti extends BaseGenerator {

	public TemplateGeneratorMulti(Float width, Float height, Float leftMargin,
			Float rightMargin, Float bottomMargin, Float topMargin) {
		super(width, height, leftMargin, rightMargin, bottomMargin, topMargin);
		// TODO Auto-generated constructor stub
	}

	public TemplateGeneratorMulti(Rectangle pageSize, Float leftMargin,
			Float rightMargin, Float bottomMargin, Float topMargin) {
		super(pageSize, leftMargin, rightMargin, bottomMargin, topMargin);
		// TODO Auto-generated constructor stub
	}

	public void generatePdfMulti(TempletePegalinasMulti templete)
			throws IOException {
		currentHeight = templete.renderPrintBlock(writer.getDirectContent(),
				currentHeight);

	}

	public void generatePdf(TempletePegalinas templete) throws IOException {
		currentHeight = templete.renderPrintBlock(writer.getDirectContent(),
				currentHeight);

	}

	public void generateCut(TempletePegalinas templete) throws IOException {
		currentHeight = templete.renderCutBlock(writer.getDirectContent(),
				currentHeight);
	}

	public void addProductHeader(String orderData, String barcodeData)
			throws DocumentException {
		PdfContentByte cb = writer.getDirectContent();
		cb.beginText();
		cb.setFontAndSize(baseFont, 8);
		cb.showTextAligned(PdfContentByte.ALIGN_CENTER, orderData, 45,
				currentHeight, 0);
		cb.endText();
	}
}
