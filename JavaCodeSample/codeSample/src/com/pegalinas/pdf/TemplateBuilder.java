package com.pegalinas.pdf;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;
import com.pegalinas.pdf.multi.templetes.BasicoMulti;
import com.pegalinas.pdf.multi.templetes.TempletePegalinasMulti;
import com.pegalinas.pdf.multi.templetes.XpressMulti;
import com.pegalinas.pdf.templetes.Basico;
import com.pegalinas.pdf.templetes.Calzado;
import com.pegalinas.pdf.templetes.Cuaderno;
import com.pegalinas.pdf.templetes.Header;
import com.pegalinas.pdf.templetes.Jumbo;
import com.pegalinas.pdf.templetes.Mini;
import com.pegalinas.pdf.templetes.TempletePegalinas;
import com.pegalinas.pdf.templetes.Xpress;
import com.tamono.bean.MultiParameterDetail;
import com.tamono.function.CommonFuctions;
import com.tamono.jdbc.JDBCUtil;

public class TemplateBuilder {

	public static final int TEMPLETE_HEADER = 0;
	private static Logger logger = Logger.getLogger(TemplateBuilder.class);
	float width = 1000f;
	float height = 2000f;
	String fontPath = null;
	Map<String, BaseFont> bs = new TreeMap<String, BaseFont>();
	String iconPath = null;
	/**
	 * Setting initializing font for the templates
	 * @throws DocumentException
	 * @throws IOException
	 */
	public void init() throws DocumentException, IOException {
		bs.put("77", BaseFont.createFont(fontPath + "/77.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("78", BaseFont.createFont(fontPath + "/78.TTF",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("79", BaseFont.createFont(fontPath + "/79.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("96", BaseFont.createFont(fontPath + "/96.TTF",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("97", BaseFont.createFont(fontPath + "/97.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("98", BaseFont.createFont(fontPath + "/98.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("101", BaseFont.createFont(fontPath + "/101.TTF",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("102", BaseFont.createFont(fontPath + "/102.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("104", BaseFont.createFont(fontPath + "/104.TTF",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("105", BaseFont.createFont(fontPath + "/105.TTF",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("106", BaseFont.createFont(fontPath + "/106.TTF",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("107", BaseFont.createFont(fontPath + "/107.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("108", BaseFont.createFont(fontPath + "/108.TTF",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("109", BaseFont.createFont(fontPath + "/109.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		bs.put("112", BaseFont.createFont(fontPath + "/112.ttf",
				BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
	}
/**
 * Header builder for template properties:- return header type template
 * @param parameters
 * @return
 */
	private TempletePegalinas buildHeader(TemplateParameters parameters) {
		TempletePegalinas templete = null;
		logger.debug("Init Header template");
		List<SimpleTextInput> texts = new ArrayList<SimpleTextInput>();
		// Version Static value as 1.17l
		SimpleTextInput text1 = new SimpleTextInput("1.17l" + " Pedido:"
				+ parameters.getOrderId().toString(), false);
		texts.add(text1);
		SimpleTextInput text7 = new SimpleTextInput(
				parameters.getTemplateName(), false);
		texts.add(text7);
		SimpleTextInput text2 = new SimpleTextInput(parameters.getFontName(),
				false);
		texts.add(text2);
		SimpleTextInput text3 = new SimpleTextInput(parameters.getIconName(),
				false);
		texts.add(text3);
		SimpleTextInput text4 = new SimpleTextInput("cantidad "
				+ parameters.getQuantity().toString(), false);
		texts.add(text4);
		SimpleTextInput text5 = new SimpleTextInput(
				parameters.getForegroundColor(), false);
		texts.add(text5);
		SimpleTextInput text6 = new SimpleTextInput(
				parameters.getBackgroundColor(), false);
		texts.add(text6);
		Header header = new Header(bs.get("78"), 6f, texts, iconPath, null);
		header.setColumns(7);
		header.setRows(1);
		header.setDebugOn(true);
		logger.debug("header build success");
		templete = header;
		return templete;
	}
	/**
	 * Icom builder for template icon information
	 * @param iconName
	 * @return
	 */
	private Icono buildIcon(Integer iconName) {
		if (iconName == null) {
			return null;
		} else {
			Icono icon = new Icono(iconName + ".pdf");
			return icon;
		}
	}

	/**
	 * Icon builder for multiTemplate type :- returns list of multiple icons
	 * @param iconid
	 * @return
	 */
	private List<Icono> buildMultiIcon(List<Integer> iconid) {
		Iterator<Integer> iterator = iconid.iterator();
		ArrayList<Icono> iconNameList = new ArrayList<Icono>();
		while (iterator.hasNext()) {
			String iconName = "" + iterator.next() + ".pdf";
			if (iconName == "0.pdf" || iconName == "255.pdf") {
				iconNameList.add(null);
			} else {
				Icono icon = new Icono(iconName);
				iconNameList.add(icon);
			}
		}
		return iconNameList;
	}

	@SuppressWarnings("null")
	private TempletePegalinas buildTemplate(TemplateParameters parameters)
			throws SQLException {
		TempletePegalinas templete = null;
		Connection conn = null;
		String templetType = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conn = JDBCUtil.getConnection();
			st = (Statement) conn.createStatement();

			/*
			 * query to find template type.
			 */
			String query = "SELECT pfp.*,  pfvl.id_lang, pfvl.value FROM  `pr_feature_product` pfp LEFT JOIN `pr_feature_value_lang` pfvl ON pfp.id_feature_value = pfvl.id_feature_value WHERE pfp.id_product = "
					+ parameters.getTemplateId()
					+ " AND pfp.id_feature = 21 AND pfvl.id_lang = 7";
			rs = (ResultSet) st.executeQuery(query);
			while (rs.next()) {
				templetType = rs.getString("value");
			}
		} catch (Exception e) {
			logger.error("unable to build template");
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			if (st == null) {
				st.close();
			}
			if (rs != null) {
				rs.close();
			}
			conn.close();
		}
		
		/*
		 *Checking type of template 
		 */
		if (templetType != null && templetType.equals("1-25")) {
			try {
				Basico pegalinas_25 = new Basico(bs.get(parameters
						.getFontNameId().toString()), 12f * 2.8f,
						parameters.getTexts(), iconPath,
						buildIcon(parameters.getIconNameId()));
				pegalinas_25.setColumns(5);
				pegalinas_25.setRows(5);
				pegalinas_25.setDebugOn(true);
				Basico.fontFlag = parameters.getFontNameId().toString();
				templete = pegalinas_25;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (templetType != null && templetType.equals("1-50")) {
			try {
				Basico pegalinas_50 = new Basico(bs.get(parameters
						.getFontNameId().toString()), 12f * 2.8f,
						parameters.getTexts(), iconPath,
						buildIcon(parameters.getIconNameId()));

				pegalinas_50.setColumns(5);
				pegalinas_50.setRows(10);
				pegalinas_50.setDebugOn(true);
				templete = pegalinas_50;
				Basico.fontFlag = parameters.getFontNameId().toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (templetType != null && templetType.equals("1-100")) {
			Basico pegalinas_100 = new Basico(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			pegalinas_100.setColumns(5);
			pegalinas_100.setRows(20);
			pegalinas_100.setDebugOn(true);
			Basico.fontFlag = parameters.getFontNameId().toString();
			templete = pegalinas_100;
		} else if (templetType != null && templetType.equals("2-28")) {
			Mini mini_28 = new Mini(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			mini_28.setColumns(7);
			mini_28.setRows(4);
			mini_28.setDebugOn(true);
			templete = mini_28;
		} else if (templetType != null && templetType.equals("2-56")) {
			Mini mini_56 = new Mini(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			mini_56.setColumns(7);
			mini_56.setRows(8);
			mini_56.setDebugOn(true);
			templete = mini_56;
		} else if (templetType != null && templetType.equals("2-105")) {
			Mini mini_105 = new Mini(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			mini_105.setColumns(7);
			mini_105.setRows(15);
			mini_105.setDebugOn(true);
			templete = mini_105;
		} else if (templetType != null && templetType.equals("3-25")) {
			Cuaderno cuaderno_25 = new Cuaderno(bs.get(parameters
					.getFontNameId().toString()), 12f * 2.8f,
					parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			cuaderno_25.setColumns(5);
			cuaderno_25.setRows(5);
			cuaderno_25.setDebugOn(true);
			templete = cuaderno_25;
		} else if (templetType != null && templetType.equals("3-50")) {
			Cuaderno cuaderno_50 = new Cuaderno(bs.get(parameters
					.getFontNameId().toString()), 12f * 2.8f,
					parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			cuaderno_50.setColumns(5);
			cuaderno_50.setRows(10);
			cuaderno_50.setDebugOn(true);
			templete = cuaderno_50;
		} else if (templetType != null && templetType.equals("4-15")) {
			logger.debug("Init Jumbo template");
			Jumbo jumbo_15 = new Jumbo(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			jumbo_15.setColumns(3);
			jumbo_15.setRows(5);
			jumbo_15.setDebugOn(true);
			templete = jumbo_15;
		} else if (templetType != null && templetType.equals("4-30")) {
			logger.debug("Init Jumbo template");
			Jumbo jumbo_30 = new Jumbo(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			jumbo_30.setColumns(3);
			jumbo_30.setRows(10);
			jumbo_30.setDebugOn(true);
			templete = jumbo_30;
		} else if (templetType != null && templetType.equals("5-14")) {
			logger.debug("Init Calzado template");
			Calzado calzado = new Calzado(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			calzado.setColumns(7);
			calzado.setRows(2);
			calzado.setDebugOn(true);
			Calzado.fontFlag = parameters.getFontNameId().toString();
			templete = calzado;
		} else if (templetType != null && templetType.equals("6-24")) {
			logger.debug("Init Xpress 24");
			Xpress xpress24 = new Xpress(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			Xpress.fontFlag = parameters.getFontNameId().toString();
			xpress24.setColumns(12);
			xpress24.setRows(2);
			templete = xpress24;
		} else if (templetType != null && templetType.equals("6-48")) {
			logger.debug("Init Xpress 48");
			Xpress xpress48 = new Xpress(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			Xpress.fontFlag = parameters.getFontNameId().toString();
			xpress48.setColumns(12);
			xpress48.setRows(4);
			templete = xpress48;
		} else if (templetType != null
				&& (templetType.equals("6-96") || templetType.equals("6-98"))) {
			logger.debug("Init Xpress 96");
			Xpress xpress96 = new Xpress(bs.get(parameters.getFontNameId()
					.toString()), 12f * 2.8f, parameters.getTexts(), iconPath,
					buildIcon(parameters.getIconNameId()));
			Xpress.fontFlag = parameters.getFontNameId().toString();
			xpress96.setColumns(12);
			xpress96.setRows(8);
			templete = xpress96;
		} else {
			logger.debug("no product found in DB.....for templete code");
		}
		return templete;
	}

	@SuppressWarnings("null")
	private TempletePegalinasMulti buildMultiTemplate(
			MultiParameterDetail multiParameters) throws SQLException {
		TempletePegalinasMulti templete = null;
		String templetType = null;
		/**
		 * multitemplet
		 */

		templetType = "" + multiParameters.getIdProduct();
		if (templetType != null && templetType.equals("22")) {
			List<BaseFont> basefontList = new ArrayList<BaseFont>();
			for (Integer fontList : multiParameters.getFontId()) {
				BaseFont bontobj = bs.get(fontList.toString());
				basefontList.add(bontobj);
			}
			BasicoMulti pegalinas_25 = new BasicoMulti(basefontList,
					12f * 2.8f, multiParameters.getTextList(), iconPath,
					buildMultiIcon(multiParameters.getIconid()));
			pegalinas_25.setColumns(1);
			pegalinas_25.setRows(1);
			pegalinas_25.setDebugOn(true);
			templete = pegalinas_25;
		} else if (templetType != null && templetType.equals("29")) {
			List<BaseFont> basefontList = new ArrayList<BaseFont>();
			for (Integer fontList : multiParameters.getFontId()) {
				BaseFont bontobj = bs.get(fontList.toString());
				basefontList.add(bontobj);
		}
			XpressMulti xpressMulti = new XpressMulti(basefontList, 12f * 2.8f,
					multiParameters.getTextList(), iconPath,
					buildMultiIcon(multiParameters.getIconid()));
			xpressMulti.setColumns(12);
			xpressMulti.setRows(2);
			xpressMulti.setDebugOn(true);
			templete = xpressMulti;

		} else {
			System.out.println("No templete found");
		}

		return templete;
	}
	
	/*
	 * Creating pdf file for template
	 */
	public void printPdfToFile(List<TemplateParameters> parameters,
			String outputFile) throws DocumentException, IOException,
			SQLException {
		logger.info("Printing to PDF File");
		TemplateGenerator tGenerator = new TemplateGenerator(100 + width,
				2000f, 0f, 0f, 0f, 0f);
		tGenerator.open(outputFile);
		Float currentHeight = 1950f;
		tGenerator.setCurrentHeight(currentHeight);
		for (TemplateParameters templateParameters : parameters) {
			logger.info("Printing template :" + templateParameters);
			TempletePegalinas header = buildHeader(templateParameters);
			float headerHeight = header.getExpectedBlockHeight();
			TempletePegalinas templete = buildTemplate(templateParameters);
			if (templete == null) {
				logger.error("TEMPLATE IS NULL");

			} else {
				logger.info("Printing "
						+ templete.getClass().getCanonicalName());
				logger.info("Position " + tGenerator.getCurrentHeight() + " "
						+ templete.getExpectedBlockHeight());
				float templateHeight = templete.getExpectedBlockHeight();
				if ((tGenerator.getCurrentHeight() - (templateHeight + headerHeight)) < 0) {
					tGenerator.newPage();
					tGenerator
							.setCurrentHeight(tGenerator.getCurrentHeight() - 50);

				}
				for (int i = 0; i < templateParameters.getQuantity(); i++) {
					logger.info("Generating count " + (i + 1) + " of "
							+ templateParameters.getQuantity() + " total items");
					tGenerator.generatePdf(header);
					tGenerator.generatePdf(templete);
					currentHeight = tGenerator.getCurrentHeight();
					tGenerator.setCurrentHeight(currentHeight - 50);
				}
			}
		}
		tGenerator.flush();
		logger.info("Flush");
		tGenerator.close();
		logger.info("Close");
	}

	/**
	 * Print to multi product method new implemetation
	 * 
	 * @throws ClassNotFoundException
	 * 
	 */
	public void printMultiPdfToFile(List<MultiParameterDetail> parameters,
			String outputFile) throws DocumentException, IOException,
			SQLException, ClassNotFoundException {
		logger.info("Printing to PDF File");
		TemplateGeneratorMulti tGenerator = new TemplateGeneratorMulti(
				100 + width, 2000f, 0f, 0f, 0f, 0f);
		tGenerator.open(outputFile);
		Float currentHeight = 1950f;
		tGenerator.setCurrentHeight(currentHeight);
		for (MultiParameterDetail multiTemplateParameters : parameters) {

			logger.info("Printing template :" + multiTemplateParameters);
			TemplateParameters tmpletheader = CommonFuctions
					.getHeader(multiTemplateParameters.getIdCart());
			TempletePegalinas header = buildHeader(tmpletheader);
			float headerHeight = header.getExpectedBlockHeight();
			
			TempletePegalinasMulti templete = buildMultiTemplate(multiTemplateParameters);
			if (templete == null) {
				logger.error("TEMPLATE IS NULL");
			} else {
				logger.info("Printing "
						+ templete.getClass().getCanonicalName());
				logger.info("Position " + tGenerator.getCurrentHeight() + " "
						+ templete.getExpectedBlockHeight());
				float templateHeight = templete.getExpectedBlockHeight();
				if ((tGenerator.getCurrentHeight() - (templateHeight + headerHeight)) < 0) {
					tGenerator.newPage();
					tGenerator
							.setCurrentHeight(tGenerator.getCurrentHeight() - 50);
				}
				for (int i = 0; i < multiTemplateParameters.getQuantity(); i++) {
					logger.info("Generating count " + (i + 1) + " of "
							+ multiTemplateParameters.getQuantity()
							+ " total items");
					tGenerator.generatePdf(header);
					tGenerator.generatePdfMulti(templete);
					currentHeight = tGenerator.getCurrentHeight();
					tGenerator.setCurrentHeight(currentHeight - 50);
				}
			}
		}
		tGenerator.flush();
		logger.info("Flush");
		tGenerator.close();
		logger.info("Close");
	}

	// multi product method end here.

	public Map<String, BaseFont> getBs() {
		return bs;
	}

	/**
	 * @return the fontPath
	 */
	public String getFontPath() {
		return fontPath;
	}

	/**
	 * @param fontPath
	 *            the fontPath to set
	 */
	public void setFontPath(String fontPath) {
		this.fontPath = fontPath;
	}

	/**
	 * @return the iconPath
	 */
	public String getIconPath() {
		return iconPath;
	}

	/**
	 * @param iconPath
	 *            the iconPath to set
	 */
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
}
