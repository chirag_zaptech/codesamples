package com.pegalinas.pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;

public class BaseGenerator {

	private static final Logger logger = Logger.getLogger(BaseGenerator.class);

	protected Rectangle pageSize = null;
	protected Float width = 0f;
	protected Float height = 0f;
	protected Float leftMargin = 0f;
	protected Float rightMargin = 0f;
	protected Float bottomMargin = 0f;
	protected Float topMargin = 0f;
	protected Float currentHeight = 0f;
	protected Document document = null;
	protected PdfWriter writer;
	protected BaseFont baseFont = null;

	public BaseFont getBaseFont() {
		return baseFont;
	}

	public void setBaseFont(BaseFont baseFont) {
		this.baseFont = baseFont;
	}

	public void open(String outputName) throws FileNotFoundException,
			DocumentException {
		writer = PdfWriter.getInstance(document, new FileOutputStream(
				outputName));
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setAtLeastPdfVersion(PdfWriter.VERSION_1_7);
		writer.setPDFXConformance(PdfWriter.PDFX32002);
		writer.setUserunit(1.0f);
		document.open();
	}

	public void open(OutputStream outputStream) throws DocumentException {
		writer = PdfWriter.getInstance(document, outputStream);
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setAtLeastPdfVersion(PdfWriter.VERSION_1_7);
		writer.setPDFXConformance(PdfWriter.PDFX32002);
		writer.setUserunit(144.0f);
		document.open();
	}

	public boolean newPage() {
		logger.info("*** NEW PAGE ***");
		currentHeight = height;
		return writer.getDirectContent().getPdfDocument().newPage();
	}

	public void setCurrentHeight(Float currentHeight) {
		this.currentHeight = currentHeight;
	}

	public Float getCurrentHeight() {
		return currentHeight;
	}

	public void flush() {
		writer.flush();
	}

	public void close() {
		document.close();
	}

	public BaseGenerator(Float width, Float height, Float leftMargin,
			Float rightMargin, Float bottomMargin, Float topMargin) {
		super();
		this.width = width;
		this.height = height;
		this.leftMargin = leftMargin;
		this.rightMargin = rightMargin;
		this.bottomMargin = bottomMargin;
		this.topMargin = topMargin;
		document = new Document(new Rectangle(width, height), leftMargin,
				rightMargin, topMargin, bottomMargin);
	}

	public BaseGenerator(Rectangle pageSize, Float leftMargin,
			Float rightMargin, Float bottomMargin, Float topMargin) {
		super();
		this.pageSize = pageSize;
		this.leftMargin = leftMargin;
		this.rightMargin = rightMargin;
		this.bottomMargin = bottomMargin;
		this.topMargin = topMargin;
		document = new Document(pageSize, leftMargin, rightMargin, topMargin,
				bottomMargin);
	}

}
