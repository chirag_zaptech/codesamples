package com.pegalinas.pdf;

import java.util.List;

import com.pegalinas.model.SimpleTextInput;

/**
 * 
 * Class that holds information for template parameter.
 * 
 */
public class TemplateParameters {
	private String fontName;
	private Integer fontNameId;
	private List<SimpleTextInput> texts;
	private String iconName;
	private Integer iconNameId;
	private Integer quantity;
	private Integer templateId;
	private String templateName;
	private Integer orderId;
	private String backgroundColor;
	private String foregroundColor;

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public List<SimpleTextInput> getTexts() {
		return texts;
	}

	public void setTexts(List<SimpleTextInput> texts) {
		this.texts = texts;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public String getForegroundColor() {
		return foregroundColor;
	}

	public void setForegroundColor(String foregroundColor) {
		this.foregroundColor = foregroundColor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TemplateParameters [fontName=" + fontName + ", texts=" + texts
				+ ", iconName=" + iconName + ", quantity=" + quantity
				+ ", templateId=" + templateId + ", orderId=" + orderId
				+ ", backgroundColor=" + backgroundColor + ", foregroundColor="
				+ foregroundColor + "]";
	}

	/**
	 * @return the fontNameId
	 */
	public Integer getFontNameId() {
		return fontNameId;
	}

	/**
	 * @param fontNameId
	 *            the fontNameId to set
	 */
	public void setFontNameId(Integer fontNameId) {
		this.fontNameId = fontNameId;
	}

	/**
	 * @return the iconNameId
	 */
	public Integer getIconNameId() {
		return iconNameId;
	}

	/**
	 * @param iconNameId
	 *            the iconNameId to set
	 */
	public void setIconNameId(Integer iconNameId) {
		this.iconNameId = iconNameId;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

}
