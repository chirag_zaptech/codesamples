package com.pegalinas.pdf.templetes;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;
import com.pegalinas.pdf.PdfUtils;

/**
 * 
 *
 */
public class Mini extends BasePegalinas implements TempletePegalinas {

	public Mini(BaseFont baseFont, Float fontSize, List<SimpleTextInput> texts,
			String iconPath, Icono icon) {

		super(baseFont, fontSize, texts, iconPath, icon);
		System.out.println("mini constctr..calling");
		width = 35f * 2.84f;
		height = 5f * 2.84f;
		logger.info("Setting width and height w:" + width + " h:" + height);

		widthMargin = 6;
		heightMargin = 2;

		if (fontSize == null) {
			fontSize = 4f * 2.8f;
		}
	}

	private float cornerWidth = 2.5f * 2.84f;
	private float columnOffset = 8f * 2.84f;
	private float leftMargin = 45f * 2.84f;
	private String idPedido = "";
	private static final Logger logger = Logger.getLogger(Mini.class);

	
	@Override
	public Float renderCutOutline(PdfContentByte cb) throws IOException {
		// cb.setRGBColorFill(0, 0, 0);
		PdfUtils.renderRoundedBox(cb, xOrg, yOrg, width, height, cornerWidth);
		cb.closePathStroke();
		return (height);
	}

	@Override
	public Float renderPrintBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		yOrg = startHeight;
		System.out.println(texts);
		calculateOffsetsAndFontSize(texts.subList(0, 1));
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			yOrg = yOrg - height;
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				xOrg = leftMargin + colCounter * (width + columnOffset);
				renderItem(cb);
			}
		}
		return yOrg;
	}

	@Override
	public Float renderCutBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		logger.debug("Render Cut Block");
		yOrg = startHeight;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			yOrg = yOrg - height;
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				xOrg = leftMargin + colCounter * (width + columnOffset);
				renderCutOutline(cb);
			}
		}
		return yOrg;
	}
	/**
	 * Templete text on pdf
	 */
	@Override
	public Float renderItem(PdfContentByte cb) throws IOException {
		if (debugOn) {
			renderCutOutline(cb);
		}
		logger.debug("Render Item");
		// float adjFontSize=0;
		if (texts.size() > 0) {
			cb.beginText();
			logger.debug("Show text @ " + xOrg + ", " + yOrg
					+ " with fontsize " + fontSize);
			cb.setFontAndSize(baseFont, fontSize - 1.5f);
			float ascent = baseFont
					.getFontDescriptor(BaseFont.ASCENT, fontSize);
			float descent = baseFont.getFontDescriptor(BaseFont.DESCENT,
					fontSize);
			float offset = ((height - ascent + descent) / 2) - descent
					- heightMargin / 2;
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, texts.get(0)
					.getText(), xOrg + widthOffset + 3, yOrg + offset, 0);
			cb.endText();
		}
		return yOrg;
	}

	@Override
	public Float getExpectedBlockHeight() {
		return height * rows;
	}

	@Override
	public String getIdPedido() {
		return idPedido;
	}

	@Override
	public void init(BaseFont baseFont, Float fontSize,
			List<SimpleTextInput> texts, Icono icon) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setIdPedido(String idLabel) {
		idPedido = idLabel;

	}

	public Mini() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BasePegalinas create(BaseFont baseFont, Float fontSize,
			List<SimpleTextInput> texts, String iconPath, Icono icon) {

		return new Mini(baseFont, fontSize, texts, iconPath, icon);
	}

	/*
	 * @Override public BasePegalinasMulti create(List<BaseFont> baseFont, Float
	 * fontSize, List<List<SimpleTextInput>> texts, String iconPath, List<Icono>
	 * iconlist) { // TODO Auto-generated method stub return null; }
	 */

}
