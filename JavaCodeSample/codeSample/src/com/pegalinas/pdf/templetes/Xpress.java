package com.pegalinas.pdf.templetes;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;

/**
 *
 */
public class Xpress extends BasePegalinas implements TempletePegalinas {

	private static final Logger logger = Logger.getLogger(Xpress.class);

	private float xOrg = 0;
	private float yOrg = 0;

	private float leftMargin = 5f;
	private float width = 2.3f;
	private float columnOffset = 0.2f;
	private float height = 2.3f;
	private float radius = 0.12f;

	public static final String DEFAULT_CHARSET = "UTF-8";
	private String svgPath = "";
	private String svgFile = "";
	protected String charSet = DEFAULT_CHARSET;
	public static String fontFlag = null;

	private BaseFont bf;

	public Xpress() {
		super();
		fontSize = 8f;
	}

	public Xpress(BaseFont baseFont, Float fontSize,
			List<SimpleTextInput> texts, String iconPath, Icono icon) {
		super(baseFont, fontSize, texts, iconPath, icon);

		bf = baseFont;
		String[][] fontName = bf.getFamilyFontName();
		for (int i = 0; i < fontName.length; i++) {

			String[] fontDetail = fontName[i];
			for (int j = 0; j < fontDetail.length; j++) {
				logger.debug("Font data - " + fontDetail[j]);
			}
		}
		System.out.println(bf.toString());
		if (fontSize == null) {
			fontSize = 8f;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#renderPrintBlock(com.lowagie
	 * .text.pdf.PdfContentByte, java.lang.Float)
	 */
	@Override
	public Float renderPrintBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		if (logger.isDebugEnabled()) {
			logger.debug("Render print block (" + xOrg + "," + yOrg + ")");
		}
		yOrg = startHeight;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {

			if (rowCounter == 4) {
				yOrg = yOrg - cmToPoints(height) / 2;
			}
			yOrg = yOrg - cmToPoints(height);
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				// System.out.println("row,cloumn==>"+rowCounter+" , "
				// +colCounter);
				if (colCounter % 2 == 0) {
					xOrg = cmToPoints(leftMargin) + colCounter
							* cmToPoints(width + columnOffset);
				} else {
					xOrg = cmToPoints(leftMargin) + colCounter
							* cmToPoints(width + columnOffset)
							- cmToPoints(columnOffset);
				}
				renderItem(cb);
			}
		}
		System.out.println("row printed");
		return yOrg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#renderCutBlock(com.lowagie
	 * .text.pdf.PdfContentByte, java.lang.Float)
	 */
	@Override
	public Float renderCutBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		System.out.println("render cutblock... in xpress...<=====");
		if (logger.isDebugEnabled()) {
			logger.debug("Render cut block (" + xOrg + "," + yOrg + ")");
		}
		yOrg = startHeight - cmToPoints(radius);
		xOrg = cmToPoints(leftMargin) + cmToPoints(radius);
		Float rowIncrease = 0f;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				rowIncrease = renderCutOutline(cb);
				xOrg += rowIncrease;
				if ((colCounter + 1) % 2 == 0) {
					xOrg += 5;
				}
			}
			xOrg = cmToPoints(leftMargin + radius);
			yOrg = yOrg - rowIncrease;
			if ((rowCounter + 1) % 4 == 0) {
				// yOrg-=10;
			}
		}
		// return yOrg;
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#getExpectedBlockHeight()
	 */
	@Override
	public Float getExpectedBlockHeight() {
		// TODO Auto-generated method stub
		return height * rows;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pegalinas.pdf.templetes.TempletePegalinas#getIdPedido()
	 */
	@Override
	public String getIdPedido() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#setIdPedido(java.lang.String
	 * )
	 */
	@Override
	public void setIdPedido(String idLabel) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.BasePegalinas#renderCutOutline(com.lowagie
	 * .text.pdf.PdfContentByte)
	 */
	@Override
	public Float renderCutOutline(PdfContentByte cb) throws IOException {
		if (logger.isDebugEnabled()) {
			logger.debug("Render cut outline (" + xOrg + "," + yOrg + ")");
		}
		cb.circle(xOrg, yOrg, cmToPoints(radius));
		cb.stroke();
		cb.closePath();
		cb.circle(xOrg, yOrg, cmToPoints(radius));
		cb.fillStroke();
		cb.closePath();
		return cmToPoints(radius * 2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.BasePegalinas#renderItem(com.lowagie.text
	 * .pdf.PdfContentByte)
	 */
	@Override
	public Float renderItem(PdfContentByte cb) throws IOException {
		if (logger.isDebugEnabled()) {
			logger.debug("Render item (" + xOrg + "," + yOrg + ")");
		}
		cb.setColorFill(BaseColor.BLACK);
		System.out.println(xOrg + "xorg <>yorg" + yOrg);
		cb.circle(xOrg + cmToPoints(.85f), yOrg + cmToPoints(.65f),
				cmToPoints(1.15f));// circle
		try {
			createPdf(cb);
		} catch (DocumentException e) {
			System.out.println("error in printing text");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		cb.stroke();
		if (debugOn) {
			System.out.println("Debug on");
		}
		if (iconFile != null) {
			PdfReader reader = new PdfReader(iconFile);
			PdfImportedPage iconoPdf = cb.getPdfWriter().getImportedPage(
					reader, 1);
			iconoPdf.setHorizontalScaling(10);
			float scaleFactor = 0.40f;
			cb.addTemplate(iconoPdf, scaleFactor, 0, 0, scaleFactor, xOrg
					+ cmToPoints(1.4f) - iconoPdf.getWidth() * scaleFactor
					+ icon.getOffsetX(),
					yOrg + cmToPoints(.70f) - iconoPdf.getHeight()
							* scaleFactor / 2);
		}
		return cmToPoints(radius * 2);
	}

	// implemeted method
	public void createPdf(PdfContentByte cb) throws IOException,
			DocumentException {
		fontSize = 10f;
		cb.setFontAndSize(baseFont, fontSize);

		float origin[] = { xOrg + cmToPoints(.85f), yOrg + cmToPoints(.65f) };
		float ascent = baseFont.getFontDescriptor(BaseFont.ASCENT, fontSize);
		// float descent = baseFont.getFontDescriptor(BaseFont.DESCENT,
		// fontSize);
		float radius = cmToPoints(1.05f) - (ascent + heightOffset);
		if (fontFlag.equals("112")) {
			cb.setFontAndSize(baseFont, fontSize - 2);
			radius = cmToPoints(1.10f) - (ascent + heightOffset + 1);
		} else if (fontFlag.equals("107")) {
			cb.setFontAndSize(baseFont, fontSize - 4);
			radius = cmToPoints(1.10f) - (ascent / 2);
		}
		String text = texts.get(0).getText();

		if (text.length() % 2 == 0) {
			text = text + " ";
		}
		char[] arr = text.toCharArray();
		float angle = 0f;
		for (int i = 0; i < arr.length / 2 + 1; i++) {
			cb.beginText();
			String strl = "" + arr[(arr.length / 2) - i];
			String strr = "" + arr[(arr.length / 2) + i];
			float[] pointL = pointOnCircle(radius, 85 + angle, origin);
			float[] pointR = pointOnCircle(radius, 85 - angle, origin);
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, strl, pointL[0],
					pointL[1], angle);
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, strr, pointR[0],
					pointR[1], -angle);
			angle = angle + 360 / 21;
			cb.endText();
		}
	}

	public static float[] pointOnCircle(float radius, float angleInDegrees,
			float[] origin) {
		/**
		 * Convert from degrees to radians via multiplication by PI/180
		 */
		float x = (float) (radius * Math.cos(angleInDegrees * Math.PI / 180F))
				+ origin[0];
		float y = (float) (radius * Math.sin(angleInDegrees * Math.PI / 180F))
				+ origin[1];
		float[] parimeterPoint = { x, y };

		return parimeterPoint;
	}

	/**
	 * @return the svgPath
	 */
	public String getSvgPath() {
		return svgPath;
	}

	/**
	 * @param svgPath
	 *            the svgPath to set
	 */
	public void setSvgPath(String svgPath) {
		this.svgPath = svgPath;
	}

	/**
	 * @return the svgFile
	 */
	public String getSvgFile() {
		return svgFile;
	}

	/**
	 * @param svgFile
	 *            the svgFile to set
	 */
	public void setSvgFile(String svgFile) {
		this.svgFile = svgFile;
	}

	public boolean fitsItem(float height) {
		return (height <= cmToPoints(this.height));
	}

	public void setPrintHeight(Float y) {
		yOrg = y;
	}

	public Float getPrintHeight() {
		return yOrg;
	}

	@Override
	public void init(BaseFont baseFont, Float fontSize,
			List<com.pegalinas.model.SimpleTextInput> texts,
			com.pegalinas.model.Icono icon) {
		// TODO Auto-generated method stub

	}

	public BasePegalinas create(BaseFont baseFont, Float fontSize,
			List<com.pegalinas.model.SimpleTextInput> texts, String iconPath,
			com.pegalinas.model.Icono icon) {
		// TODO Auto-generated method stub
		return null;
	}
}
