package com.pegalinas.pdf.multi.templetes;

import java.io.IOException;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;
import com.pegalinas.pdf.PdfUtils;
import com.pegalinas.pdf.templetes.BasePegalinas;
import com.pegalinas.pdf.templetes.TempletePegalinas;

/**
 * 
 *
 */
public class Header extends BasePegalinas implements TempletePegalinas {

	public Header(BaseFont baseFont, Float fontSize,
			List<SimpleTextInput> texts, String iconPath, Icono icon) {
		super(baseFont, fontSize, texts, iconPath, icon);
		if (fontSize == null) {
			fontSize = 8f;
		}
	}

	private float xOrg = 0;
	private float yOrg = PAGE_HEIGHT;
	public static final int PAGE_HEIGHT = 2840;
	private float baseScaleFactor = 2.84f;
	private float width = 35f * 2.84f;
	private float height = 5f * 2.84f;
	private float cornerWidth = 2.5f * 2.84f;
	private float rowOffset = 0f;
	private float columnOffset = 8f * 2.84f;
	private float leftMargin = 45f * 2.84f;
	private String idPedido = "";
	private int colCounter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.BasePegalinas#renderItem(com.lowagie.text
	 * .pdf.PdfContentByte)
	 */
	@Override
	public Float renderCutOutline(PdfContentByte cb) throws IOException {
		PdfUtils.renderRoundedBox(cb, xOrg, yOrg, width, height, cornerWidth);
		cb.closePathStroke();
		return (height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#renderBlock(com.lowagie
	 * .text.pdf.PdfContentByte, java.lang.Float)
	 */
	@Override
	public Float renderPrintBlock(PdfContentByte cb, Float startHeight)
			throws IOException {

		yOrg = startHeight;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			yOrg = yOrg - height;
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (colCounter = 0; colCounter < columns; colCounter++) {
				xOrg = leftMargin + colCounter * (width + columnOffset);
				renderItem(cb);
			}
		}
		return yOrg;
	}

	@Override
	public Float renderCutBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		logger.debug("Render Cut Block" + "header rowoffset=>" + rowOffset);
		yOrg = startHeight;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			yOrg = yOrg - height;
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				xOrg = leftMargin + colCounter * (width + columnOffset);
				renderCutOutline(cb);
			}
		}
		return yOrg;
	}

	@Override
	public Float renderItem(PdfContentByte cb) throws IOException {
		logger.debug("Render Item");
		float adjFontSize = 0;
		logger.debug("***** HEADER " + texts.size() + " - " + colCounter);
		if (texts.size() > 0) {
			if (texts.get(colCounter).getText() == null) {
				logger.debug("Skipping null value");
			} else {
				cb.beginText();
				cb.setFontAndSize(baseFont, fontSize);
				Chunk frase = new Chunk(texts.get(colCounter).getText(),
						new Font(baseFont, fontSize));
				adjFontSize = fontSize * (width - 2 * baseScaleFactor)
						/ frase.getWidthPoint();
				float heightOffset;
				float widthOffset = (width) / 2;
				if (adjFontSize < fontSize) {
					heightOffset = (float) (Math.sqrt((fontSize - adjFontSize))
							* (height - frase.getTextRise()) / 8);
					cb.setFontAndSize(baseFont, adjFontSize);
				} else {
					heightOffset = height / 8;
				}
				cb.showTextAligned(PdfContentByte.ALIGN_CENTER,
						texts.get(colCounter).getText(), xOrg + widthOffset,
						yOrg + heightOffset + 2.0f, 0);
				cb.endText();
			}
		}
		if (debugOn) {
			renderCutOutline(cb);
		}
		return yOrg;
	}

	@Override
	public Float getExpectedBlockHeight() {
		return height * rows;
	}

	@Override
	public String getIdPedido() {
		return idPedido;
	}

	@Override
	public void init(BaseFont baseFont, Float fontSize,
			List<SimpleTextInput> texts, Icono icon) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setIdPedido(String idLabel) {
		idPedido = idLabel;

	}

	public Header() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BasePegalinas create(BaseFont baseFont, Float fontSize,
			List<SimpleTextInput> texts, String iconPath, Icono icon) {
		return new Header(baseFont, fontSize, texts, iconPath, icon);
	}

}
