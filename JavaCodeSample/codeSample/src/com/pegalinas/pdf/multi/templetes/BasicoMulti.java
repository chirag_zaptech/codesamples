package com.pegalinas.pdf.multi.templetes;

/**
 *
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;
import com.pegalinas.pdf.PdfUtils;

public class BasicoMulti extends BasePegalinasMulti implements
		TempletePegalinasMulti {

	public static final int PAGE_HEIGHT = 2840;
	private static final Logger logger = Logger.getLogger(BasicoMulti.class);

	protected float columnOffset = 4f * 2.84f;
	protected float leftMargin = 45f * 2.84f;
	protected float cornerWidth = 4.5f * 2.84f;
	protected String idPedido = "";
	public static String fontFlag = null;

	public BasicoMulti(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconList) {
		super(baseFont, fontSize, texts, iconPath, iconList);
		System.out.println("BusicoMulti Constructor()" + BasicoMulti.class);
		width = 56f * 2.84f;
		height = 9f * 2.84f;
		logger.info("Setting width and height w:" + width + " h:" + height);
	}

	@Override
	public Float getExpectedBlockHeight() {
		Float height = 0f;
		if (rows > 10) {
			height = rows * Double.valueOf(this.height).floatValue() + 20;

		} else {
			height = rows * Double.valueOf(this.height).floatValue();
		}
		return height;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.BasePegalinas#renderItem(com.lowagie.text
	 * .pdf.PdfContentByte)
	 */
	// @Override
	public Float renderCutOutline(PdfContentByte cb, int i) throws IOException {
		// System.out.println(yOrg+"<===Yorg");
		PdfUtils.renderRoundedBox(cb, xOrg, yOrg, width, height, cornerWidth);
		cb.closePathStroke();
		return (height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#renderBlock(com.lowagie
	 * .text.pdf.PdfContentByte, java.lang.Float)
	 */
	@Override
	public Float renderPrintBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		System.out.println("renderPrintBlock()" + BasicoMulti.class);
		yOrg = startHeight;
		// System.out.println(texts);
		int i = 0;
		for (List<SimpleTextInput> text : texts) {
			for (SimpleTextInput textString : text) {
				List<SimpleTextInput> textStringValue = new ArrayList<SimpleTextInput>();
				textStringValue.add(textString);
				calculateOffsetsAndFontSize(textStringValue, i);
			}
			boolean checkRow = true;
			if (i % 5 == 0) {
				yOrg = yOrg - height;
			}

			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			xOrg = leftMargin + i % 5 * (width + columnOffset);
			renderItem(cb, i);
			i++;
		}
		return yOrg;
	}

	@Override
	public Float renderCutBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		yOrg = startHeight;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			yOrg = yOrg - height;
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				xOrg = leftMargin + colCounter * (width + columnOffset);
				renderCutOutline(cb);
			}
		}
		return yOrg;
	}

	@Override
	public Float renderItem(PdfContentByte cb, int i) throws IOException {
		List<SimpleTextInput> textvalue = texts.get(i);
		if (textvalue.size() > 0) {

			cb.beginText();
			cb.setFontAndSize(baseFont.get(i), fontSize);
			logger.debug("Show text @ " + xOrg + ", " + yOrg);
			float descent = baseFont.get(i).getFontDescriptor(BaseFont.DESCENT,
					fontSize);
			if (BasicoMulti.fontFlag != null
					&& BasicoMulti.fontFlag.equals("107")) {
				cb.showTextAligned(PdfContentByte.ALIGN_CENTER, textvalue
						.get(0).toString(), xOrg + widthOffset + 8, yOrg
						- descent - 4, 0);
			} else {
				cb.showTextAligned(PdfContentByte.ALIGN_CENTER, textvalue
						.get(0).toString(), xOrg + widthOffset + 8, yOrg
						- descent + heightOffset + 2f, 0);
			}
			cb.endText();
		}
		if (iconFile != null) {
			PdfReader reader = new PdfReader(iconFile.get(i));
			PdfImportedPage iconoPdf = cb.getPdfWriter().getImportedPage(
					reader, 1);
			iconoPdf.setHorizontalScaling(10);
			float scaleFactor = 0.25f;
			Icono icon = new Icono();
			cb.addTemplate(iconoPdf, scaleFactor, 0, 0, scaleFactor, xOrg
					- cornerWidth / 2 + width - iconoPdf.getWidth()
					* scaleFactor + icon.getOffsetX(), yOrg + height / 2
					- iconoPdf.getHeight() * scaleFactor / 2);
		}
		if (debugOn) {
			renderCutOutline(cb, i);
		}
		return yOrg;
	}

	@Override
	public String getIdPedido() {
		return idPedido;
	}

	@Override
	public void setIdPedido(String idLabel) {
		idPedido = idLabel;

	}

	public BasicoMulti() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public BasePegalinasMulti create(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconlist) {
		// TODO Auto-generated method stub
		return new BasicoMulti(baseFont, fontSize, texts, iconPath, iconlist);
	}

	@Override
	public void init(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, List<Icono> icon) {

		new BasicoMulti(baseFont, fontSize, texts, iconPath, iconList);
	}

	@Override
	public String toString() {
		return "BasicoMulti [baseFont=" + baseFont + ", fontSize=" + fontSize
				+ ", texts=" + texts + ", iconPath=" + iconPath + ", iconList="
				+ iconList + "]";
	}

	@Override
	public Float renderCutOutline(PdfContentByte cb) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
