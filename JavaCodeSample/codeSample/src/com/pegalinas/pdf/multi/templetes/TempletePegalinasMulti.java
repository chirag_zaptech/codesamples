package com.pegalinas.pdf.multi.templetes;

import java.io.IOException;
import java.util.List;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;

/**
 * Interface for multiple template type generation
 */
public interface TempletePegalinasMulti {

	public Float renderPrintBlock(PdfContentByte cb, Float startHeight)
			throws IOException;

	public Float renderCutBlock(PdfContentByte cb, Float startHeight)
			throws IOException;

	public void init(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, List<Icono> icon);

	public Float getExpectedBlockHeight();

	public String getIdPedido();

	public void setRows(int rows);

	public void setIdPedido(String idLabel);

	public BasePegalinasMulti create(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconlist);
}
