package com.pegalinas.pdf.multi.templetes;

/**
 * 
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;

/**
 *
 *
 */
public abstract class BasePegalinasMulti {
	public static final int PAGE_HEIGHT = 2840;

	protected static final Logger logger = Logger
			.getLogger(BasePegalinasMulti.class);
	protected List<BaseFont> baseFont = null;
	protected Float fontSize = null;
	protected List<String> iconFile = null;
	protected float widthMargin = 36;
	protected float heightMargin = 5;
	protected List<List<SimpleTextInput>> texts = null;
	protected boolean debugOn = false;
	protected String iconPath = null;
	protected List<Icono> iconList = null;
	protected Float dpi = 72f;
	protected float heightOffset;
	protected float widthOffset;
	protected float width = 1f;
	protected float height = 9f * 2.84f;

	protected float xOrg = 0;
	protected float yOrg = PAGE_HEIGHT;

	protected Double cmToPoints(Double cm) {
		return dpi * cm / 2.54;
	}

	protected Float cmToPoints(Float cm) {
		return dpi * cm / 2.54f;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public List<Icono> getIcon() {
		return iconList;
	}

	public void setIcon(List<Icono> iconList) {
		this.iconList = iconList;
	}

	public boolean isDebugOn() {
		return debugOn;
	}

	public void setDebugOn(boolean debugOn) {
		this.debugOn = debugOn;
	}

	protected int rows = 0;

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	protected int columns = 0;

	/**
	 * Renders the outline of the Pegalina for the cutter to follow
	 * 
	 * @param cb
	 * @return
	 * @throws IOException
	 */
	public abstract Float renderCutOutline(PdfContentByte cb)
			throws IOException;

	/**
	 * Renders the content of the Pegalina for the printer
	 * 
	 * @param cb
	 * @return
	 * @throws IOException
	 */
	public abstract Float renderItem(PdfContentByte cb, int i)
			throws IOException;

	/**
	 * calculate font and off set as per template type.
	 * 
	 * @param textString
	 * @param i
	 */
	protected void calculateOffsetsAndFontSize(
			List<SimpleTextInput> textString, int i) {
		logger.info("Calculate Offset and FontSize for area : " + width + " x "
				+ height + " - " + fontSize);
		float hAdjFontSize = 0;
		float vAdjFontSize = 0;
		float adjFontSize = fontSize;
		float ascent;
		float descent;
		try {
			if (textString.size() > 0) {
				for (SimpleTextInput simpleTextInput : textString) {
					float txtheight = height;
					logger.debug("Setting font size with text "
							+ simpleTextInput.getText()
							+ " starting at font size " + fontSize
							+ " basefont " + baseFont);
					Chunk frase = new Chunk(simpleTextInput.getText(),
							new Font(baseFont.get(0), fontSize));
					float widthPoint = frase.getWidthPoint();
					ascent = baseFont.get(0).getFontDescriptor(BaseFont.ASCENT,
							fontSize);
					descent = baseFont.get(0).getFontDescriptor(
							BaseFont.DESCENT, fontSize);
					logger.debug("Widthpoint : " + widthPoint + " ascent : "
							+ ascent + " descent : " + descent);
					float heightPoint = ascent - descent;
					logger.debug("Point comparison - width : " + width
							+ " height : " + height + ", widthPoint : "
							+ widthPoint + " heightPoint : " + heightPoint
							+ " Margins : w " + widthMargin + " h "
							+ heightMargin);
					hAdjFontSize = fontSize * (width - widthMargin)
							/ widthPoint;
					vAdjFontSize = fontSize * txtheight / heightPoint;
					logger.debug("FontSize : " + fontSize + " hAdjFontSize : "
							+ hAdjFontSize + " vAdjFontSize : " + vAdjFontSize);
					adjFontSize = Math.min(adjFontSize,
							Math.min(vAdjFontSize, hAdjFontSize));
				}
				fontSize = adjFontSize;
				logger.debug("Using font size : " + fontSize);
				widthOffset = (width - widthMargin) / 2;
				descent = baseFont.get(0).getFontDescriptor(BaseFont.DESCENT,
						fontSize);
				ascent = baseFont.get(0).getFontDescriptor(BaseFont.ASCENT,
						fontSize);
				logger.debug("At render ascent : " + ascent + " descent : "
						+ descent);
				heightOffset = (height - heightMargin - (ascent - descent)) / 2
						- descent;
				heightOffset = heightOffset - 4;
				logger.debug("Render " + xOrg + " - " + yOrg + ","
						+ widthOffset + " - " + heightOffset);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param baseFont2
	 * @param fontSize
	 * @param texts
	 * @param iconPath
	 * @param iconList
	 */
	public BasePegalinasMulti(List<BaseFont> baseFont2, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconList) {
		super();
		System.out.println("BasePegalinasMulti() " + BasePegalinasMulti.class);
		this.baseFont = new ArrayList<BaseFont>();
		Iterator<BaseFont> iteratorfont = baseFont2.iterator();
		while (iteratorfont.hasNext()) {
			BaseFont baseFontvalue = iteratorfont.next();
			this.baseFont.add(baseFontvalue);
			// System.out.println("Itreating font==>"+this.baseFont);

		}
		this.baseFont = baseFont2;
		this.fontSize = fontSize;
		this.iconPath = iconPath;
		List<String> iconFileList = new ArrayList<String>();
		Iterator<Icono> iterator = iconList.iterator();
		this.iconFile = new ArrayList<String>();
		while (iterator.hasNext()) {
			Icono icono = iterator.next();
			if (icono == null) {
				icono.setFilename(null);
			}
			iconFileList.add(icono.getFilename());
			this.iconFile.add(iconPath + icono.getFilename());
		}
		this.texts = new ArrayList<List<SimpleTextInput>>();
		Iterator<List<SimpleTextInput>> iterator2 = texts.iterator();
		while (iterator2.hasNext()) {
			List<com.pegalinas.model.SimpleTextInput> list = iterator2
					.next();
			this.texts.add(list);
		}
		System.out.println(this.baseFont);
		this.iconList = iconList;
	}

	public BasePegalinasMulti() {
		super();
	}
}
