package com.pegalinas.pdf.multi.templetes;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;
import com.pegalinas.pdf.PdfUtils;

/**
 * 
 *
 */
public class MiniMulti extends BasePegalinasMulti implements
		TempletePegalinasMulti {

	public MiniMulti(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconList) {

		super(baseFont, fontSize, texts, iconPath, iconList);
		width = 35f * 2.84f;
		height = 5f * 2.84f;
		logger.info("Setting width and height w:" + width + " h:" + height);

		widthMargin = 6;
		heightMargin = 2;

		if (fontSize == null) {

			fontSize = 4f * 2.8f;
		}
	}

	private float cornerWidth = 2.5f * 2.84f;
	private float columnOffset = 8f * 2.84f;
	private float leftMargin = 45f * 2.84f;
	private String idPedido = "";
	private static final Logger logger = Logger.getLogger(MiniMulti.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.BasePegalinas#renderItem(com.lowagie.text
	 * .pdf.PdfContentByte)
	 */
	@Override
	public Float renderCutOutline(PdfContentByte cb) throws IOException {
		PdfUtils.renderRoundedBox(cb, xOrg, yOrg, width, height, cornerWidth);
		cb.closePathStroke();
		return (height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#renderBlock(com.lowagie
	 * .text.pdf.PdfContentByte, java.lang.Float)
	 */
	@Override
	public Float renderPrintBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		yOrg = startHeight;
		int i = 0;
		for (List<SimpleTextInput> textsvalue : texts) {

			calculateOffsetsAndFontSize(textsvalue.subList(0, 1), i);
			for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
				yOrg = yOrg - height;
				if (yOrg < 0) {
					cb.getPdfDocument().newPage();
					yOrg = PAGE_HEIGHT;
				}
				for (int colCounter = 0; colCounter < columns; colCounter++) {
					xOrg = leftMargin + colCounter * (width + columnOffset);
					renderItem(cb, i);
				}
			}
			i++;
		}
		return yOrg;
	}

	@Override
	public Float renderCutBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		logger.debug("Render Cut Block");
		yOrg = startHeight;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			yOrg = yOrg - height;
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				xOrg = leftMargin + colCounter * (width + columnOffset);
				renderCutOutline(cb);
			}
		}
		return yOrg;
	}

	@Override
	public Float renderItem(PdfContentByte cb, int i) throws IOException {
		if (debugOn) {
			renderCutOutline(cb);
		}
		logger.debug("Render Item");
		// float adjFontSize=0;

		for (List<SimpleTextInput> textInputs : texts) {

			if (textInputs.size() > 0) {
				cb.beginText();
				logger.debug("Show text @ " + xOrg + ", " + yOrg
						+ " with fontsize " + fontSize);

				cb.endText();
				i++;
			}
		}
		return yOrg;
	}

	@Override
	public Float getExpectedBlockHeight() {
		return height * rows;
	}

	@Override
	public String getIdPedido() {
		return idPedido;
	}

	public void init(BaseFont baseFont, Float fontSize,
			List<SimpleTextInput> texts, Icono icon) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setIdPedido(String idLabel) {
		idPedido = idLabel;

	}

	public MiniMulti() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public BasePegalinasMulti create(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconlist) {
		return null;
	}

	@Override
	public String toString() {
		return "MiniMulti [cornerWidth=" + cornerWidth + ", columnOffset="
				+ columnOffset + ", leftMargin=" + leftMargin + ", idPedido="
				+ idPedido + ", baseFont=" + baseFont + ", texts=" + texts
				+ ", iconList=" + iconList + "]";
	}

	@Override
	public void init(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, List<Icono> icon) {
		// TODO Auto-generated method stub

	}

}
