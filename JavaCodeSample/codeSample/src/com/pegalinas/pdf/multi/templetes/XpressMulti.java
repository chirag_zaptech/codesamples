package com.pegalinas.pdf.multi.templetes;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.pegalinas.model.Icono;
import com.pegalinas.model.SimpleTextInput;

/**
 *
 */
public class XpressMulti extends BasePegalinasMulti implements
		TempletePegalinasMulti {

	private static final Logger logger = Logger.getLogger(XpressMulti.class);

	private float xOrg = 0;
	private float yOrg = 0;

	private float leftMargin = 5f;
	private float width = 2.3f;
	private float columnOffset = 0.2f;
	private float height = 2.3f;
	private float radius = 0.12f;

	public static final String DEFAULT_CHARSET = "UTF-8";
	private String svgPath = "";
	private String svgFile = "";
	protected String charSet = DEFAULT_CHARSET;
	public static String fontFlag = null;

	private BaseFont bf;

	public XpressMulti() {
		super();
		fontSize = 8f;
	}

	public XpressMulti(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconList) {
		super(baseFont, fontSize, texts, iconPath, iconList);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#renderPrintBlock(com.lowagie
	 * .text.pdf.PdfContentByte, java.lang.Float)
	 */
	@Override
	public Float renderPrintBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		int i = 0;
		if (logger.isDebugEnabled()) {
			logger.debug("Render print block (" + xOrg + "," + yOrg + ")");
		}
		yOrg = startHeight;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			yOrg = yOrg - cmToPoints(height);
			if (yOrg < 0) {
				cb.getPdfDocument().newPage();
				yOrg = PAGE_HEIGHT;
			}
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				if (colCounter % 2 == 0) {
					xOrg = cmToPoints(leftMargin) + colCounter
							* cmToPoints(width + columnOffset);
				} else {
					xOrg = cmToPoints(leftMargin) + colCounter
							* cmToPoints(width + columnOffset)
							- cmToPoints(columnOffset);
				}
				renderItem(cb, i);
				i++;
			}
		}

		return yOrg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pegalinas.pdf.templetes.TempletePegalinas#renderCutBlock(com.lowagie
	 * .text.pdf.PdfContentByte, java.lang.Float)
	 */
	@Override
	public Float renderCutBlock(PdfContentByte cb, Float startHeight)
			throws IOException {
		if (logger.isDebugEnabled()) {
			logger.debug("Render cut block (" + xOrg + "," + yOrg + ")");
		}
		yOrg = startHeight - cmToPoints(radius);
		xOrg = cmToPoints(leftMargin) + cmToPoints(radius);
		Float rowIncrease = 0f;
		for (int rowCounter = 0; rowCounter < rows; rowCounter++) {
			for (int colCounter = 0; colCounter < columns; colCounter++) {
				rowIncrease = renderCutOutline(cb);
				xOrg += rowIncrease;
				if ((colCounter + 1) % 2 == 0) {
					xOrg += 5;
				}
			}
			xOrg = cmToPoints(leftMargin + radius);
			yOrg = yOrg - rowIncrease;
			if ((rowCounter + 1) % 4 == 0) {
			}
		}
		return null;
	}

	/*
	 *Updateing height for template block
	 */
	@Override
	public Float getExpectedBlockHeight() {
		// TODO Auto-generated method stub
		return height * rows;
	}


	@Override
	public String getIdPedido() {
		return null;
	}

	
	@Override
	public void setIdPedido(String idLabel) {
		// TODO Auto-generated method stub

	}

	/*
	 * Creating outline(Xpress) for the template
	 */
	@Override
	public Float renderCutOutline(PdfContentByte cb) throws IOException {
		if (logger.isDebugEnabled()) {
			logger.debug("Render cut outline (" + xOrg + "," + yOrg + ")");
		}
		cb.circle(xOrg, yOrg, cmToPoints(radius));
		cb.stroke();
		cb.closePath();
		cb.circle(xOrg, yOrg, cmToPoints(radius));
		cb.fillStroke();
		cb.closePath();
		return cmToPoints(radius * 2);
	}

	/*
	 * Creating template 
	 */
	// @Override

	public void createPdf(PdfContentByte cb, int i) throws IOException,
			DocumentException {
		fontSize = 10f;
		for (Iterator iterator = baseFont.iterator(); iterator.hasNext();) {
			BaseFont baseFontValue = (BaseFont) iterator.next();
			cb.setFontAndSize(baseFont.get(i), fontSize);
			float origin[] = { xOrg + cmToPoints(.85f), yOrg + cmToPoints(.65f) };
			float ascent = baseFont.get(i).getFontDescriptor(BaseFont.ASCENT,
					fontSize);
			float radius = cmToPoints(1.05f) - (ascent + heightOffset);
			String text = texts.get(i).get(0).getText();
			if (text.length() % 2 == 0) {
				text = text + " ";
			}
			char[] arr = text.toCharArray();
			float angle = 0f;
			for (int j = 0; j < arr.length / 2 + 1; j++) {
				cb.beginText();
				String strl = "" + arr[(arr.length / 2) - j];
				String strr = "" + arr[(arr.length / 2) + j];
				float[] pointL = pointOnCircle(radius, 85 + angle, origin);
				float[] pointR = pointOnCircle(radius, 85 - angle, origin);
				cb.showTextAligned(PdfContentByte.ALIGN_CENTER, strl,
						pointL[0], pointL[1], angle);
				cb.showTextAligned(PdfContentByte.ALIGN_CENTER, strr,
						pointR[0], pointR[1], -angle);
				angle = angle + 360 / 21;
				cb.endText();
			}

		}
	}
	
	/**
	 * Convert from degrees to radians via multiplication by PI/180
	 */
	public static float[] pointOnCircle(float radius, float angleInDegrees,
			float[] origin) {
		float x = (float) (radius * Math.cos(angleInDegrees * Math.PI / 180F))
				+ origin[0];
		float y = (float) (radius * Math.sin(angleInDegrees * Math.PI / 180F))
				+ origin[1];
		float[] parimeterPoint = { x, y };

		return parimeterPoint;
	}

	

	public boolean fitsItem(float height) {
		return (height <= cmToPoints(this.height));
	}

	public void setPrintHeight(Float y) {
		yOrg = y;
	}

	public Float getPrintHeight() {
		return yOrg;
	}

	public BasePegalinasMulti create(BaseFont baseFont, Float fontSize,
			List<com.pegalinas.model.SimpleTextInput> texts, String iconPath,
			com.pegalinas.model.Icono icon) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, List<Icono> icon) {

		new XpressMulti(baseFont, fontSize, texts, iconPath, iconList);
	}

	@Override
	public BasePegalinasMulti create(List<BaseFont> baseFont, Float fontSize,
			List<List<SimpleTextInput>> texts, String iconPath,
			List<Icono> iconlist) {
		// TODO Auto-generated method stub
		return null;
	}
	/*
	 * Printing text on template
	 */
	@Override
	public Float renderItem(PdfContentByte cb, int i) throws IOException {

		if (logger.isDebugEnabled()) {
			logger.debug("Render item (" + xOrg + "," + yOrg + ")");
		}
		cb.setColorFill(BaseColor.BLACK);
		cb.circle(xOrg + cmToPoints(.85f), yOrg + cmToPoints(.65f),
				cmToPoints(1.15f));// circle
		try {
			createPdf(cb, i);

		} catch (Exception e) {
			logger.error("error in renderItem", e);
			e.printStackTrace();
		}

		cb.stroke();
		if (debugOn) {
		}
		/*
		 * Check for icon & importing icon Pdf
		 */
		if (!iconFile.get(i).equals("/templetespdf/icons/0.pdf")) {
			PdfReader reader = new PdfReader(iconFile.get(i));
			Icono icon = new Icono();
			
			PdfImportedPage iconoPdf = cb.getPdfWriter().getImportedPage(
					reader, 1);
			iconoPdf.setHorizontalScaling(10);
			float scaleFactor = 0.40f;
			cb.addTemplate(iconoPdf, scaleFactor, 0, 0, scaleFactor, xOrg
					+ cmToPoints(1.4f) - iconoPdf.getWidth() * scaleFactor
					+ icon.getOffsetX(),
					yOrg + cmToPoints(.70f) - iconoPdf.getHeight()
							* scaleFactor / 2);
		}
		return cmToPoints(radius * 2);

	}

	@Override
	public String toString() {
		return "XpressMulti [radius=" + radius + ", bf=" + bf + ", baseFont="
				+ baseFont + ", iconFile=" + iconFile + ", texts=" + texts
				+ ", iconList=" + iconList + "]";
	}

}
