package com.tamono.action;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.tamono.form.RenameFormBean;

/**
 * 
 * class to Rename Action for file
 * 
 */
public class RenameAction extends Action {

	@Override
	public ActionForward execute(ActionMapping am, ActionForm form,
			HttpServletRequest req, HttpServletResponse res) throws Exception {
		final String outputFolder = getServlet().getServletContext()
				.getRealPath("") + "/../output/";
		RenameFormBean rnfb = (RenameFormBean) form;
		String newName = rnfb.getNewName();
		String oldName = rnfb.getOldName();
		int id = rnfb.getId();
		System.out.println("oldName " + oldName);
		System.out.println("newName " + newName);
		String status = "renameFailed";
		File oldfile = null;
		File newfile = null;
		try {
			oldfile = new File(outputFolder + oldName + ".pdf");
			newfile = new File(outputFolder + newName + ".pdf");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (oldfile.renameTo(newfile)) {
			status = "renameSuccess";
			req.setAttribute("MSG", "renameSuccess");
		} else {
			System.out.println("Rename failed");
		}
		req.setAttribute("FNAME", newName);
		req.setAttribute("ID", id);
		req.setAttribute("MSG", status);
		return am.findForward(status);
	}

}
