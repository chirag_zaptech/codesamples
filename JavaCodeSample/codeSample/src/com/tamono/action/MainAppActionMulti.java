package com.tamono.multi.action;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

//import com.itextpdf.text.pdf.hyphenation.TernaryTree.Iterator;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;
import com.pegalinas.model.SimpleTextInput;
import com.pegalinas.pdf.TemplateBuilder;
import com.tamono.bean.MultiParameterDetail;
import com.tamono.form.PdfFormSubmit;
import com.tamono.function.CommonFuctions;
import com.tamono.jdbc.JDBCUtil;

/*
 * Multi template Action Class
 */
public class MainAppActionMulti extends Action {

	private ArrayList<Integer> getIdInt(String detailId) {
		String[] parts = detailId.split(",");
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (int i = 0; i < parts.length; i++) {
			parts[i] = parts[i].replace("check=", "");
			int val = Integer.parseInt(parts[i]);
			// System.out.println(val);
			ids.add(val);
			System.out.println(ids);
		}
		return ids;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping,
	 *      org.apache.struts.action.ActionForm,
	 *      javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping am, ActionForm af,
			HttpServletRequest req, HttpServletResponse res) throws Exception {
		System.out.println("====>Multi Action");
		PdfFormSubmit pdfFormSubmit = (PdfFormSubmit) af;
		String detailId = pdfFormSubmit.getDetailId();
		String submitFileName = pdfFormSubmit.getName();
		String status = "failed";
		String downloadName = null;
		String resourcePath = "/templetespdf";
		Statement st = null;
		Statement st1 = null;
		Statement st2 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		Connection conn = null;
		ArrayList<Integer> detailIdInt = getIdInt(detailId);
		Iterator iterator = detailIdInt.iterator();
		try {
			/*
			 * creating parameter detail object
			 */
			ArrayList<MultiParameterDetail> multiParameterDetailsList = new ArrayList<MultiParameterDetail>();
			while (iterator.hasNext()) {
				int ids = (int) iterator.next();
				System.out.println(ids);
				try {
					conn = JDBCUtil.getConnection();
					st = (Statement) conn.createStatement();
					String query = "SELECT * FROM `pr_cart_product_multiple` WHERE `id_cart`="
							+ ids + " ORDER BY `label_rank` ASC";
					System.out.println(query);
					rs = (ResultSet) st.executeQuery(query);
					System.out.println("after query");
					MultiParameterDetail mulitparam = new MultiParameterDetail();
					ArrayList<Integer> labelRank = new ArrayList<Integer>();
					ArrayList<Integer> backgroundColorId = new ArrayList<Integer>();
					ArrayList<Integer> iconid = new ArrayList<Integer>();
					ArrayList<Integer> fontId = new ArrayList<Integer>();
					ArrayList<List<SimpleTextInput>> textList = new ArrayList<List<SimpleTextInput>>();
					int cartId = 0;
					int quantity = 0;
					int idProduct = 0;

					while (rs.next()) {
						cartId = rs.getInt("id_cart");
						quantity = rs.getInt("quantity");
						idProduct = rs.getInt("id_product");
						labelRank.add(rs.getInt("label_rank"));
						backgroundColorId.add(rs.getInt("background_color_id"));
						iconid.add(rs.getInt("icon_id"));
						fontId.add(rs.getInt("font_id"));
						CommonFuctions commomFuction = new CommonFuctions();
						textList.add(commomFuction.getSimpleTextInputsList(rs
								.getString("text_values")));
					}
					mulitparam.setIdCart(cartId);
					mulitparam.setQuantity(quantity);
					mulitparam.setBackgroundColorId(backgroundColorId);
					mulitparam.setFontId(fontId);
					mulitparam.setIconid(iconid);
					mulitparam.setLabelRank(labelRank);
					mulitparam.setIdProduct(idProduct);
					mulitparam.setTextList(textList);

					multiParameterDetailsList.add(mulitparam);

					/**
					 * Setting header attribute
					 * 
					 */
					MultiParameterDetail mulitparamHeader = new MultiParameterDetail();
					ArrayList<Integer> backgroundColorIdHeader = new ArrayList<Integer>();
					backgroundColorIdHeader.add(backgroundColorId.get(0));
					ArrayList<Integer> iconidHeader = new ArrayList<Integer>();
					iconidHeader.add(null);
					ArrayList<Integer> fontIdHeader = new ArrayList<Integer>();
					fontIdHeader.add(null);
					ArrayList<List<SimpleTextInput>> textListHeader = new ArrayList<List<SimpleTextInput>>();
					textListHeader.add(null);
					int cartIdHeader = cartId;
					int quantityheader = quantity;
					int idProductHeader = idProduct;
					mulitparamHeader.setQuantity(quantityheader);
					mulitparamHeader.setIdCart(cartIdHeader);
					mulitparamHeader.setIdProduct(idProductHeader);
					mulitparamHeader
							.setBackgroundColorId(backgroundColorIdHeader);
				} catch (Exception e) {
					status = "failed";
					e.printStackTrace();
					req.setAttribute("MSG", status);
					return am.findForward(status);
				} finally {
					if (st != null) {
						st.close();
					}
					if (rs != null) {
						rs.close();
					}
					conn.close();
				}
			}
			String fontPath = resourcePath + "/fonts/";
			String iconPath = resourcePath + "/icons/";
			TemplateBuilder builder = new TemplateBuilder();
			builder.setFontPath(fontPath);
			builder.setIconPath(iconPath);
			builder.init();
			ServletContext ctx = getServlet().getServletContext();
			String outputFile = null;
			Calendar cal1 = new GregorianCalendar();
			try {
				SimpleDateFormat date_format = new SimpleDateFormat(
						"yyyyMMdd_HHmmss");
				String fileName = date_format.format(cal1.getTime());
				if (submitFileName.equals("")) {
					downloadName = fileName + ".pdf";
				} else {
					downloadName = submitFileName + ".pdf";
					fileName = submitFileName;
				}
				File theDir = new File(ctx.getRealPath("") + "/../output/");
				if (!theDir.exists()) {
					System.out.println("creating directory: "
							+ ctx.getRealPath("") + "/../output/");
					boolean result = theDir.mkdir();
					if (result) {
						outputFile = ctx.getRealPath("") + "/../output/"
								+ fileName + ".pdf";
						System.out.println("DIR created");
					}
				} else {
					outputFile = ctx.getRealPath("") + "/../output/" + fileName
							+ ".pdf";
				}
			} catch (Exception e) {
				e.printStackTrace();
				req.setAttribute("MSG", status);
				return am.findForward(status);
			}

			try {
				builder.printMultiPdfToFile(multiParameterDetailsList,
						outputFile);
				try {
					Statement st3 = (Statement) conn.createStatement();
					String query = "INSERT INTO `pdf_generation_log` (`transaction_id`, `filename`, `generated_date`, `date_added`) VALUES (NULL, "
							+ "'"
							+ downloadName
							+ "'"
							+ ", CURRENT_DATE(), CURRENT_TIMESTAMP)";
					System.out.println(query);
					st3.executeUpdate(query);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
				req.setAttribute("MSG", status);
				return am.findForward(status);
			}
		} catch (Exception e) {
			e.printStackTrace();
			req.setAttribute("MSG", status);
			return am.findForward(status);
		} finally {
			{
				if (st1 != null) {
					st1.close();
				}
				if (st2 != null) {
					st2.close();
				}
				if (st != null) {
					st.close();
				}
				if (rs1 != null) {
					rs1.close();
				}
				if (rs2 != null) {
					rs2.close();
				}
				if (rs != null) {
					rs.close();
				}
				conn.close();
			}
		}
		status = "success";
		System.out.println(submitFileName + " generated succefully");
		req.setAttribute("MSG", status);

		return am.findForward(status);
	}

}
