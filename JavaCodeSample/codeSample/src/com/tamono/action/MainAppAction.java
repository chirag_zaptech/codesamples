package com.tamono.action;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;
import com.pegalinas.model.SimpleTextInput;
import com.pegalinas.pdf.TemplateBuilder;
import com.pegalinas.pdf.TemplateParameters;
import com.tamono.bean.ParameterDetail;
import com.tamono.form.PdfFormSubmit;
import com.tamono.jdbc.JDBCUtil;

public class MainAppAction extends Action {

	private String getIdString(String detailId) {
		String[] parts = detailId.split(",");
		String detailIdString = "(";
		for (int i = 0; i < parts.length; i++) {
			parts[i] = parts[i].replace("check=", "");
			if (parts.length == i + 1) {
				detailIdString = detailIdString + parts[i] + ")";
			} else {
				detailIdString = detailIdString + parts[i] + ",";
			}
		}
		System.out.println(detailIdString);
		return detailIdString;
	}

	/*
	 * / (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping am, ActionForm af,
			HttpServletRequest req, HttpServletResponse res) throws Exception {
		PdfFormSubmit pdfFormSubmit = (PdfFormSubmit) af;
		String detailId = pdfFormSubmit.getDetailId();
		String submitFileName = pdfFormSubmit.getName();
		String status = "failed";
		String downloadName = null;
		String resourcePath = "/templetespdf";
		Statement st = null;
		Statement st1 = null;
		Statement st2 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		Connection conn = null;
		String detailIdString = getIdString(detailId);
		try {

			/*
			 * Creating connection
			 */
			
			conn = JDBCUtil.getConnection();
			st = (Statement) conn.createStatement();
			String query1 = "SELECT pcp.*, pro.id_order FROM  `pr_cart_product` pcp	"
					+ "LEFT JOIN pr_orders pro ON pcp.id_cart = pro.id_cart where pcp.id_detail IN "
					+ detailIdString + " ";
			rs = (ResultSet) st.executeQuery(query1);
			List<TemplateParameters> parameters = new ArrayList<TemplateParameters>();
			while (rs.next()) {
				ParameterDetail parameterDetail = new ParameterDetail();
				TemplateParameters parameter = new TemplateParameters();
				if (rs.getInt("icon_id") != 0 && rs.getInt("icon_id") != 251) {
					parameterDetail.setIcon_id(rs.getInt("icon_id"));
					parameter.setIconNameId(parameterDetail.getIcon_id());
				} else {
					System.out.println("value equal zero");
					parameterDetail.setIcon_id(rs.getInt("icon_id"));
					parameter.setIconNameId(null);
				}
				parameterDetail.setFont_id(rs.getInt("font_id"));
				parameterDetail.setText_values(rs.getString("text_values"));
				parameterDetail.setQuantity(rs.getInt("quantity"));
				parameterDetail.setId_product(rs.getInt("id_product"));
				parameterDetail.setId_order(rs.getInt("id_order"));
				parameter.setQuantity(parameterDetail.getQuantity());
				parameter.setTemplateId(parameterDetail.getId_product());
				parameter.setFontNameId(parameterDetail.getFont_id());// Default

				List<SimpleTextInput> texts = new ArrayList<SimpleTextInput>();
				String wholeText = parameterDetail.getText_values();
				if (wholeText == null) {
					wholeText = "";
				}
				try {
					String[] splitText = wholeText.split("\\|");
					for (int j = 0; j < splitText.length; j++) {
						String text = splitText[j];
						SimpleTextInput textTest = new SimpleTextInput(text,
								false);
						texts.add(textTest);
					}
				} catch (Exception e) {
					e.printStackTrace();
					conn.close();
					req.setAttribute("MSG", status);
					return am.findForward(status);
				}
				parameter.setTexts(texts);
				parameter.setOrderId(parameterDetail.getId_order());

				/*
				 * Header Property Set
				 */
				try {
					st1 = (Statement) conn.createStatement();
					rs1 = (ResultSet) st1
							.executeQuery("SELECT * FROM `pr_attribute_lang` WHERE `id_attribute` IN ("
									+ rs.getString("font_id")
									+ ","
									+ rs.getString("icon_id")
									+ ","
									+ rs.getString("foreground_color_id")
									+ ","
									+ rs.getString("background_color_id")
									+ ") AND `id_lang` = '7'");

					while (rs1.next()) {

						System.out.println(rs1.getString("name")
								+ "Header Values are here");
						if (rs1.getInt("id_attribute") == parameterDetail
								.getFont_id()) {
							String fnm = "(" + parameterDetail.getFont_id()
									+ ")" + rs1.getString("name");
							parameter.setFontName(fnm);
						}
						if (rs1.getInt("id_attribute") == rs.getInt("icon_id")) {
							parameter.setIconName(rs1.getString("name"));
						}
						if (rs1.getInt("id_attribute") == rs
								.getInt("foreground_color_id")) {
							parameter.setForegroundColor(rs1.getString("name"));
						}
						if (rs1.getInt("id_attribute") == rs
								.getInt("background_color_id")) {
							parameter.setBackgroundColor(rs1.getString("name"));
						}
					}

					if (parameter.getBackgroundColor() == null) {
						parameter.setBackgroundColor("Fondo: NA");
					} else {
						parameter.setBackgroundColor("("
								+ rs.getInt("background_color_id") + ") "
								+ parameter.getBackgroundColor());
					}

					if (parameter.getForegroundColor() == null) {
						parameter.setForegroundColor("Letra: NA");
					} else {
						parameter.setForegroundColor("("
								+ rs.getInt("foreground_color_id") + ") "
								+ parameter.getForegroundColor());
					}
					if (parameter.getIconName() == null
							|| rs.getInt("icon_id") == 251) {
						parameter.setIconName("SIN ICONO");
					} else {
						parameter.setIconName("(" + rs.getInt("icon_id") + ") "
								+ parameter.getIconName());
					}
				} catch (Exception e) {
					e.printStackTrace();
					req.setAttribute("MSG", status);
					return am.findForward(status);
				} finally {
					rs1.close();
					st1.close();
				}

				/*
				 * Setting templetetype
				 */
				try {
					st2 = (Statement) conn.createStatement();
					rs2 = (ResultSet) st2
							.executeQuery("SELECT * FROM  `pr_product_lang` WHERE `id_product` = "
									+ rs.getInt("id_product")
									+ "  AND `id_lang` = '7'");
					while (rs2.next()) {

						parameter.setTemplateName(rs2.getString("name"));
						System.out.println(parameter.getTemplateName());
						System.out.println(parameter.getTemplateId());
					}
				} catch (Exception e) {
					e.printStackTrace();
					req.setAttribute("MSG", status);
					return am.findForward(status);
				} finally {
					rs2.close();
					st2.close();
				}
				parameters.add(parameter);
			}
			String fontPath = resourcePath + "/fonts/";
			String iconPath = resourcePath + "/icons/";
			TemplateBuilder builder = new TemplateBuilder();
			builder.setFontPath(fontPath);
			builder.setIconPath(iconPath);
			builder.init();
			ServletContext ctx = getServlet().getServletContext();
			String outputFile = null;
			Calendar cal1 = new GregorianCalendar();
			try {
				SimpleDateFormat date_format = new SimpleDateFormat(
						"yyyyMMdd_HHmmss");
				String fileName = date_format.format(cal1.getTime());
				if (submitFileName.equals("")) {
					downloadName = fileName + ".pdf";
				} else {
					downloadName = submitFileName + ".pdf";
					fileName = submitFileName;
				}
				File theDir = new File(ctx.getRealPath("") + "/../output/");
				if (!theDir.exists()) {
					System.out.println("creating directory: "
							+ ctx.getRealPath("") + "/../output/");
					boolean result = theDir.mkdir();
					if (result) {
						outputFile = ctx.getRealPath("") + "/../output/"
								+ fileName + ".pdf";
						System.out.println("DIR created");
					}
				} else {
					outputFile = ctx.getRealPath("") + "/../output/" + fileName
							+ ".pdf";
				}
			} catch (Exception e) {
				e.printStackTrace();
				req.setAttribute("MSG", status);
				return am.findForward(status);
			}
			try {
				builder.printPdfToFile(parameters, outputFile);
				try {
					/*
					 * Inserting the record for created template
					 */
					
					Statement st3 = (Statement) conn.createStatement();
					String query = "INSERT INTO `pdf_generation_log` (`transaction_id`, `filename`, `generated_date`, `date_added`) VALUES (NULL, "
							+ "'"
							+ downloadName
							+ "'"
							+ ", CURRENT_DATE(), CURRENT_TIMESTAMP)";
					System.out.println(query);
					st3.executeUpdate(query);
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
			} catch (Exception e) {
				e.printStackTrace();
				req.setAttribute("MSG", status);
				return am.findForward(status);
			}
		} catch (Exception e) {
			status = "failed";
			e.printStackTrace();
			req.setAttribute("MSG", status);
			return am.findForward(status);
		} finally {
			if (st1 != null) {
				st1.close();
			}
			if (st2 != null) {
				st2.close();
			}
			if (st != null) {
				st.close();
			}
			if (rs1 != null) {
				rs1.close();
			}
			if (rs2 != null) {
				rs2.close();
			}
			if (rs != null) {
				rs.close();
			}
			conn.close();
		}

		status = "success";
		System.out.println(submitFileName + " generated succefully");
		req.setAttribute("MSG", status);
		return am.findForward(status);
	}

}
