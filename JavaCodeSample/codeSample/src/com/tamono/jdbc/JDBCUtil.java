package com.tamono.jdbc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.jdbc.Connection;

/*
 * Connection class 
 */
public class JDBCUtil {

	public static String db_driver;
	public static String db_url;
	public static String db_username;
	public static String db_password;

	/*
	 * Get Connection properties from property file.
	 */
	public static Connection getConnection() throws ClassNotFoundException,
			SQLException {
		System.out.println("get connection");
		Connection connection = null;
		Properties prop = new Properties();
		try {
			prop.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("dbDetail.properties"));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		db_driver = prop.getProperty("db_driver");
		db_url = prop.getProperty("db_url");
		db_username = prop.getProperty("db_username");
		db_password = prop.getProperty("db_password");
		Class.forName(db_driver);
		connection = (Connection) DriverManager.getConnection(db_url,
				db_username, db_password);
		return connection;
	}

}
