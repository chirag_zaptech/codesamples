package com.tamono.bean;

/*
 * Class to hold the property of templates.
 */
public class ParameterDetail {

	int id_cart;
	int id_product;
	int id_address_delivery;
	int id_shop;
	int id_product_attribute;
	int quantity;
	String instructions;
	String instructions_valid;
	String instructions_id;
	int foreground_color_id;
	int background_color_id;
	int font_id;
	String text_values;
	String extra_values;
	int product_status;
	int id_detail;
	int id_order;
	Integer Icon_id;

	public void setIcon_id(Integer icon_id) {

		Icon_id = icon_id;
	}

	public void setId_order(int id_order) {

		this.id_order = id_order;
	}

	public int getId_cart() {

		return id_cart;
	}

	public void setId_cart(int id_cart) {

		this.id_cart = id_cart;
	}

	public int getId_product() {

		return id_product;
	}

	public void setId_product(int id_product) {

		this.id_product = id_product;
	}

	public int getId_address_delivery() {

		return id_address_delivery;
	}

	public void setId_address_delivery(int id_address_delivery) {

		this.id_address_delivery = id_address_delivery;
	}

	public int getId_shop() {

		return id_shop;
	}

	public void setId_shop(int id_shop) {

		this.id_shop = id_shop;
	}

	public int getId_product_attribute() {

		return id_product_attribute;
	}

	public void setId_product_attribute(int id_product_attribute) {

		this.id_product_attribute = id_product_attribute;
	}

	public int getQuantity() {

		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getInstructions() {

		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getInstructions_valid() {

		return instructions_valid;
	}

	public void setInstructions_valid(String instructions_valid) {

		this.instructions_valid = instructions_valid;
	}

	public String getInstructions_id() {

		return instructions_id;
	}

	public void setInstructions_id(String instructions_id) {

		this.instructions_id = instructions_id;
	}

	public int getForeground_color_id() {

		return foreground_color_id;
	}

	public void setForeground_color_id(int foreground_color_id) {

		this.foreground_color_id = foreground_color_id;
	}

	public int getBackground_color_id() {

		return background_color_id;
	}

	public void setBackground_color_id(int background_color_id) {

		this.background_color_id = background_color_id;
	}

	public int getFont_id() {

		return font_id;
	}

	public void setFont_id(int font_id) {

		this.font_id = font_id;
	}

	public String getText_values() {

		return text_values;
	}

	public void setText_values(String text_values) {

		this.text_values = text_values;
	}

	public String getExtra_values() {

		return extra_values;
	}

	public void setExtra_values(String extra_values) {

		this.extra_values = extra_values;
	}

	public int getProduct_status() {

		return product_status;
	}

	public void setProduct_status(int product_status) {

		this.product_status = product_status;
	}

	public int getId_detail() {

		return id_detail;
	}

	public void setId_detail(int id_detail) {

		this.id_detail = id_detail;
	}

	public int getId_order() {

		return id_order;
	}

	public Integer getIcon_id() {

		return Icon_id;
	}

	@Override
	public String toString() {
		return "ParameterDetail [id_cart=" + id_cart + ", id_product="
				+ id_product + ", id_address_delivery=" + id_address_delivery
				+ ", id_shop=" + id_shop + ", id_product_attribute="
				+ id_product_attribute + ", quantity=" + quantity
				+ ", instructions=" + instructions + ", instructions_valid="
				+ instructions_valid + ", instructions_id=" + instructions_id
				+ ", foreground_color_id=" + foreground_color_id
				+ ", background_color_id=" + background_color_id + ", font_id="
				+ font_id + ", text_values=" + text_values + ", extra_values="
				+ extra_values + ", product_status=" + product_status
				+ ", id_detail=" + id_detail + ", id_order=" + id_order
				+ ", Icon_id=" + Icon_id + "]";
	}

}
