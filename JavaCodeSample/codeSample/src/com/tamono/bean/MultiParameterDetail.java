package com.tamono.bean;

import java.util.List;

import com.pegalinas.model.SimpleTextInput;

/*
 * Class to hols the property for multi type template.
 */
public class MultiParameterDetail {
	int idCart;
	List<Integer> labelRank;
	List<Integer> backgroundColorId;
	List<Integer> iconid;
	List<Integer> fontId;
	List<List<SimpleTextInput>> textList;
	int quantity;
	int idProduct;

	/*
	 * Setter & getter
	 */
	public List<Integer> getLabelRank() {
		return labelRank;
	}

	public void setLabelRank(List<Integer> labelRank) {
		this.labelRank = labelRank;
	}

	public List<Integer> getBackgroundColorId() {
		return backgroundColorId;
	}

	public void setBackgroundColorId(List<Integer> backgroundColorId) {
		this.backgroundColorId = backgroundColorId;
	}

	public List<Integer> getIconid() {
		return iconid;
	}

	public void setIconid(List<Integer> iconid) {
		this.iconid = iconid;
	}

	public List<Integer> getFontId() {
		return fontId;
	}

	public void setFontId(List<Integer> fontId) {
		this.fontId = fontId;
	}

	public List<List<SimpleTextInput>> getTextList() {
		return textList;
	}

	public void setTextList(List<List<SimpleTextInput>> textList) {
		this.textList = textList;
	}

	public int getIdCart() {
		return idCart;
	}

	public void setIdCart(int idCart) {
		this.idCart = idCart;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	@Override
	public String toString() {
		return "MultiParameterDetail [idCart=" + idCart + ", labelRank="
				+ labelRank + ", backgroundColorId=" + backgroundColorId
				+ ", iconid=" + iconid + ", fontId=" + fontId + ", textList="
				+ textList + ", quantity=" + quantity + ", idProduct="
				+ idProduct + "]";
	}

}
