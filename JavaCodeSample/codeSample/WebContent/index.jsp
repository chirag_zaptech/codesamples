<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>PDF Generator - Tamono.com</title>
	<script type="text/javascript">
		function load()
		{
			document.getElementById("indexId").submit();
		}
	</script>
	<style type="text/css">
		body {
			background: url(./img/login_bg.jpg) no-repeat top center; 
			color: white;
			margin-top: 60px;
		}
	</style>
	<link rel="shortcut icon" href="./img/doc_pdf.png"/>
</head>
<body onload="load()">
	<center>
		<form id="indexId"  action="pdfgen.do">
			<h1>PDF Generator - Tamono.com</h1><br>
			<h2>Please wait PDF generation in process...</h2>
			<input type="hidden" value="<%=request.getParameter("check")%>" name="detailId">
			<input type="hidden" value="<%=request.getParameter("name")%>" name="name">
		</form>
	</center>
</body>
</html>	