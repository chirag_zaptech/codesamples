<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>PDF Generator - Tamono.com</title>
	<style type="text/css">
		body {
			background: url(./img/login_bg.jpg) no-repeat top center; 
			color: white;
			margin-top: 60px;
		}
	</style>
</head>
<body>
	<% 
	String redirectPath="http://tamono.com/psadmin/pdfgen";
	if(request.getAttribute("MSG").equals("failed"))
	{
		response.sendRedirect(redirectPath+"/manage_orders.php?msg="+request.getAttribute("MSG"));
	}
	else if((request.getAttribute("MSG").equals("success"))) 
	{
		response.sendRedirect(redirectPath+"/manage_pdflog.php?msg="+request.getAttribute("MSG"));
	}
	else if(request.getAttribute("MSG").equals("renameFailed"))
	{
		response.sendRedirect(redirectPath+"/edit_pdflog.php?id="+request.getAttribute("ID")+"&msg="+request.getAttribute("MSG")); 
	}
	else
	{
		response.sendRedirect(redirectPath+"/pdf_function.php?id="+request.getAttribute("ID")+"&newfile="+request.getAttribute("FNAME"));
	} 
	%>
</body>
</html>