<?php
include('library/function.php');
if (isset($_POST['sUserEmail']) && isset($_POST['sPassword'])) {
    if (isset($_SESSION['nUserID']) && intval($_SESSION['nUserID']) > 0) {
        $_SESSION['success_msg'] = danishMessage("already_login");
        header("location:" . $base_url . "index.php");
        exit;
    }
    $login_data = array();
    if ($_POST['sUserEmail'] != '' && $_POST['sPassword'] != '') {
        $where = "sUserEmail = '" . $_POST['sUserEmail'] . "' AND sPassword = '" . md5($_POST['sPassword']) . "' ";
        $fields = 'bStatus, bLocked, nUserID, sFirstName, sLastName, bMembershiptype, dDateCreated, sArea, nuserType,bDeleteUser';
        $login_data = getAnyData($fields, 'users', $where, null, null);
    }
    $wrong_attempts = str_replace(".", "_", $_POST['sUserEmail'] . "_attempts");
    $time_limit = str_replace(".", "_", $_POST['sUserEmail'] . "_time_limit");
    if (!empty($login_data)) {
        if ($login_data[0]['bDeleteUser'] == "1") {
            $message = danishMessage("login_user_deleted");
            $msg_type = "error";
            if (isset($_COOKIE['rememberme'])) {
                $past = time() - 100;
                if (isset($_COOKIE['rememberme'])) {
                    setcookie('rememberme', '', $past);
                }
                if (isset($_COOKIE['sUserEmail'])) {
                    setcookie('sUserEmail', '', $past);
                }
                if (isset($_COOKIE['sPassword'])) {
                    setcookie('sPassword', '', $past);
                }
            }
            $return_data = array();
            $return_data['message'] = $message;
            $return_data['type'] = $msg_type;
            echo json_encode($return_data);
            exit;
        } elseif ($login_data[0]['bStatus'] == "0") {
            $message = danishMessage("login_inactive");
            $msg_type = "error";
            $return_data = array();
            $return_data['message'] = $message;
            $return_data['type'] = $msg_type;
            echo json_encode($return_data);
            exit;
        } elseif ($login_data[0]['bStatus'] == "2") {
            $message = danishMessage("login_block");
            $msg_type = "error";
            $return_data = array();
            $return_data['message'] = $message;
            $return_data['type'] = $msg_type;
            echo json_encode($return_data);
            exit;
        } else {
            if ($_POST['rememberme'] == "1" && !isset($_COOKIE['rememberme'])) {
                $month = time() + (60 * 60 * 24 * 30);
                setcookie('rememberme', $_POST['rememberme'], $month);
                setcookie('sUserEmail', $_POST['sUserEmail'], $month);
                setcookie('sPassword', $_POST['sPassword'], $month);
            } elseif (!isset($_COOKIE['rememberme']) || $_POST['rememberme'] == "") {
                $past = time() - 100;
                if (isset($_COOKIE['rememberme'])) {
                    setcookie('rememberme', '', $past);
                }
                if (isset($_COOKIE['sUserEmail'])) {
                    setcookie('sUserEmail', '', $past);
                }
                if (isset($_COOKIE['sPassword'])) {
                    setcookie('sPassword', '', $past);
                }
            }
            if ($login_data[0]['bLocked'] > time()) {  //If the time restriction is not over
                $message = danishMessage("login_locked");
                $msg_type = "error";
                $return_data = array();
                $return_data['message'] = $message;
                $return_data['type'] = $msg_type;
                echo json_encode($return_data);
                exit;
            }

            /* Remove cookie for wrong password less than 3 times */
            if (isset($_COOKIE[$wrong_attempts])) {
                setcookie($wrong_attempts, "", time() - 100);
            }

            /*  checking membership trial period */
            if ($login_data[0]['bMembershiptype'] == "1") {
                date_default_timezone_set('America/Denver');
                $datetime1 = date_create($login_data[0]['dDateCreated']);
                $datetime2 = date_create('now');
                $interval = date_diff($datetime1, $datetime2);
                $cur_days = $interval->format('%a');
                $remain_days = 30 - $cur_days;
                $message = danishMessage("login_suc_trial", $remain_days);
                $msg_type = "success";
                if ($remain_days <= 0) {
                    $condition = "nUserID = '" . $login_data[0]['nUserID'] . "'";
                    $data['bStatus'] = "1";
                    $suc = dbRowUpdate('users', $data, $condition);
                    if ($suc) {
                        $message = danishMessage("login_inactive");
                        $msg_type = "error";
                        $return_data = array();
                        $return_data['message'] = $message;
                        $return_data['type'] = $msg_type;
                        echo json_encode($return_data);
                        exit;
                    }
                }
            }

            if ($login_data[0]['nUserID'] != '') {
                $_SESSION['nUserID'] = $login_data[0]['nUserID'];
                $_SESSION['sFirstName'] = $login_data[0]['sFirstName'];
                $_SESSION['sLastName'] = $login_data[0]['sLastName'];
                $_SESSION['bMembershiptype'] = $login_data[0]['bMembershiptype'];
                $_SESSION['loggedUser_area'] = utf8_encode($login_data[0]['sArea']);
                $_SESSION['nuserType'] = $login_data[0]['nuserType'];
                $message = danishMessage("login_suc");
                $msg_type = "success";

                $return_data = array();
                $return_data['message'] = $message;
                $return_data['type'] = $msg_type;
                echo json_encode($return_data);
                exit;
            }
            /* End of checking membership trial period */
        } /* End of else block */
    } else {
        $message = danishMessage("login_not_member");
        $msg_type = "error";
        /* checking for wrong password in a row */
        if (isset($_COOKIE[$time_limit]) && isset($_COOKIE[$wrong_attempts])) {
            $num_tries = $_COOKIE[$wrong_attempts] + 1;
            $expire_time = $_COOKIE[$time_limit];
            setcookie($wrong_attempts, $num_tries, time() + 900);
        } else {
            $num_tries = 1;
            $expire_time = time() + 3600;
            setcookie($wrong_attempts, $num_tries, time() + 900);
            setcookie($time_limit, $expire_time, time() + 900);
        }
        if ($num_tries >= 3) {
            $data['bLocked'] = time() + 3600;
            $condition = "sUserEmail = '" . $_POST['sUserEmail'] . "'";
            $suc = dbRowUpdate('users', $data, $condition);
            if ($suc) {
                $message = danishMessage("login_wrong_psw");
                $msg_type = "error";
            }
        }
        /* End of checking for wrong password in a row */
        $return_data = array();
        $return_data['message'] = $message;
        $return_data['type'] = $msg_type;
        echo json_encode($return_data);
        exit;
    }
    $return_data = array();
    $return_data['message'] = $message;
    $return_data['type'] = $msg_type;
    echo json_encode($return_data);
    exit;
}
?>
