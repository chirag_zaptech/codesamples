<?php
include("library/function.php");
error_reporting(0);
$data = "";
if (isset($_POST['nGroupID']) && intval($_POST['nGroupID']) > 0) {
    $_SESSION['current_nGroupID'] = $_POST['nGroupID'];
    $fields = $join = $where = $having = '';
    $from = ' FROM groups as g ';
    $fields = "g.*,gm.*";
    $join = " left join group_messages as gm on g.nGroupID = gm.nGroupID";
    $where = " where g.nGroupID =" . $_POST['nGroupID'];
    $sql = 'SELECT ' . $fields . $from . $join . $where;

    $conversation = fetchQueryRes($sql);
    if (!empty($conversation)) {
        /* Getting members data in the conversation */
        $userdata = getAnyData('nUserID,sFirstName,sLastName,sPartnerLogo', 'users', 'nUserID IN(' . $conversation[0]['sMembers'] . ') group by nUserID', null, null);
        $nCreatedByID = $conversation[0]['nCreatedByID'];
        $bGroupType = $conversation[0]['bGroupType'];
        if (!empty($userdata)) {
            for ($i = 0; $i < count($userdata); $i++) {
                $userData[$userdata[$i]['nUserID']] = $userdata[$i];
            }
        }
        $message_data = json_decode($conversation[0]['sMessage'], true);

        if (!empty($message_data)) {
            foreach ($message_data as $time => $msg) {
                $msg_date = date("Y-m-d", $time);
                $message[$msg_date][$time] = $msg;
            }
            /* Getting the user stasus of the conversation */
            $gms = getAnyData('*', 'group_messages_status', 'nMessageID = "' . $conversation[0]['nMessageID'] . '"', null, null);
            for ($i = 0; $i < count($gms); $i++) {
                $group_msg_status[$gms[$i]['nUserID']] = $gms[$i];
            }
            $i = 0;
            $data .= '<div class="m_chat_container"><div id="chat_container"><div>';
            foreach ($message as $key => $val) {
                if ($i == 0) {
                    $start_data = dateDanishFormat($key, '');
                    $data .= '<div class="m_old_date_header"> Samtale begyndt <br> ' . $start_data . ' </div>
                        <div class="message_users_wrapper">';
                }else{
                    $date = dateDanishFormat($key, '');
                    $data .= '<div class="m_new_date_header"><span> ' . $date . ' </span></div>';
                }
                foreach ($val as $timestamp => $conv) {
                    foreach ($conv as $k => $v) {
                        $userId = $k;
                        $msg = base64_decode($v[0]);
                        $left = $v[1];

                        $profile_img_url = $base_url . "images/cs_images/ol-no-img.jpeg";

                        if (is_file($user_profile_images_path . $userData[$userId]['sPartnerLogo'])) {
                            $profile_img_url = $user_profile_images_url . $userData[$userId]['sPartnerLogo'];
                        }
                        $user_left = false;
                        if ($group_msg_status[$_SESSION['nUserID']]['bLeft'] == "1" || $group_msg_status[$_SESSION['nUserID']]['bDelete'] == "1") {
                            /* If the loggedin user left/delete from this conversation then the conversation will be show till left/deleted date for the logedin user only */
                            $dtDateLeft = strtotime($group_msg_status[$_SESSION['nUserID']]['dtModifiedDate']);
                            $msg_date = strtotime(date("Y-m-d H:i:s", $timestamp));
                            if ($msg_date <= $dtDateLeft) {
                                $user_left = true;
                                if ($msg != "") {
                                    $data .= '<div class="m_single_users_wrapper"> 
                                <div class="m_chat_users_img"> <a href="' . $base_url . 'profile.php?id=' . $userData[$userId]['nUserID'] . '"><img alt="" src="' . $profile_img_url . '"></a> </div>
                                <div class="m_chat_users_message">
                                    <span class="top"> <a href="' . $base_url . 'profile.php?id=' . $userData[$userId]['nUserID'] . '">' . $userData[$userId]['sFirstName'] . " " . $userData[$userId]['sLastName'] . '</a> </span>
                                    <span class="bottom"> ' . $msg . ' </span>
                                </div>
                                </div>';
                                }

                                if ($left == "1" || $msg == "") {
                                    $data .= '<div class="m_leave_message">
                                             <span class="middle"> 
                                             <span class="left"> ' . $userData[$userId]['sFirstName'] . " " . $userData[$userId]['sLastName'] . ' </span>
                                             <span class="right"> har forladt samtalen </span>
                                             </span>
                                            </div>';
                                    $user_left = true;
                                }
                            }
                        } elseif ($group_msg_status[$nCreatedByID]['bLeft'] == "1" || $group_msg_status[$nCreatedByID]['bDelete'] == "1") {
                            /* If the group created user left/delete then the conversation will be showing till left/deleted date for all the members */
                            $dtDateLeft = strtotime($group_msg_status[$nCreatedByID]['dtModifiedDate']);
                            $msg_date = strtotime(date("Y-m-d H:i:s", $timestamp));
                            if ($msg_date <= $dtDateLeft) {
                                $user_left = true;
                                if ($msg != "") {
                                    $data .= '<div class="m_single_users_wrapper"> 
                                <div class="m_chat_users_img"> <a href="' . $base_url . 'profile.php?id=' . $userData[$userId]['nUserID'] . '"><img alt="" src="' . $profile_img_url . '"></a> </div>
                                <div class="m_chat_users_message">
                                    <span class="top"> <a href="' . $base_url . 'profile.php?id=' . $userData[$userId]['nUserID'] . '">' . $userData[$userId]['sFirstName'] . " " . $userData[$userId]['sLastName'] . '</a> </span>
                                    <span class="bottom"> ' . $msg . ' </span>
                                </div>
                                </div>';
                                }
                                if ($left == "1" || $msg == "") {
                                    $data .= '<div class="m_leave_message">
                                             <span class="middle"> 
                                             <span class="left"> ' . $userData[$userId]['sFirstName'] . " " . $userData[$userId]['sLastName'] . ' </span>
                                             <span class="right"> har forladt samtalen </span>
                                             </span>
                                            </div>';
                                    $user_left = true;
                                }
                            }
                        } else {
                            /* This conversation will be shown normally to all the conversation */
                            if ($msg != "") {
                                $data .= '<div class="m_single_users_wrapper"> 
                                <div class="m_chat_users_img"> <a href="' . $base_url . 'profile.php?id=' . $userData[$userId]['nUserID'] . '"><img alt="" src="' . $profile_img_url . '"></a> </div>
                                <div class="m_chat_users_message">
                                    <span class="top"> <a href="' . $base_url . 'profile.php?id=' . $userData[$userId]['nUserID'] . '">' . $userData[$userId]['sFirstName'] . " " . $userData[$userId]['sLastName'] . '</a> </span>
                                    <span class="bottom"> ' . $msg . ' </span>
                                </div>
                                </div>';
                            }
                            if ($left == "1" || $msg == "") {
                                $data .= '<div class="m_leave_message">
                                             <span class="middle"> 
                                             <span class="left"> ' . $userData[$userId]['sFirstName'] . " " . $userData[$userId]['sLastName'] . ' </span>
                                             <span class="right"> har forladt samtalen </span>
                                             </span>
                                            </div>';
                                $user_left = true;
                            }
                        }

                        /* To show the message textarea box for sending message */
                        if ($group_msg_status[$_SESSION['nUserID']]['bLeft'] == "1" || $group_msg_status[$_SESSION['nUserID']]['bDelete'] == "1") {
                            $user_left = true;
                        } elseif ($group_msg_status[$nCreatedByID]['bLeft'] == "1" || $group_msg_status[$nCreatedByID]['bDelete'] == "1") {
                            $user_left = true;
                        } else {
                            $user_left = false;
                        }
                    }
                }
                $date = dateDanishFormat($key, '');
                $i++;
                $data .= '</div>';
                
            }
            $data .= '</div>';
            if (!$user_left) {
                /* This will show if the user not left/delete from the conversation */
                $data .= '<div class="m_chat_room">
                    <span class="top"><textarea cols="50" id="message_box" placeholder="svar .." rows="4"></textarea></span>
                    <span class="bottom"><button type="button" id="btn_send_message">BESVAR</button></span>
                </div>';
            }
            $data .= '</div>';
        } else {
            /* if no conversation found  */
            $data .= '<div class="m_chat_container">';
            $data .= '<span>No Conversation Found.</span>';
            $data .= '<div class="m_chat_room">
                    <span class="top"><textarea cols="50" id="message_box" placeholder="svar .." rows="4"></textarea></span>
                    <span class="bottom"><button type="button" id="btn_send_message">BESVAR</button></span>
                </div>';
            $data .= '</div>';
        }
    }
} else {
    $data = "No Conversation Found";
}

echo utf8_encode($data);
exit;
?>