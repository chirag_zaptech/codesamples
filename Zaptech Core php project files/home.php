<?php   include('header-inner.php');

	/* Latest 3 Events are fetched - Start*/
	$fieldEvents = 'sEventTitle, sEventImage, dtAdded';
	$eventsData = getAnyData($fieldEvents, 'admin_events', '', null, 'ORDER BY nEventId DESC Limit 3');
	/* Latest 3 Events are fetched - End*/

	/* News Content fetched - Start*/
	$news = getAnyData('sFieldContent', 'settings', 'nSettingsID=8', null, null);
	$where = "nUserID = '".$_SESSION['nUserID']."'";
	/* News Content are fetched - End*/
	
	$sArea = getAnyData('sArea,bGender', 'users', $where, null, null);
	
	/* 8 User Profiles are fetched for User listing of same Area - start*/
	$from = ' users as u left join user_favorites as uf on uf.nUserID = u.nUserID AND uf.nUserID = "'.$_SESSION['nUserID'].'" ';
	$fieldUser = ' u.nUserID, u.sFirstName, u.sLastName, u.dBirthDate, u.sArea, u.sPartnerLogo, uf.nFavoriteUserID';
	$whereUser = 'bDeleteUser = 0 AND nuserType = 1 AND bStatus = 1 AND sArea = "'.$sArea[0]['sArea'].'" && u.nUserID <> "'.$_SESSION['nUserID'].'" ';
	$userData = getAnyData($fieldUser, $from, $whereUser, '0,8', null);
	/* 8 User Profiles are fetched for User listing of same Area - End*/
	
	if(isset($sArea[0]['bGender']) && $sArea[0]['bGender'] == '1'){
		/* 8 User Profiles are fetched for User listing of same Area - start*/
		$fieldUser = 'nUserID, sFirstName, sLastName, dBirthDate, sArea, sPartnerLogo';
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 2 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$userData = getAnyData($fieldUser, 'users', $where, '0,8', null);
		/* 8 User Profiles are fetched for User listing of same Area - End*/
		/* Counting User Profiles of same Area - Start*/
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 2 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$profileCount = getAnyData('count(*) as total', 'users', $where, null, null);
		/* Counting User Profiles of same Area - End*/
		
	}elseif(isset($sArea[0]['bGender']) && $sArea[0]['bGender'] == '2'){
		/* 8 User Profiles are fetched for User listing of same Area - start*/
		$fieldUser = 'nUserID, sFirstName, sLastName, dBirthDate, sArea, sPartnerLogo';
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 1 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$userData = getAnyData($fieldUser, 'users', $where, '0,8', null);
		/* 8 User Profiles are fetched for User listing of same Area - End*/
		/* Counting User Profiles of same Area - Start*/
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 1 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$profileCount = getAnyData('count(*) as total', 'users', $where, null, null);
		/* Counting User Profiles of same Area - End*/
	}else{
		/* 8 User Profiles are fetched for User listing of same Area - start*/
		$fieldUser = 'nUserID, sFirstName, sLastName, dBirthDate, sArea, sPartnerLogo';
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && nUserID <> "'.$_SESSION['nUserID'].'"';
		$userData = getAnyData($fieldUser, 'users', $where, '0,8', null);
		/* 8 User Profiles are fetched for User listing of same Area - End*/
		/* Counting User Profiles of same Area - Start*/
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && nUserID <> "'.$_SESSION['nUserID'].'"';
		$profileCount = getAnyData('count(*) as total', 'users', $where, null, null);
		/* Counting User Profiles of same Area - End*/
	}
	
	if(!empty($profileCount) && isset($profileCount[0]['total']) && $profileCount[0]['total'] <= '80'){
		$totalpages = ceil(($profileCount[0]['total'])/8);
	}else{
		$totalpages = 10;
	}
?>
<!--middle bar part div start-->
	<section class="home_content_top">
		<div class="width_container">
			<div class="news_block">
				<span class="news_block_left">
					<span class="news_header">NYHEDER</span>
					<p class="news_content">
						<?php if(isset($news[0]['sFieldContent'])){
							$text = strip_tags($news[0]['sFieldContent']);
							echo limit_words($text, 100);
						}?>
					</p>
				</span>
				<!-- Home Page Slider Start -->
				<span class="news_block_right">
					<div class="whole_showcase_wrapper">
						<div id="showcase" class="showcase">
							<?php 
							$fieldSlider = 'sSliderURL, sSliderImage, sSliderCaption, sSliderImageTitle';
							$sliderData = getAnyData($fieldSlider, 'home_slider', 'bStatus = 1', null, null);
							for($i = 0; $i < count($sliderData); $i++){ ?>
							<div class="showcase-slide">
								<!-- Put the slide content in a div with the class .showcase-content. -->
								<div class="showcase-content">
									<a class="image_wrapper" href="<?php echo $base_url.$sliderData[$i]['sSliderURL'];?>"><img src="<?php echo $home_slider_upload_url.$sliderData[$i]['sSliderImage'];;?>" alt="03" /></a>
								</div>
								<!-- Put the caption content in a div with the class .showcase-caption -->
								<div class="showcase-caption">
									<?php if(isset($sliderData[$i]['sSliderCaption'])){?>
									<a href="<?php echo $base_url.$sliderData[$i]['sSliderURL']?>">
										<?php echo $sliderData[$i]['sSliderCaption'];?>
									</a>
									<?php }?>
								</div>
							</div>
							<?php }?>
							<div style="clear: both;" class="showcase-button-wrapper">
							<?php for($j = 0; $j < count($sliderData); $j++){?>
							<span id="showcase-navigation-button-<?php echo $j;?>" <?php if(!$j){ echo 'class="active"';}?> onclick="location.href='<?php echo $base_url.$sliderData[$j]['sSliderURL'];?>'" >
								<?php echo $sliderData[$j]['sSliderImageTitle'];?>
							</span>
							<?php }?>
							</div>
						</div>
					</div>
				</span>
				<!-- Home Page Slider End -->
			</div>
			<!-- Latest Three Events Image Start -->
			<div class="Calendar_events_block">
			<?php for($i = 0; $i < 3; $i++){
				if($i == 0){$class = 'Calendar_block_left';}
				elseif($i == 1){$class = 'Calendar_block_center';}
				elseif($i == 2){$class = 'Calendar_block_right';} ?>
					<span class="<?php echo $class;?>">
						<span class="Calendar_block_header"><?php echo $eventsData[$i]['sEventTitle'];?> - <?php echo dateDanishFormat($eventsData[$i]['dtAdded'], '');?></span>
						<?php if(file_exists($event_path.$eventsData[$i]['sEventImage']) && $eventsData[$i]['sEventImage'] != ''){?>
							<img width="100%" height="" src="<?php echo $event_url.$eventsData[$i]['sEventImage'];?>" alt="">
						<?php }else{?>
							<img width="100%" src="<?php echo $base_url;?>images/3_no_image.png" >
						<?php }?>
					</span>
			<?php }?>
			</div>
			<!-- Latest Three Events Image End -->
		</div>
	</section>
	
	<!-- Load Profiles Modules Start -->
	<section class="home_content">
		<div class="width_container">
			<div class="profiles_wrapper">
			<input type="hidden" id="totalpages" name="totalpages" value="<?php echo $totalpages; ?>" />
			<input type="hidden" id="scrollpage" name="scrollpage" value="1" />
			<input type="hidden" id="user_area" name="user_area" value="<?php if(isset($sArea[0]['sArea'])){ echo $sArea[0]['sArea']; }else{ echo 'all'; } ?>" />
			<input type="hidden" id="user_gender" name="user_gender" value="<?php if(isset($sArea[0]['bGender'])){ echo $sArea[0]['bGender']; }else{ echo '0'; } ?>" />

			<?php for($i = 0,$j=1; $i < count($userData); $i++,$j++){ ?>
				<?php if($i == 0){  ?>
					<div id="" class="profile_container">
				<?php }elseif(($i%4) == 0){ ?>
					</div>
					<div id="" class="profile_container">
				<?php } ?>

				<?php if(($j%4) == 0){  ?>
						<div id="<?php echo 'profile_container_wrapper'.($i + 1);?>" class="profile_container_wrapper last">
				<?php }else{ ?>
						<div id="<?php echo 'profile_container_wrapper'.($i + 1);?>" class="profile_container_wrapper">
				<?php } ?>
					<span class="pro_span">
						<?php 
							$where = 'nUserID = "'.$_SESSION['nUserID'].'" && nFavoriteUserID = "'.$userData[$i]['nUserID'].'"';
							$checkFav = getAnyData('nFavoriteUserID', 'user_favorites', $where, null, null);
							$class="";
                                                        if(!empty($checkFav)){ $class = "greenStar"; $text_fav = "Fjern fra favoritliste"; }else{$text_fav = "Tilf&Oslash;j favoritliste";}
						?>
						<?php if(isset($_SESSION['nuserType']) && $_SESSION['nuserType'] != '2'){ ?>
						<a href="javascript:void(0);" onclick="addFavorite('<?php echo $userData[$i]['nUserID'];?>')" id="anchor_fav_<?php echo $userData[$i]['nUserID'];?>" class="anchor_fav <?php echo $class;?>" ><?php echo $text_fav; ?></a>
						<?php } ?>
						<a class="pro_img" href="javascript:void(0);" onclick="movetoprofile('<?php echo $userData[$i]['nUserID'];?>');">
						<?php if($userData[$i]['sPartnerLogo'] != '' ){?>
							<img src="<?php echo $user_profile_images_url.$userData[$i]['sPartnerLogo'];?>" alt="">
						<?php }else{?>
							<img src="<?php echo $base_url;?>images/4_no_image.png" alt="">
						<?php }?>
						</a>
						<span class="pro_desc_wrapper" onclick="movetoprofile('<?php echo $userData[$i]['nUserID'];?>');"> 
							<p class="pro_name"><?php echo $userData[$i]['sFirstName'] .' '. $userData[$i]['sLastName'];?></p>
							<p class="pro_age"><?php echo get_age($userData[$i]['dBirthDate']);?> år - <?php echo $userData[$i]['sArea'];?></p>
						</span>
					</span>
				</div>
			<?php } ?>
			</div> <!--profile container div end-->
			
			</div>
			<div class="pro_go_on" style="margin-top: 30px;">
				<span class="go_on_arrow" id="ajaxloader" style="display: none;"><img src="<?php echo $base_url;?>images/ajaxloader.gif" alt=""></span>
				<span class="go_on_button" id="searchon" style="display: none;"><a href="<?php echo $base_url; ?>search.php" class="go_on">SØG FLERE PROFILER</a></span>
			</div>
		</div>
	</section>
	<!-- Load Profiles Modules End -->
	
<?php include('footer-inner.php');?>
<script type="text/javascript">
	
/*** User profile click will redirect to specific profile page ***/
function movetoprofile(userid){
	location.href = "profile.php?id="+userid;
}

/*** User added to favorite with ajax call start ***/
function addFavorite(id){
	var base_url = '<?php echo $base_url;?>';
	jQuery.ajax({
		method: "POST",
		data: "id="+id,
		url: base_url+"addFavorite.php",
		success:function(msg){
			if(msg){
				$('#anchor_fav_'+id).addClass('greenStar');
				$('#anchor_fav_'+id).html('Fjern fra favoritliste');
			}else{
				$('#anchor_fav_'+id).removeClass('greenStar');
				$('#anchor_fav_'+id).html('Tilf&Oslash;j favoritliste');
			}
		}
	});
}
/*** User added to favorite with ajax call end ***/

/*** On scroll paging for users profile listing through ajax call section start ***/
var processing;
$(document).ready(function(){
    $(document).scroll(function(e){
        if (processing){
            return false;
        }
        if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.6){
        $('#ajaxloader').show();
            processing = true;
	    var totalpages = $('#totalpages').val();
	    var scrollpage = $('#scrollpage').val();
	    var user_area = $('#user_area').val();
	    var user_gender = $('#user_gender').val();
	    var profilecount = $('#profilecount').val();
	    if (scrollpage < totalpages) {
		var base_url = '<?php echo $base_url;?>'; 
		$.post(base_url+"profilePaging.php", { page: "home", scrollpage: scrollpage, user_area: user_area, user_gender: user_gender, profilecount: profilecount }, function(data){
			$('#ajaxloader').hide();
			data = JSON.parse(data);
			$('#scrollpage').val(data.page);
			$('.profiles_wrapper').append(data.usrprofiles);
			loadcontentdesign();
			//load the content to your div
			processing = false; //resets the ajax flag once the callback concludes
		});
	    }else{
		$('#ajaxloader').hide();
		$('#searchon').show();
	    }
        }
    });
});
/*** On scroll paging for users profile listing through ajax call section end ***/
</script>