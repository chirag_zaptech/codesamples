<?php include('header-inner.php');
/*** search parameters session management for related result listing with paging display section start ***/
$sArea = '';
if(isset($_SESSION['searchflag']) && $_SESSION['searchflag'] == '1'){
    $fields = $join = $where = $having = '';
    $where .= ' where u.bDeleteUser = 0 AND u.nuserType = 1 AND u.bStatus = 1 AND u.nUserID <> "'.$_SESSION['nUserID'].'"';
    $fields = ' u.*, FLOOR(DATEDIFF (NOW(), dBirthDate)/365) AS uAge  ';
    if(isset($_SESSION['ser_agemin']) && $_SESSION['ser_agemin'] != ''){
        $having .= " HAVING uAge >=  ".$_SESSION['ser_agemin'];
    }
    if(isset($_SESSION['ser_agemax']) && $_SESSION['ser_agemax'] != ''){
        if(isset($_SESSION['ser_agemin']) && $_SESSION['ser_agemin'] != ''){
            $having .= " AND uAge <=  ".$_SESSION['ser_agemax'];
        }else{
            $having .= " HAVING uAge <=  ".$_SESSION['ser_agemax'];    
        }
    }
    if(isset($_SESSION['ser_gender']) && $_SESSION['ser_gender'] != 'Alle'){
        if($_SESSION['ser_gender'] == 'Man'){
            $where .= " AND bGender =  1";
        }elseif($_SESSION['ser_gender'] == 'Kvinder'){
            $where .= " AND bGender =  2";
        }
    }
    if(isset($_SESSION['ser_area']) && $_SESSION['ser_area'] != 'Alle'){
        $where .= " AND sArea =  '".$_SESSION['ser_area']."'";
    }
    if(isset($_SESSION['ser_postcodefrom']) && $_SESSION['ser_postcodefrom'] != ''){
        $where .= " AND nPostCode >=  ".$_SESSION['ser_postcodefrom'];
    }
    if(isset($_SESSION['ser_postcodeto']) && $_SESSION['ser_postcodeto'] != ''){
        $where .= " AND nPostCode <=  ".$_SESSION['ser_postcodeto'];
    }
    if(isset($_SESSION['ser_relstatus']) && $_SESSION['ser_relstatus'] != 'Alle'){
        $where .= " AND sRelStatus =  '".$_SESSION['ser_relstatus']."'";
    }
    if(isset($_SESSION['ser_fname']) && $_SESSION['ser_fname'] != ''){
        $where .= " AND sFirstName =  '".$_SESSION['ser_fname']."'";
    }
    if(isset($_SESSION['ser_lname']) && $_SESSION['ser_lname'] != ''){
        $where .= " AND sLastName =  '".$_SESSION['ser_lname']."'";
    }
    if(isset($_SESSION['ser_favorite']) && $_SESSION['ser_favorite'] != ''){
        $join = " left join user_favorites as uf ON uf.nFavoriteUserID = u.nUserID ";
	$where .= " AND uf.nUserID = ".$_SESSION['nUserID'];
    }
    $from = ' FROM users as u ';
    
    $sql = 'SELECT '.$fields.$from.$join.$where;
    $sql_total = 'SELECT '.$fields.$from.$join.$where;
    
    if($having!= ''){
        $sql .= $having;
	$sql_total .= $having;
    }
    $limit = ' Limit 0,8';
    $sql .= $limit;
    $userData = fetchQueryRes($sql);
    $profileCount = fetchQueryRes($sql_total);
    $totalProfileCount = count($profileCount);
    $totalpages = ceil($totalProfileCount/8);
/*** search parameters session management for related result listing with paging display section end ***/
}else{
/*** First time search parameters management for related result listing with paging display section start ***/
	$where = "nUserID = '".$_SESSION['nUserID']."'";
	$sArea = getAnyData('sArea,bGender', 'users', $where, null, null);
	if(isset($sArea[0]['bGender']) && $sArea[0]['bGender'] == '1'){
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 2 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$userData = getAnyData('*', 'users', $where, '0,8', null);
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 2 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$profileCount = getAnyData('count(*) as total', 'users', $where, null, null);
	}elseif(isset($sArea[0]['bGender']) && $sArea[0]['bGender'] == '2'){
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 1 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$userData = getAnyData('*', 'users', $where, '0,8', null);
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && bGender = 1 && nUserID <> "'.$_SESSION['nUserID'].'"';
		$profileCount = getAnyData('count(*) as total', 'users', $where, null, null);
	}else{
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && nUserID <> "'.$_SESSION['nUserID'].'" ';
		$userData = getAnyData('*', 'users', $where, '0,8', null);
		$where = 'bDeleteUser = 0 && nuserType = 1 && bStatus = 1 && sArea = "'.$sArea[0]['sArea'].'" && nUserID <> "'.$_SESSION['nUserID'].'" ';
		$profileCount = getAnyData('count(*) as total', 'users', $where, null, null);
	}
	if(!empty($profileCount) && isset($profileCount[0]['total'])){
		$totalpages = ceil(($profileCount[0]['total'])/8);
	}
/*** First time search parameters management for related result listing with paging display section end ***/
}
?>

<!--middle bar part div start-->
<form name="searchfrm" id="searchfrm" method="post" action="" >
	<section class="search_content_top">
		<div class="width_container">
			<div class="search_block_top">
				<span class="search_span_top age">
				    <span class="search_title"> <?php echo Age; ?> </span>
				    <span class="select_min_age_wrapper span_select">
					<?	
						if(isset($_SESSION['ser_agemin']) && $_SESSION['ser_agemin'] != ''){
							$mintext = $minval =  $_SESSION['ser_agemin'];
						}else{
							$minval = '';
							$mintext = 'Min';
						}
					?>
					<input class="select_age select_hidden" type="text" id="agemin" name="agemin" value="<?php echo $minval; ?>">
					<span style="pointer-events: none;" id="agemin_lbl" class="selected_wrapper"><?php echo $mintext; ?></span>
					<div class="select_menu_wrapper">
					    <ul class="select_menu">
						    <?php for($i=15;$i<=100;$i++){
							    echo '<li class="select_items" value="'.$i.'" >'.$i.'</li>';
						    } ?>
					    </ul>
					</div>
				    </span>
				    <span class="select_max_age_wrapper span_select">
					<?php
						if(isset($_SESSION['ser_agemax']) && $_SESSION['ser_agemax'] != ''){
							$maxtext = $maxval =  $_SESSION['ser_agemax'];
						}else{
							$maxval = '';
							$maxtext = 'Max';
						}
					?>
					<input class="select_age select_hidden" type="text" id="agemax" name="agemax" value="<?php echo $maxval; ?>">
					<span style="pointer-events: none;" id="agemax_lbl" class="selected_wrapper"><?php echo $maxtext; ?></span>
					<div class="select_menu_wrapper">
					    <ul class="select_menu">
						    <?php for($i=15;$i<=100;$i++){ echo '<li class="select_items"  value="'.$i.'" >'.$i.'</li>'; } ?>
					    </ul>
					</div>
				    </span>
				</span>
				<span class="search_span_top sex">
					<span class="search_title"> <?php echo utf8_encode(Sex); ?>  </span>
					<span class="select_sex_wrapper span_select">
						<?	
							if(isset($_SESSION['ser_gender']) && $_SESSION['ser_gender'] != ''){
								$gentext = $genval =  $_SESSION['ser_gender'];
							}else{
								if(isset($sArea[0]['bGender']) && $sArea[0]['bGender'] == '1' ){
									$gentext = $genval = Women;
								}elseif(isset($sArea[0]['bGender']) && $sArea[0]['bGender'] == '2' ){
									$gentext = $genval = 'Man';
								}else{
									$gentext = $genval = 'Alle';	
								}
							}
						?>
						<input class="select_sex select_hidden" type="text" name="gender" id="gender" value="<?php echo $genval; ?>">
						<span style="pointer-events: none;" id="gender_lbl" class="selected_wrapper"><?php echo $gentext; ?></span>
						<div class="select_menu_wrapper">
							<ul class="select_menu">
							    <li class="select_items"><?php echo 'Alle'; ?></li>
							    <li class="select_items"><?php echo 'Man'; ?></li>
							    <li class="select_items"><?php echo Women; ?></li>
							</ul>
						</div>
					</span>
				</span>
				<span class="search_span_top area">
					<span class="search_title"><?php echo utf8_encode(Area); ?> </span>
					<span class="select_area_wrapper span_select">
						<?	
							if(isset($_SESSION['ser_area']) && $_SESSION['ser_area'] != ''){
								$areatext = $areaval =  $_SESSION['ser_area'];
							}else{
                                                                $areatext = $areaval = 'Alle';
							}
						?>
						<input class="select_area select_hidden" type="text" name="area" id="area" value="<?php echo $areaval; ?>">
						<span style="pointer-events: none;" id="area_lbl" class="selected_wrapper"><?php echo $areatext; ?></span>
						<div class="select_menu_wrapper">
							<ul class="select_menu">
							    <li class="select_items">Alle</li>
							    <li class="select_items">Jylland</li>
							    <li class="select_items">Fyn</li>
							    <li class="select_items">Sjælland</li>
							    <li class="select_items">Bornholm</li>
							</ul>
						</div>
					</span>
				</span>
				<span class="search_span_top post">
					<span class="search_title"> <?php echo utf8_encode(Postcode); ?> </span>
					<span class="select_post_from_wrapper span_select"> 
						<label for="from">FRA :</label>
						<?	$postcodefromval = '';
							if(isset($_SESSION['ser_postcodefrom']) && $_SESSION['ser_postcodefrom'] != ''){
								$postcodefromval =  $_SESSION['ser_postcodefrom'];
							}
						?>
						<input class="select_post_from select_visible validate[custom[integer],minSize[4],maxSize[4]]" type="text" name="postcodefrom" id="postcodefrom" value="<?php echo $postcodefromval; ?>">
					</span>
					<span class="select_post_to_wrapper span_select"> 
						<label for="to">TIL :</label>
						<?	$postcodetoval = '';
							if(isset($_SESSION['ser_postcodeto']) && $_SESSION['ser_postcodeto'] != ''){
								$postcodetoval =  $_SESSION['ser_postcodeto'];
							}
						?>
						<input class="select_post_to select_visible validate[custom[integer],minSize[4],maxSize[4]]" type="text" name="postcodeto" id="postcodeto" value="<?php echo $postcodetoval; ?>">				
					</span>
				</span>
				<span class="search_span_top status">
					<span class="search_title"><?php echo Status; ?></span>
					<span class="select_status_wrapper span_select">
					    <?	
						if(isset($_SESSION['ser_relstatus']) && $_SESSION['ser_relstatus'] != ''){
							$relstatustext = $relstatusval =  $_SESSION['ser_relstatus'];
							
						}else{
							$relstatusval = 'Alle';
							$relstatustext = 'Alle';
						}
					    ?>
					    <input class="select_status select_hidden" type="text" name="relstatus" id="relstatus" value="<?php echo $relstatusval; ?>">
					    <span style="pointer-events: none;" id="relstatus_lbl" class="selected_wrapper"><?php echo $relstatustext; ?></span>
					    <div class="select_menu_wrapper">
						<ul class="select_menu">
						    <li class="select_items">Alle</li>
						    <li class="select_items"><?php echo 'Single'; ?></li>
						    <li class="select_items"><?php echo Married; ?></li>
						    <li class="select_items"><?php echo In_a_relationship; ?></li>
						    <li class="select_items"><?php echo Friends; ?></li>
						</ul>
					    </div>
					</span>
				</span>
			</div> <!--*** search section parameters block div end with class search_block_top ***-->
			<div class="search_block_bottom">
			    <span class="search_span_bottom_left">
				    <span class="search_span_bottom fname">
					    <span class="search_title"><?php echo Firstname; ?></span>
					    <span class="select_fname_wrapper span_select">
						    <?	$fnameval = '';
							    if(isset($_SESSION['ser_fname']) && $_SESSION['ser_fname'] != ''){
								    $fnameval =  addslashes(strip_tags($_SESSION['ser_fname']));
							    }
						    ?>
						    <input class="select_fname select_visible" type="text" value="<?php echo $fnameval; ?>" id="fname" name="fname">
					    </span>
				    </span>
				    <span class="search_span_bottom lname">
					    <span class="search_title"><?php echo Lastname; ?></span>
					    <span class="select_lname_wrapper span_select">
						    <?	$lnameval = '';
							    if(isset($_SESSION['ser_lname']) && $_SESSION['ser_lname'] != ''){
								    $lnameval =  addslashes(strip_tags($_SESSION['ser_lname']));
							    }
						    ?>
						    <input class="select_lname select_visible" type="text" value="<?php echo $lnameval; ?>" id="lname" name="lname">
					    </span>
				    </span>
			    </span>
			    <span class="search_span_bottom_right">
				    <span class="search_span_bottom left">
					    <?	$favoriteval = '';
						    if(isset($_SESSION['ser_favorite']) && $_SESSION['ser_favorite'] == '1'){
							    $favoriteval =  'checked="checked"';
						    }
					    ?>
					    <input id="favorite" name="favorite" class="css-checkbox lrg" type="checkbox" <?php echo $favoriteval; ?> >
					    <label class="css-label lrg vlad label_star" name="checkbox1_lbl" for="favorite"><a href="#"><?php echo Show_Favorites; ?></a></label>
				    </span>
				    <span class="search_span_bottom center">
					    <a href="#" onclick="clearsearch();"><?php echo Clear_All; ?></a>
				    </span>
				    <span class="search_span_bottom right">
					    <img src="<?php echo $base_url;?>images/processing.gif" id="processing" style="display: none;" alt="">
					    <input type="button" name="submit" onclick="searchProfiles();" value="<?php echo utf8_encode(Save_Search); ?>">
				    </span>
			    </span>
			</div> <!--*** End of search_block_bottom class div ***-->
		</div>
	</section>
</form> <!--*** Search parameters section form block end ***-->
<!--middle bar part div End-->

<!-- Load Profiles Modules Start -->
	<section class="home_content">
		<input type="hidden" id="totalpages" name="totalpages" value="<?php echo $totalpages; ?>" />
		<input type="hidden" id="scrollpage" name="scrollpage" value="1" />
		<input type="hidden" id="user_area" name="user_area" value="<?php if(isset($sArea[0]['sArea'])){ echo $sArea[0]['sArea']; }else{ echo 'all'; } ?>" />
		<input type="hidden" id="baseurl" name="baseurl" value="<?php echo $base_url;?>" />
		<div class="width_container">
			<div class="profiles_wrapper">
			<?php if(!empty($userData)){ for($i = 0,$j=1; $i < count($userData); $i++,$j++){ ?>
				<?php if($i == 0){  ?>
					<div id="" class="profile_container">
				<?php }elseif(($i%4) == 0){ ?>
					</div>
					<div id="" class="profile_container">
				<?php } ?>
				
				<?php if(($j%4) == 0){  ?>
						<div id="<?php echo 'profile_container_wrapper'.($i + 1);?>" class="profile_container_wrapper last">
				<?php }else{ ?>
						<div id="<?php echo 'profile_container_wrapper'.($i + 1);?>" class="profile_container_wrapper">
				<?php } ?>
				
					<span class="pro_span">
						<?php 
							$where = 'nUserID = "'.$_SESSION['nUserID'].'" && nFavoriteUserID = "'.$userData[$i]['nUserID'].'"';
							$checkFav = getAnyData('nFavoriteUserID', 'user_favorites', $where, null, null);
							$class="";
                                                        if(!empty($checkFav)){ $class = "greenStar"; $text = "Fjern fra favoritliste"; }else{ $text = "TilfØj favoritliste";}
						?>
						
						<?php if(isset($_SESSION['nuserType']) && $_SESSION['nuserType'] != '2'){ ?>
						<a href="javascript:void(0);" onclick="addFavorite('<?php echo $userData[$i]['nUserID'];?>')" id="anchor_fav_<?php echo $userData[$i]['nUserID'];?>" class="anchor_fav <?php echo $class;?>" ><?php echo $text; ?></a>
						<?php } ?>
						
						<a class="pro_img" href="javascript:void(0);" onclick="movetoprofile('<?php echo $userData[$i]['nUserID'];?>');">
						<?php if($userData[$i]['sPartnerLogo'] != '' ){?>
							<img src="<?php echo $user_profile_images_url.$userData[$i]['sPartnerLogo'];?>" alt="">
						<?php }else{?>
							<img src="<?php echo $base_url;?>images/4_no_image.png" alt="">
						<?php }?>
						</a>
						<span class="pro_desc_wrapper" onclick="movetoprofile('<?php echo $userData[$i]['nUserID'];?>');"> 
							<p class="pro_name"><?php echo $userData[$i]['sFirstName'] .' '. $userData[$i]['sLastName'];?></p>
							<p class="pro_age"><?php echo get_age($userData[$i]['dBirthDate']);?> år - <?php echo $userData[$i]['sArea'];?></p>
						</span>
					</span>
				</div>
			<?php }}else{ echo '<div id="" class="profile_container">No profile found.</div>'; } ?>
			</div> <!--profile container div end-->
			</div>
			<div class="pro_go_on" style="margin-top: 30px;">
				<span class="go_on_arrow" id="ajaxloader" style="display: none;"><img src="<?php echo $base_url;?>images/ajaxloader.gif" alt=""></span>
			</div>
		</div>
	</section>
	<!-- Load Profiles Modules End -->
<?php include('footer-inner.php');?>
<script>
/*** User profile click will redirect to specific profile page ***/
function movetoprofile(userid){
	location.href = "profile.php?id="+userid;
}
    
/*** Clear all search parameters and set default listing start ***/
function clearsearch(){
	$('#ajaxloader').show();
	var ajaxtimestamp = new Date().getTime();
	var base_url = $('#baseurl').val();
	$.post(base_url+"profilePaging.php", { page: "clear_search", ajaxtimestamp: ajaxtimestamp }, function(data){
		if(data == 'clear'){
		    $('#agemin,#agemax, #area, #postcodefrom, #postcodeto, #fname, #lname, #relstatus').val('');
		    $('#agemin_lbl').html('Min');
		    $('#agemax_lbl').html('Max');
		    $('#gender').val('Alle');
		    $("#area_lbl").html('Alle');
		    $("#area").val('Alle');
		    $('#gender_lbl').html('Alle');
		    $('#relstatus_lbl').html('Alle');
		    $('#relstatus').val('Alle');
		    $('#favorite').attr('checked', false);
		    searchProfiles();
		}
	});
}
/*** used for clear all search parameters and set default listing end ***/
</script>