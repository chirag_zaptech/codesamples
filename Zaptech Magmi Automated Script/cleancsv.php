<?php
clearstatcache();
$cur_dir = dirname(__FILE__);
$fin = fopen($cur_dir . '/PartsExport.csv', 'r') or die('cant open file');

$file = $cur_dir . "/sample.csv";
$newfile = $cur_dir . "/newproducts.csv";
$cp = copy($file, $newfile); // copy sample which has headers 
$output = fopen($cur_dir . "/newproducts.csv", "a");

$i = 0;

//load magento core files

require_once('app/Mage.php');

umask(0);
Mage::app();
ini_set('display_errors', 1);

// Register a secure admin environment
Mage::register('isSecureArea', true); /* set secure admin area*/


while (($data = fgetcsv($fin, 0, ",")) !== FALSE) {

    if ($i == 0) { // ignore first line which is header
        $i++;
        continue;
    }
    
    $d = fill_default($data);
    if(count($d) > 0){
        fputcsv($output, $d);
    }

    $i++;
}


$ts = date('Y-m-d-h-m');

//Copy PartsExport to backup directory
$cp = copy($cur_dir . "/PartsExport.csv", $cur_dir . "/backups/PartsExport". $ts .".csv");

//remove PartsExport file
unlink($cur_dir . "/PartsExport.csv");

Mage::unregister('isSecureArea'); /* unset secure admin area*/
    
    
function fill_default($data) {
   
    $odata = array();

    if($data[41] == 1) {
        delete_product($data[4]);   // if the internal value is 1 delete product
        return array();

    } else {
        $p_name = iconv('UTF-8', 'ASCII//TRANSLIT', $data[4]);
        $sku= trim($p_name);
        $name = trim($p_name);
        //echo $name;
        
        $total_desc = create_str(5, 11,',',$data);
        $description =   preg_replace('/[\x80-\xFF]/', "”", $total_desc);

        //price calculateion based on formula        
        $price = calculate_price($data[23], $data[24]);
        
        //url key genearete
        $url_key_1 = create_str(4, 9,'-',$data);
        $url_key = str_replace('"','',$url_key_1);
        $url_key = str_replace("'","",$url_key_1);
        
        //meta title genearete
        $meta_title = create_str(4, 9,',',$data);
        
        //meta keywords genearete
        $meta_keywords = create_str(4, 9,',',$data);
        //echo $meta_keywords;
        $meta_discription = create_str(9, 11,',',$data);
        $meta_discription =   preg_replace('/[\x80-\xFF]/', "”", $meta_discription);
        
        $qty = $data[25];
        
        $notify_min_qty = $data[29];
        $available_qty = $data[25];
        
        $notes_1 = create_str(1, 4,', ',$data);
        $notes_2 = create_str(12, 23,', ',$data);
        $notes_3 = create_str(27, 41,', ',$data);
        $notes_4 = create_str(43, 43,', ',$data);
        $notes = array_filter(array ($notes_1, $notes_2, $notes_3, $notes_4));
        $notes = implode(",", $notes);
    }

    $odata[0] = "admin"; //store
    $odata[1] = "Default"; // attribute_set
    $odata[2] = "simple"; // type
    $odata[3] = $sku; // sku
    $odata[4] = "12"; // category_ids

    $odata[5] = $name; //product_name
    $odata[6] = $name; //name
    $odata[7] = "1"; // weight
    $odata[8] = $price; // price
    $odata[9] = $description; // description

    $odata[10] = $description; // short_description
    $odata[11] = "1"; // status
    $odata[12] = "0"; // tax_class_id
    $odata[13] = "4"; // visibility
    $odata[14] = $available_qty; // manage_stock

    $odata[15] = "1"; //use_config_manage_stock
    $odata[16] = $available_qty; // qty
    $odata[17] = "1"; // is_in_stock
    $odata[18] = $meta_keywords; // meta_keyword
    $odata[19] = $meta_title; // meta_title 
    $odata[20] = $meta_discription; // meta_description
    $odata[21] = "/" . $sku . ".jpg"; //image
    $odata[22] = "/" . $sku . ".jpg"; // small_image
    $odata[23] = "/" . $sku . ".jpg"; // thumbnail
    $odata[24] = $data[4]; //image_label
    $odata[25] = $url_key; //url_key
    $odata[26] = $notify_min_qty; // notify_stock_qty
    $odata[27] = $notes; // notes
    
    return $odata;
}


// function delete product which have internal value set true
function delete_product($productSku) {
    // Load the Magento product model by sku and delete
    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
    if($product) {
        try {
                $product->delete();
                $cust_file = fopen("/home/abasolut/public_html/scripts/import_product_log.txt", "a");
                if ($cust_file == false)
                {
                    throw new Exception("custome file cann't open it");
                }
                $cust_file_data = "Product with sku:" . $productSku . " deleted on ".date('m-d-Y H:i:s');
                fwrite($cust_file, "$cust_file_data\n");
                fclose($cust_file);
                
            } catch (Exception $e) {
                $cust_file = fopen("/home/abasolut/public_html/scripts/import_product_log.txt", "a");
                if ($cust_file == false)
                {
                    throw new Exception("custome file cann't open it");
                }
                $cust_file_data = "Product with sku:" . $productSku . " not deleted on ".date('m-d-Y H:i:s');
                fwrite($cust_file, "$cust_file_data\n");
                fclose($cust_file);
            }
     } else {
        //echo $productSku . " Not Available";
     }

}


// function calculates product price
function calculate_price($cost, $margin) {
    if(isset($cost) && $cost != 0 && isset($margin) && $margin != 0 ) {
        $price = $cost * $margin;
        
        if($price){
           $price = number_format($price, 2); 
        }

    }else { 
       
       if($cost){
            $price = number_format($cost,2); 
       }
        

    }
    return $price;
}


// function creates new string as per separator
function create_str($start, $end, $sep, $data) {
    $str_array = array();
    for($j=$start; $j<$end; $j++) {
        if(isset($data[$j])) {
            $str_array[] = $data[$j];
        }
    }
    $created_string = rtrim(implode($sep,array_filter($str_array, 'strlen')));
    return $created_string;
}

?>
