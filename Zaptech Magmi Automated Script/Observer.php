<?php
/**
 * @category   ZAPTECH
 * @package    ZAPTECH_PRODUCTQA
 * Use         Sends email notification for followed product to customer 
 */  
Class Zaptech_ProductQa_Model_Observer  extends Mage_Cron_Model_Observer
{
    const XML_PATH_QUESTION_NOTIFY_TEMPLATE = 'zaptech_productqa/product_email/question_notify_template';
    const XML_PATH_ANSWER_NOTIFY_TEMPLATE = 'zaptech_productqa/product_email/answer_notify_template';
    
    /*
     *function sends product related questions by emails to customer
     */
    public function sendProductQuestionEmail()
    {
        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template');
        // Get followed question for particular product and send email
        $this->_resource_notify = Mage::getSingleton('core/resource')->getConnection('core_write');
        $result = $this->_resource_notify->fetchAll("SELECT nickname, content, email FROM zaptech_productqa_questions where notify = 1 and status = 5 and created_datetime >= DATE_SUB(NOW(), interval 1 day)");
        // Send email to all those customer who want to follow questions for selected product
        foreach ($result as $recipient) {
            if($recipient['email']!= '') {
            if($recipient['nickname'] == '') { $name = 'Guest'; } else { $name = $recipient['nickname']; }
            $mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>Mage::app()->getStore()->getId()))
                ->sendTransactional(
                    Mage::getStoreConfig(self::XML_PATH_QUESTION_NOTIFY_TEMPLATE, Mage::app()->getStore()->getId()),
                    Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY,Mage::app()->getStore()->getId()),
                    $recipient['email'],
                    null,
                    array(
                        'customer'  =>$name,
                        'question' => $recipient['content']
                    )
                );
            }
        }
        // Get all the questions related to particular product and send email
        $this->_resource_notifyall = Mage::getSingleton('core/resource')->getConnection('core_write');
        $res = $this->_resource_notifyall->fetchAll("SELECT product_id, email, nickname FROM zaptech_productqa_questions where notifyall = 1");
        foreach($res as $info) {
            if($info['email']!= '') {
                $productId = $info['product_id'];
                if($info['nickname'] == '') {
                    $name = 'Guest';
                } else { 
                    $name =  $info['nickname'];
                }
                $this->_resource_questions = Mage::getSingleton('core/resource')->getConnection('core_write');            
                $questions = $this->_resource_questions->fetchAll("SELECT content FROM zaptech_productqa_questions where product_id = $productId and status = 5 and created_datetime >= DATE_SUB(NOW(), interval 1 day)");            
            
                $mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>Mage::app()->getStore()->getId()))
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_QUESTION_NOTIFY_TEMPLATE, Mage::app()->getStore()->getId()),
                        Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY,Mage::app()->getStore()->getId()),
                        $info['email'],
                        null,
                        array(
                            'customer' => $name,
                            'question' => $questions
                        )
                    );
            }
        }
        $translate->setTranslateInline(true);
        return $this;
    }
    
    /*
     *function sends product related answer by emails to customer
     */
    public function sendProductAnswerEmail()
    {
        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);    
        $mailTemplate = Mage::getModel('core/email_template');
        // Get followed answer for particular product and send email
        $this->_resource_notify = Mage::getSingleton('core/resource')->getConnection('core_write');
        $result = $this->_resource_notify->fetchAll("SELECT nickname, content, email FROM zaptech_productqa_answers where notify = 1 and status = 5 and created_datetime >= DATE_SUB(NOW(), interval 1 day)");
        // Send email to all those customer who want to follow answer for selected product    
        foreach ($result as $recipient) {
            if($recipient['email']!= '') {
            if($recipient['nickname'] == '') { $name = 'Guest'; } else { $name = $recipient['nickname']; }
            $mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>Mage::app()->getStore()->getId()))
                ->sendTransactional(
                    Mage::getStoreConfig(self::XML_PATH_ANSWER_NOTIFY_TEMPLATE, Mage::app()->getStore()->getId()),
                    Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY,Mage::app()->getStore()->getId()),
                    $recipient['email'],
                    null,
                    array(
                        'customer'  =>$recipient['name'],
                        'answer' => $recipient['content']
                    )
                );
            }
        }
        
        // Get all the answers related to particular product and send email
        $this->_resource_notifyall = Mage::getSingleton('core/resource')->getConnection('core_write');
        $res = $this->_resource_notifyall->fetchAll("SELECT product_id, email, nickname FROM zaptech_productqa_answers where notifyall = 1");

        foreach($res as $info) {
            if($info['email']!= '') {
                $productId = $info['product_id'];
                if($info['nickname'] == '') {
                    $name = 'Guest';
                } else { 
                    $name =  $info['nickname'];
                }
                $this->_resource_answers = Mage::getSingleton('core/resource')->getConnection('core_write');
                $answers = $this->_resource_answers->fetchAll("SELECT content FROM zaptech_productqa_answers where product_id = $productId and status = 5 and created_datetime >= DATE_SUB(NOW(), interval 1 day)");
            
                $mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>Mage::app()->getStore()->getId()))
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_ANSWER_NOTIFY_TEMPLATE, Mage::app()->getStore()->getId()),
                        Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY,Mage::app()->getStore()->getId()),
                        $info['email'],
                        null,
                        array(
                            'customer' => $name,
                            'question' => $answers
                        )
                    );
            }
        }
        $translate->setTranslateInline(true);
        return $this;
    }
}